/*
 *  eio_main.c
 *
 *  Copyright (C) 2012 STEC, Inc. All rights not specifically granted
 *   under a license included herein are reserved
 *  Made EnhanceIO specific changes.
 *   Saied Kazemi <skazemi@stec-inc.com>
 *   Siddharth Choudhuri <schoudhuri@stec-inc.com
 *  Amit Kale <akale@stec-inc.com>
 *   Restructured much of the io code to split bio within map function instead
 *   of letting dm do it.
 *   Simplified queued logic for write through.
 *   Created per-cache spinlocks for reducing contention in IO codepath.
 *  Amit Kale <akale@stec-inc.com>
 *  Harish Pujari <hpujari@stec-inc.com>
 *   Designed and implemented the writeback caching mode
 *  Copyright 2010 Facebook, Inc.
 *   Author: Mohan Srinivasan (mohan@facebook.com)
 *
 *  Based on DM-Cache:
 *   Copyright (C) International Business Machines Corp., 2006
 *   Author: Ming Zhao (mingzhao@ufl.edu)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; under version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <linux/sort.h>		//for log compaction
#include <linux/jhash.h>	//for redundancy check
#include "eio.h"
#include "eio_ttc.h"
#define CTRACE(X) { }

/*
 * TODO List :
 * 1) sysctls : Create per-cache device sysctls instead of global sysctls.
 * 2) Management of non cache pids : Needs improvement. Remove registration
 * on process exits (with  a pseudo filesstem'ish approach perhaps) ?
 * 3) Breaking up the cache spinlock : Right now contention on the spinlock
 * is not a problem. Might need change in future.
 * 4) Use the standard linked list manipulation macros instead rolling our own.
 * 5) Fix a security hole : A malicious process with 'ro' access to a file can
 * potentially corrupt file data. This can be fixed by copying the data on a
 * cache read miss.
 */

static int eio_read_peek(struct cache_c *dmc, struct eio_bio *ebio);
static int eio_write_peek(struct cache_c *dmc, struct eio_bio *ebio);
static void eio_read(struct cache_c *dmc, struct bio_container *bc,
		     struct eio_bio *ebegin);
static void eio_write(struct cache_c *dmc, struct bio_container *bc,
		      struct eio_bio *ebegin);
static int eio_inval_block(struct cache_c *dmc, sector_t iosector);
static void eio_enqueue_readfill(struct cache_c *dmc, struct kcached_job *job);
static int eio_acquire_set_locks(struct cache_c *dmc, struct bio_container *bc);
static int eio_release_io_resources(struct cache_c *dmc,
				    struct bio_container *bc);
static void eio_clean_set(struct cache_c *dmc, index_t set, int whole,
			  int force);
#ifdef CONFIG_TX_CACHE
static int tx_cache_recover_rented_set(struct cache_c *dmc);
static void	tx_cache_check_persist_set_thresholds(struct cache_c *dmc, index_t set);
static void tx_cache_check_persist_cache_thresholds(struct cache_c *dmc);
void tx_cache_make_checkpoint_on_recovery(struct cache_c *dmc);
static void tx_cache_attach_set(struct cache_c *dmc, unsigned set);
static void tx_cache_detach_set(struct cache_c *dmc, unsigned set);
#endif

#if CONFIG_SET_SHARING
static struct set_chain *alloc_set_chain(void);
static void dealloc_set_chain(struct set_chain *chain) ;
#endif

#if CONFIG_DELAYED_MD_WRITE
static void eio_do_delayed_mdupdate(struct work_struct *work);
static void eio_post_delayed_mdupdate(struct work_struct *work);
static void tx_cache_dealloc_mdupdate_set(struct mdupdate_cache_set *set);
#else
static void eio_do_mdupdate(struct work_struct *work);
static void eio_post_mdupdate(struct work_struct *work);
static void eio_mdupdate_callback(int error, void *context);
#endif

static void eio_uncached_read_done(struct kcached_job *job);
static void eio_addto_cleanq(struct cache_c *dmc, index_t set, int whole);
static int eio_alloc_mdreqs(struct cache_c *, struct bio_container *);
#ifndef CONFIG_TX_CACHE
static void eio_check_dirty_set_thresholds(struct cache_c *dmc, index_t set);
static void eio_check_dirty_cache_thresholds(struct cache_c *dmc);
#endif
static void eio_post_io_callback(struct work_struct *work);
static u_int32_t hash_block(struct cache_c *dmc, sector_t dbn);

#ifdef CONFIG_TX_CACHE
struct kmem_cache *tx_unit_cache;
struct kmem_cache *tx_block_cache;

atomic_t aoml_log_id;

#if CONFIG_DELAYED_MD_WRITE
static struct kmem_cache *tx_set_cache;
#endif

#if CONFIG_SET_SHARING
static struct kmem_cache *set_chain_cache;
#endif

unsigned long callee_loc = 0;
#define PREPARE_LAST_LOC(loc) do { \
	callee_loc = loc; \
	} while (0);

unsigned long stat_tx_alloc = 0;
unsigned long stat_tx_commit = 0;
unsigned long stat_tx_abort = 0;

unsigned int recovered_aoml_head = 0;
unsigned int recovered_aoml_rear = 0;

struct tx_block *alloc_tx_block(void)
{
	return kmem_cache_zalloc(tx_block_cache, GFP_KERNEL);
}

static inline void dealloc_tx_block(struct tx_block *block)
{
	kmem_cache_free(tx_block_cache, block);	
}

/* make persistent cache block and find rented entries */
static inline void tx_cache_recover_metadata_state(struct cache_c *dmc)
{
	int i;

	for (i = 0; i < dmc->size; i++) {
		if (EIO_DBN_GET(dmc, i) != 0) 
			EIO_CACHE_STATE_SET(dmc, i, PERSIST | VALID);
		else 
			eio_invalidate_md(dmc, i);
	}
}

static int tx_cache_fill_by_zero (struct cache_c *dmc)
{
	struct flash_cacheblock *meta_data_cacheblock, *next_ptr;
	struct eio_io_region where;
	int nr_pages, page_count, page_index;
	int i, slots_write;
	sector_t size;
	struct bio_vec *header_page, *pages;
	int ret = 0, error;
	void **pg_virt_addr;
	
	page_count = 0;

	pages = eio_alloc_pages(dmc->bio_nr_pages, &page_count);
	
	nr_pages = page_count;

	pg_virt_addr = kmalloc(nr_pages * (sizeof(void *)), GFP_KERNEL);
	if (pg_virt_addr == NULL) {
		pr_err("eio_md_store: System memory too low.");
		for (i = 0; i < nr_pages; i++)
			put_page(pages[i].bv_page);
		kfree(pages);
		ret = -ENOMEM;
		goto out;
	}

	for (i = 0; i < nr_pages; i++) {
		pg_virt_addr[i] = kmap(pages[i].bv_page);
		memset(pg_virt_addr[i], 0, PAGE_SIZE);
	}

	page_index = 0;
	page_count = 0;
	meta_data_cacheblock =
		(struct flash_cacheblock *)pg_virt_addr[page_index];

	where.bdev = dmc->cache_dev->bdev;
	where.sector = dmc->md_start_sect;
	size = dmc->aoml_size;
	i = 0;
	while (size > 0) {
		slots_write =
			min((long)size, ((long)MD_BLOCKS_PER_PAGE * nr_pages));

		if (slots_write % MD_BLOCKS_PER_SECTOR)
			where.count = 1 + (slots_write / MD_BLOCKS_PER_SECTOR);
		else
			where.count = slots_write / MD_BLOCKS_PER_SECTOR;

		if (slots_write % MD_BLOCKS_PER_PAGE)
			page_count = 1 + (slots_write / MD_BLOCKS_PER_PAGE);
		else
			page_count = slots_write / MD_BLOCKS_PER_PAGE;

		error = eio_io_sync_vm(dmc, &where, WRITE, pages, page_count);
		
		if (error) {
			TX_CACHE_ERROR("error occurred while io");
			return -EIO;
			goto out;
		}

		where.sector += where.count;
		next_ptr = meta_data_cacheblock;
		size -= slots_write;
	}
out:
	for (i = 0; i < nr_pages; i++)
		kunmap(pages[i].bv_page);
	
	kfree(pg_virt_addr);
	
	if (pages) {
		for (i = 0; i < nr_pages; i++)
			put_page(pages[i].bv_page);
		kfree(pages);
		pages = NULL;
	}

	return ret;
}

struct md_entry_check {
	u_int32_t log_id;
	u_int64_t index;
	u_int64_t dbn;
};

static inline index_t tx_cache_hash1(u_int64_t val, int size)
{
	index_t key;
	u_int32_t	word1, word2, mask = ~0U;
	u_int32_t	hashval;
		
	word1 = val & mask;
	word2 = (val > 32) & mask;
	hashval = jhash_2words(word1, word2, 17);	

	return hashval % size;
}

static inline index_t tx_cache_hash2(u_int64_t val, int size)
{
	index_t key;
	u_int32_t	word1, word2, mask = ~0U;
	u_int32_t	hashval;
		
	word1 = val & mask;
	word2 = (val > 32) & mask;
	hashval = jhash_2words(word1, word2, 5);	

	return hashval % size;
}

void tx_cache_check_dbn_redundancy(struct cache_c *dmc)
{
	//struct cacheblock_md16 *dbn_table;
	struct md_entry_check *dbn_table;
	int order = dmc->size * sizeof(struct md_entry_check);
	int i;
	u_int32_t	hashval, log_id, r_log_id;
	u_int64_t	dbn;

	dbn_table = vmalloc((size_t)order);

	if (!dbn_table) {
		TX_CACHE_ERROR("memory exhausted");
		return;	
	}

	memset(dbn_table, 0, order);

	for (i = 0; i < dmc->size; i++) {
		if (TX_CACHE_STATE_TEST(dmc, i, PERSIST)) {
			dbn = EIO_DBN_GET(dmc, i);
			hashval = tx_cache_hash1(dbn, dmc->size);
			log_id = TX_CACHE_LOG_ID_GET(dmc, i);

			if (dbn_table[hashval].dbn != 0) {
				/* find redundancy */
				if (dbn_table[hashval].dbn == dbn) {
					TX_CACHE_DEBUG("find redundancy (index: %d)", i);
					r_log_id = dbn_table[hashval].log_id;

					if (log_id > r_log_id) {
						eio_invalidate_md(dmc, dbn_table[hashval].index);
						dbn_table[hashval].dbn = dbn;
						dbn_table[hashval].index = i;
						dbn_table[hashval].log_id = log_id;
					} else 
						eio_invalidate_md(dmc, i);
				} else {
					/* hash conflict: give second chance */	
					TX_CACHE_INFO("hash conflict(index: %d: dbn: %lu)give second chance", i, dbn);
					hashval = tx_cache_hash2(dbn, dmc->size);
					if (dbn_table[hashval].dbn != 0) {
						if (dbn_table[hashval].dbn == dbn) {
							r_log_id = dbn_table[hashval].log_id;

							if (log_id > r_log_id) {
								eio_invalidate_md(dmc, dbn_table[hashval].index);
								dbn_table[hashval].dbn = dbn;
								dbn_table[hashval].index = i;
								dbn_table[hashval].log_id = log_id;
							} else
								eio_invalidate_md(dmc, i);

						} else {
							TX_CACHE_INFO("hash conflict(index: %d dbn: %lu)", i, dbn);
							TX_CACHE_INFO("just skip this");
							continue;	
						}
					} else {
						dbn_table[hashval].dbn = dbn;
						dbn_table[hashval].index = i;
						dbn_table[hashval].log_id = log_id;
					}
				}
			} else {
				/* newly inserted */
				dbn_table[hashval].dbn = dbn;
				dbn_table[hashval].index = i;
				dbn_table[hashval].log_id = log_id;
			}
		}
	}

	vfree(dbn_table);
	TX_CACHE_INFO("redundancy check complete");
}


void tx_cache_wrapup_recovery(struct cache_c *dmc)
{
	int i, count = 0;

	for (i = 0; i < dmc->size; i++) {
		if (TX_CACHE_STATE_TEST(dmc, i, PERSIST)) {
			TX_CACHE_LOG_ID_SET(dmc, i, 0);
			EIO_CACHE_STATE_SET(dmc, i, PERSIST | VALID);	
			count++;
		}
	}
	TX_CACHE_INFO("Total %d cache blocks are remained", count);
}

int tx_cache_init(struct cache_c *dmc)
{
	unsigned long persist_idx, set_idx;
	unsigned long flag;
	struct tx_block *block;
	struct list_head tmp_lst;
	struct rb_node	tmp_tree;
#if CONFIG_SET_SHARING
	unsigned long set;
#endif

	tx_unit_cache = kmem_cache_create("tx_unit_cache", sizeof(struct tx_unit), 0, SLAB_PANIC, NULL);	

	if (!tx_unit_cache) {
		TX_CACHE_DEBUG("tx_unit_cache creation failed");
		return -ENOMEM;
	}

	tx_block_cache = kmem_cache_create("tx_block_cache", sizeof(struct tx_block), 0, SLAB_PANIC, NULL);
	
	if (!tx_block_cache) {
		TX_CACHE_DEBUG("tx_block_cache creation failed");
		kmem_cache_destroy(tx_unit_cache);

		return -ENOMEM;
	}

#if CONFIG_DELAYED_MD_WRITE
	tx_set_cache = kmem_cache_create("tx_set_cache", sizeof(struct mdupdate_cache_set), 0, SLAB_PANIC, NULL);
	
	if (!tx_set_cache) {
		TX_CACHE_ERROR("tx_set_cache creation failed");
		kmem_cache_destroy(tx_unit_cache);
		kmem_cache_destroy(tx_block_cache);

		return -ENOMEM;
	}
#endif

#if CONFIG_SET_SHARING
	set_chain_cache = kmem_cache_create("set_chain_cache", sizeof(struct set_chain), 0, SLAB_PANIC, NULL);
	spin_lock_init(&dmc->empty_list_lock);
	spin_lock_init(&dmc->sorted_list_lock);
	spin_lock_init(&dmc->rented_list_lock);
	INIT_LIST_HEAD(&dmc->empty_list);
	INIT_LIST_HEAD(&dmc->rented_list);

	if (!set_chain_cache) {
		TX_CACHE_ERROR("set_chain_cache creation failed");
		kmem_cache_destroy(tx_unit_cache);
		kmem_cache_destroy(tx_block_cache);
#if CONFIG_DELAYED_MD_WRITE
		kmem_cache_destroy(tx_set_cache);
#endif
	}

	for (set = 0; set < dmc->num_sets; set++) 
		list_add_tail(&dmc->cache_sets[set].empty_list, &dmc->empty_list);
#endif	//CONFIG_SET_SHARING

	spin_lock_init(&dmc->shadow_lock);
	spin_lock_init(&dmc->single_req_tx_lock);
	spin_lock_init(&dmc->pers_list_lock);

	atomic_set(&aoml_log_id, 0);
	atomic_set(&dmc->in_compaction, 0);
	atomic_set(&dmc->nr_checkpoints, 0);
	atomic64_set(&dmc->last_compaction, jiffies);

	atomic64_set(&dmc->nr_mdlog_pages, 0);
	atomic64_set(&dmc->nr_ckpt_pages, 0);
	
	INIT_LIST_HEAD(&dmc->shadow_list);	
	//INIT_LIST_HEAD(&dmc->persist_list);
	INIT_LIST_HEAD(&dmc->single_req_tx_list);
	INIT_LIST_HEAD(&dmc->checkpoint_list);
	INIT_LIST_HEAD(&dmc->clean_undo_list);

	list_add(&tmp_lst, &dmc->checkpoint_list);
	list_del(&tmp_lst);

	dmc->in_deletion = 0;
	dmc->nr_shadow_tx = 0;
	dmc->nr_persist_tx = 0;
	dmc->nr_single_req_tx = 0;

	dmc->pers_leftmost = NULL;
	dmc->pers_sets = vmalloc(dmc->num_sets * sizeof(struct set_list));
	
	dmc->aoml_len_thresh = DEFAULT_AOML_LEN_THRESH;
	dmc->aoml_rear = 0;
	dmc->aoml_head = 0;
	dmc->aoml_free = dmc->aoml_size / SECTORS_PER_PAGE;
	
	atomic_set(&dmc->aoml_emergency, 0);
	
	init_rwsem(&dmc->reclaimer_lock);

	dmc->checkpoint_rear = dmc->aoml_size / SECTORS_PER_PAGE;
	dmc->checkpoint_head = dmc->aoml_size / SECTORS_PER_PAGE;
	
	spin_lock_init(&dmc->aoml_lock);
	spin_lock_init(&dmc->checkpoint_lock);

	INIT_LIST_HEAD(&dmc->md_log_list);	
	spin_lock_init(&dmc->md_log_lock);

	spin_lock_init(&dmc->ebio_lock);

	for (set_idx = 0; set_idx < dmc->num_sets; set_idx++) {
		dmc->pers_sets[set_idx].key = 0;
		dmc->pers_sets[set_idx].set_num = set_idx;
		dmc->pers_sets[set_idx].q_flag = IN_QUEUE;
		tx_cache_pers_set_timestamp(&dmc->pers_sets[set], jiffies);
	}

	atomic_set(&dmc->nr_tx_block, 0);
	
	dmc->alloc_tx_block = alloc_tx_block;
	dmc->set_undo = tx_cache_set_undo;

	if (dmc->persistence == CACHE_RELOAD && dmc->mode == CACHE_MODE_WB) {
		/*
		 * The cache is reloaded after the last shutdown.
		 * So, we should re-build the persistent commit list based on persist map.
		 */
		unsigned orig_set, curr_set, prev_set = 0;
		tx_cache_recover_metadata_state(dmc);
		//spin_lock_irqsave(&dmc->persist_lock, flag);	
		
		tx_cache_detach_set(dmc, 0);
		
		for (persist_idx = 0; persist_idx < dmc->size; persist_idx++) {
			orig_set = hash_block(dmc, EIO_DBN_GET(dmc, persist_idx));
			curr_set = persist_idx / dmc->assoc;

			if (prev_set != curr_set) {
				/* cross the set border */
				tx_cache_attach_set(dmc, prev_set);
				prev_set = curr_set;
				tx_cache_detach_set(dmc, curr_set);
			}
			
			if (TX_CACHE_STATE_TEST(dmc, persist_idx, PERSIST)) {
				atomic_inc(&dmc->nr_tx_block);
				dmc->cache_sets[persist_idx / dmc->num_sets].nr_persist++;

				atomic64_inc(&dmc->nr_persist);

				/* handle rented entry */
				if (orig_set != curr_set) {
					struct cache_set *rented_set;
					struct set_header *renter_set;
					struct set_chain *set_chain;

					rented_set = &dmc->cache_sets[curr_set];
					renter_set = &dmc->set_headers[orig_set];
					TX_CACHE_STATE_RENT(dmc, persist_idx);
					
					if (rented_set->nr_rented == 0) {
						set_chain = alloc_set_chain();
						//list_del(&rented_set->empty_list);
						list_add_tail(&set_chain->chain, &renter_set->chain);
						rented_set->set_chain = set_chain;
						set_chain->master_set = orig_set;
						set_chain->rental_set = curr_set;
						renter_set->nr_rent++;
					} else {
						EIO_ASSERT(rented_set->set_chain->master_set == orig_set);
					}
					
					rented_set->nr_rented++;
				}
			}
		}

		tx_cache_attach_set(dmc, curr_set);
		
		//spin_unlock_irqrestore(&dmc->persist_lock, flag);	
		tx_cache_recover_pers_list(dmc);
		tx_cache_check_dbn_redundancy(dmc);
		//tx_cache_make_checkpoint_on_recovery(dmc);
		tx_cache_wrapup_recovery(dmc);
		//tx_cache_recover_rented_set(dmc);
		//tx_cache_fill_by_zero(dmc);
		if (recovered_aoml_head != 0)
			dmc->aoml_head = recovered_aoml_head;
		if (recovered_aoml_rear != 0)
			dmc->aoml_rear = recovered_aoml_rear;
		TX_CACHE_INFO("TxFCache recovery complete.");
	}
	TX_CACHE_DEBUG("tx cache initialization complete");
	TX_CACHE_DEBUG("dirty set high threshold: %ld", dmc->sysctl_active.dirty_set_high_threshold * (dmc)->assoc / 100);
	return TX_SUCCESS;
}

void tx_cache_exit(struct cache_c *dmc)
{
	kmem_cache_destroy(tx_unit_cache);
	kmem_cache_destroy(tx_block_cache);
	kmem_cache_destroy(tx_set_cache);
	TX_CACHE_INFO("Good bye~");
}

/*
 * Must check shadow_map before calling this function.
 * Must hold spinlock before calling this function.
 */

void add_block_to_tx(struct cache_c *dmc, struct tx_unit *tx, struct tx_block *block, index_t index)
{
	list_add_tail(&block->blist, &tx->blist);
	tx->nr_blocks++;

	TX_CACHE_STATE_ON(dmc, index, SHADOW);
}

struct tx_unit *search_tx_by_id(struct list_head *list, unsigned long tx_id)
{
	struct tx_unit *tx;

	list_for_each_entry(tx, list, tlist) {
		if (tx->tx_id == tx_id)
			return tx;
	}

	TX_CACHE_ERROR("search failed: tx_id(%ld)", tx_id);

	return NULL;
}

/*
 * Persistent blocks are merged into single list.
 */
struct tx_block *search_persist_block_by_index(struct list_head *persist_list, index_t index)
{
	struct tx_block *block;

	list_for_each_entry(block, persist_list, blist){
		if (block->index == index)
			return block;
	}
	TX_CACHE_DEBUG("block(%d) is not in persist list", index);

	return NULL;
}

struct tx_block *search_tx_block_by_index(struct list_head *shadow_list, index_t index, struct tx_unit **owner)
{
	struct tx_unit *tx;
	struct tx_block *block;

	list_for_each_entry(tx, shadow_list, tlist) {
		TX_CACHE_DEBUG_IF(tx, "given tx(%ld)", tx->tx_id);
		list_for_each_entry(block, &tx->blist, blist) {
			if (block->index == index) {
				if (owner)
					*owner = tx;
				TX_CACHE_DEBUG("find block(%ld)", index);
				return block;
			}
		}
	}

	return NULL;
}

static int test_index_in_tx(struct tx_unit *tx, index_t index)
{
	struct tx_block *block;

	list_for_each_entry(block, &tx->blist, blist)
	{
		TX_CACHE_DEBUG("block->index: %d index: %d", block->index, index);
		if (block->index == index)
			return 1;
	}
	return 0;
}

struct tx_unit *__alloc_new_tx(struct cache_c *dmc, unsigned long tx_id)
{
	struct tx_unit *new_tx;

	new_tx = kmem_cache_zalloc(tx_unit_cache, GFP_KERNEL);
	
	if (!new_tx) {
		TX_CACHE_ERROR("tx allocation is failed");
		return NULL;
	}

	new_tx->tx_id = tx_id;
	new_tx->tx_status = TX_STARTED;
	new_tx->nr_blocks = 0;

	INIT_LIST_HEAD(&new_tx->blist);
	spin_lock_init(&new_tx->tx_lock);

#if CONFIG_DELAYED_MD_WRITE
	INIT_LIST_HEAD(&new_tx->pending_set_list);
	atomic_set(&new_tx->mdupdate_cnt, 0);
	atomic_set(&new_tx->set_cnt, 0);
	spin_lock_init(&new_tx->mdupdate_lock);
#endif
	atomic_set(&new_tx->nr_remain, 0);

	stat_tx_alloc++;

	TX_CACHE_INFO("new transaction(%ld) is allocated", tx_id);	

	return new_tx;
}

/* alloc_new_tx MUST be called while hold shadow lock */
struct tx_unit *alloc_new_tx(struct cache_c *dmc, unsigned long tx_id)
{
	struct tx_unit *new_tx;
	
	/* single request tx */
	if (tx_id == 0) {
		new_tx = kmem_cache_zalloc(tx_unit_cache, GFP_KERNEL);
		new_tx->tx_status = TX_STARTED;
		INIT_LIST_HEAD(&new_tx->blist);
		spin_lock_init(&new_tx->tx_lock);
		spin_lock_init(&new_tx->mdupdate_lock);
		stat_tx_alloc++;

		return new_tx;
	}

	new_tx = search_tx_by_id(&dmc->shadow_list, tx_id);

	if (new_tx) {
		TX_CACHE_ERROR("tx(%ld) is already exist", tx_id);
		return NULL;
	}

	return __alloc_new_tx(dmc, tx_id);
}

int dealloc_tx(struct cache_c *dmc, struct tx_unit *tx)
{
	TX_CACHE_DEBUG("%lu", callee_loc);
	if (tx->tx_status == TX_IN_PROGRESS || tx->tx_status == TX_IN_COMMIT
		|| tx->tx_status == TX_IN_ABORT) {
		TX_CACHE_ERROR("TX(%ld) is in progress", tx->tx_id);
		return TX_CONFLICT;
	}

	if (tx->tx_status == TX_COMMITTED) {
		TX_CACHE_DEBUG("TX(%ld) is committed", tx->tx_id);
		stat_tx_commit++;
	} else if (tx->tx_status == TX_ABORTED) {
		TX_CACHE_DEBUG("TX(%ld) is aborted", tx->tx_id);
		stat_tx_abort++;
	}

	TX_CACHE_DEBUG("TX(%ld) is deallocated", tx->tx_id);
	kmem_cache_free(tx_unit_cache, tx);

	return TX_SUCCESS;
}

void tx_cache_set_undo(struct cache_c *dmc, index_t index, struct cacheblock_md16 *undo)
{
	undo->cache_state 	= EIO_CACHE_STATE_GET(dmc, index);
	undo->index			= TX_CACHE_GET_LINK_INDEX(dmc, index);
	undo->dbn 			= EIO_DBN_GET(dmc, index);
}

static inline void tx_cache_do_undo(struct cache_c *dmc, index_t index, struct cacheblock_md16 *undo)
{
	EIO_CACHE_STATE_SET(dmc, index, undo->cache_state);
	TX_CACHE_SET_LINK_INDEX(dmc, index, undo->index);
	EIO_DBN_SET(dmc, index, undo->dbn);
}

static void tx_cache_undo_ebio(struct cache_c *dmc, struct eio_bio *ebio)
{
	index_t index;
	struct tx_block *block;

	index = ebio->eb_index;
	block = search_tx_block_by_index(&dmc->shadow_list, index, NULL);

	if (TX_CACHE_STATE_TEST(dmc, index, SHADOW))
		TX_CACHE_STATE_OFF(dmc, index, SHADOW);
	else if (TX_CACHE_STATE_TEST(dmc, index, PERSIST))
		TX_CACHE_STATE_OFF(dmc, index, PERSIST);

	EIO_CACHE_STATE_OFF(dmc, index, CACHEWRITEINPROG);
	tx_cache_do_undo(dmc, index, &block->undo);
}


#if CONFIG_DELAYED_MD_WRITE
/* get locks of participated cache sets */

static inline void tx_cache_prepare_cache_sets(struct cache_c *dmc, struct tx_unit *tx)
{
	struct mdupdate_cache_set *participant, *next;
	
	list_for_each_entry_safe(participant, next, &tx->pending_set_list, plist) {
		down_read(&dmc->cache_sets[participant->set_num].rw_lock);	
	}
}

static inline void tx_cache_acquire_locks_participants(struct cache_c *dmc, struct tx_unit *tx)
{
	struct mdupdate_cache_set *participant, *next;

	//spin_lock_irqsave(&tx->mdupdate_lock, tx->commit_flag);
	spin_lock(&tx->mdupdate_lock);

	list_for_each_entry_safe(participant, next, &tx->pending_set_list, plist) {
		TX_CACHE_DEBUG("set_header[%d] lock is held", participant->set_header);
		if (!dmc->set_headers[participant->set_header].locked_in_tx) {
			spin_lock(&dmc->set_headers[participant->set_header].header_lock);
			dmc->set_headers[participant->set_header].locked_in_tx = 1;
		}
	}
	list_for_each_entry_safe(participant, next, &tx->pending_set_list, plist) {
		TX_CACHE_DEBUG("cache_set[%d] lock is held", participant->set_num);
		spin_lock(&dmc->cache_sets[participant->set_num].cs_lock);	
		atomic_inc(&tx->set_cnt);
	}
}

static inline void tx_cache_release_locks_participants(struct cache_c *dmc, struct tx_unit *tx)
{	
	struct mdupdate_cache_set *participant, *next;

	list_for_each_entry_safe(participant, next, &tx->pending_set_list, plist) {
		TX_CACHE_DEBUG("cache_set[%d] lock is released", participant->set_num);
		spin_unlock(&dmc->cache_sets[participant->set_num].cs_lock);	
		atomic_dec(&tx->set_cnt);
	}
	list_for_each_entry_safe(participant, next, &tx->pending_set_list, plist) {
		TX_CACHE_DEBUG("set_header[%d] lock is released", participant->set_header);
		if (dmc->set_headers[participant->set_header].locked_in_tx) {
			spin_unlock(&dmc->set_headers[participant->set_header].header_lock);
			dmc->set_headers[participant->set_header].locked_in_tx = 0;
		}
	}
	//spin_unlock_irqrestore(&tx->mdupdate_lock, tx->commit_flag);
	spin_unlock(&tx->mdupdate_lock);
}

/* release locks of participated cache sets and dellocate resouces */

static inline void tx_cache_release_cache_sets(struct cache_c *dmc, struct tx_unit *tx)
{
	struct mdupdate_cache_set *participant, *next;

	list_for_each_entry_safe(participant, next, &tx->pending_set_list, plist) {
		TX_CACHE_DEBUG("TX(%ld) COMMIT complete: set: %d, nr_persist: %d",
				tx->tx_id, participant->set_num,
				dmc->cache_sets[participant->set_num].nr_persist);

		//up_read(&dmc->cache_sets[participant->set_num].rw_lock);	
		list_del(&participant->plist);
		tx_cache_dealloc_mdupdate_set(participant);
	}
}

static inline void tx_cache_touch_participants(struct cache_c *dmc, struct tx_unit *tx)
{
	struct mdupdate_cache_set *participant, *next;
	unsigned long flags;
	
	spin_lock_irqsave(&dmc->pers_list_lock, flags);
	list_for_each_entry_safe(participant, next, &tx->pending_set_list, plist) {
		if (dmc->pers_sets[participant->set_num].q_flag & IN_CLEANING)
			continue;

		spin_lock(&dmc->cache_sets[participant->set_num].cs_lock);
		tx_cache_pers_touch(dmc, participant->set_num, dmc->cache_sets[participant->set_num].nr_persist);	
		spin_unlock(&dmc->cache_sets[participant->set_num].cs_lock);
		
		tx_cache_check_persist_set_thresholds(dmc, participant->set_num);
	}
	tx_cache_check_persist_cache_thresholds(dmc);
	spin_unlock_irqrestore(&dmc->pers_list_lock, flags);
}

static inline void tx_cache_release_lock_participant(struct cache_c *dmc, struct tx_unit *tx, index_t set)
{
	spin_unlock(&dmc->cache_sets[set].cs_lock);	
	
	if (atomic_dec_and_test(&tx->set_cnt))
		spin_unlock_irqrestore(&tx->mdupdate_lock, tx->commit_flag);
}

#if 0
	}
#else
static int 
tx_cache_packing_md_log(struct cache_c *dmc, void *pg_addr, struct log_pack *pack, int log_idx)
{
	int i;
	struct log_header *header;
	struct log_entry *log;
	
	i = 0;

	if (log_idx == 0) {	//case of head 
		header = (struct log_header *)pg_addr;
		header->log_type = pack->header.log_type;
		header->log_id = pack->header.log_id;
		header->nr_entries = pack->header.nr_entries;
		header++;
		i++;
		
		/* if nr_entries == 255, it should cross page align */	
		//if (header->nr_entries == LOG_PACK_MAX_HEAD)
		//	head = 1;
		
		log = (struct log_entry *)header;
	} else 
		log = (struct log_entry *)pg_addr;

	if (log_idx == pack->header.nr_entries - 2)
		goto wrap_up_log;

	while (i < LOG_PACK_MAX) {
		log->index = pack->logs[log_idx].index;
		log->dbn = pack->logs[log_idx].dbn;
		log++;
		log_idx++;
		i++;
		
		//if (head == 1 && i == (LOG_PACK_MAX - 1))
		//	goto handle_head_page;

		if (log_idx == pack->header.nr_entries - 2) {
			if (i == LOG_PACK_MAX)
				return log_idx;
			goto wrap_up_log;
		}
	}

//handle_head_page:
	//TX_CACHE_DEBUG2("log_idx: %d", log_idx);
	return log_idx;

wrap_up_log:
	header = (struct log_header *)log;
	header->log_type = pack->header.log_type;
	header->log_id = pack->header.log_id;
	header->nr_entries = pack->header.nr_entries;
	TX_CACHE_DEBUG("log_type: %d log_id: %d nr_entries: %ld", TX_CACHE_GET_LOG_TYPE(header->log_type), header->log_id, header->nr_entries);
	header++;
	i++;
	
	log = (struct log_entry *)header;

	/* fill zero to rest of page */
	while (i < LOG_PACK_MAX) {
		log->index = 0;
		log->dbn = 0;
		log++;
		i++;
	}

	//TX_CACHE_DEBUG2("nr_entries: %lu log_idx: %d - complete", pack->header.nr_entries, log_idx); 
	return log_idx;
}

static int tx_cache_prepare_commit_log(struct cache_c *dmc, struct tx_unit *tx, struct log_page *log_page)
{
	unsigned long log_size = tx->nr_blocks + tx->nr_invalidate;
	int nr_log_pages = 1, ret, i;
	u_int32_t log_idx;
	struct log_pack *pack;
	void *pg_virt_addr[NR_COMMIT_PAGE] = {NULL};

	/* log entries collection is separated by page_size */
	
	log_size += 2; //including header and footer
	nr_log_pages = log_size / LOG_PACK_MAX;
	
	if (log_size % LOG_PACK_MAX)
		nr_log_pages++;

	/* if nr_log_pages > NR_MD_PAGE, how can we handle that case ?*/
	//TX_CACHE_INFO("log page size is %d", nr_log_pages);

	pack = (struct log_pack *)kzalloc(sizeof(struct log_pack), GFP_NOWAIT);

	if (!pack) {
		TX_CACHE_ERROR("log_pack allocation failed!");
		return -ENOMEM;
	}

	if (nr_log_pages > NR_MD_PAGE) {
		log_page->log_page = kmalloc(sizeof(struct page *) * nr_log_pages, GFP_NOWAIT);
		TX_CACHE_ERROR("nr_log_pages(%d) are larger than NR_MD_PAGE", nr_log_pages);

		if (!log_page->log_page) {
			TX_CACHE_ERROR("log_page allocation failed!");
			kfree(pack);
			return -ENOMEM;
		}

		ret = eio_alloc_wb_pages(log_page->log_page, nr_log_pages);
		
		if (ret) {
			TX_CACHE_ERROR("page allocation failed!");
			kfree(pack);
			kfree(log_page->log_page);
			log_page->log_page = NULL;

			return -ENOMEM;
		}
	} else
		log_page->log_page = dmc->commit_pages;

	pack->header.log_id = atomic_inc_return(&aoml_log_id);
	TX_CACHE_SET_COMMIT_LOG(pack->header.log_type);
	pack->header.nr_entries = log_size;
	pack->logs = tx->logs;
	
	log_page->log_pack = pack;
	log_page->nr_pages = nr_log_pages;

	for (i = 0; i < nr_log_pages; i++) 
		pg_virt_addr[i] = kmap(log_page->log_page[i]);
	
	log_idx = 0;
	ret = 0;
	
	for (i = 0; i < nr_log_pages; i++) {
		log_idx = tx_cache_packing_md_log(dmc, pg_virt_addr[i], 
						pack, log_idx);
	}
	
	for (i = 0; i < nr_log_pages; i++) 
		kunmap(log_page->log_page[i]);

	/* if nr_entries does not count header/footer */
	if (log_idx != pack->header.nr_entries - 2) {
		TX_CACHE_ERROR("error occurred while in committing tx(%ld)", tx->tx_id);
		ret = -EINVAL;
	}

	return ret;
}

#endif
#endif	//CONFIG_DELAYED_MD_WRITE

static void tx_cache_prepare_abort(struct cache_c *dmc, struct tx_unit *tx)
{
	struct tx_block *block;

	list_for_each_entry(block, &tx->blist, blist) {
		if (TX_CACHE_STATE_TEST(dmc, block->index, SHADOW))
			TX_CACHE_STATE_OFF(dmc, block->index, SHADOW);
		if (EIO_CACHE_STATE_GET(dmc, block->index) | CACHEWRITEINPROG)
			EIO_CACHE_STATE_OFF(dmc, block->index, CACHEWRITEINPROG);
		tx_cache_do_undo(dmc, block->index, &block->undo);
	}
}

inline void __abort_tx(struct cache_c *dmc, struct tx_unit *tx)
{
	struct tx_block *block, *next;
	index_t curset;
	
	list_for_each_entry_safe(block, next, &tx->blist, blist) {
		/*
		if (TX_CACHE_STATE_TEST(dmc, block->index, SHADOW))
			TX_CACHE_STATE_OFF(dmc, block->index, SHADOW);
		if (TX_CACHE_TEST_LINK(dmc, block->index)) 
			TX_CACHE_CLEAR_LINK(dmc, block->index);	
		*/	
		eio_invalidate_md(dmc, block->index);
		list_del(&block->blist);
		dealloc_tx_block(block);
		atomic_dec(&dmc->nr_tx_block);
	}
}

struct tx_unit *abort_tx(struct cache_c *dmc, unsigned long tx_id)
{
	struct tx_unit *tx;
	unsigned long flags = 0;

	spin_lock_irqsave(&dmc->shadow_lock, flags);
	tx = search_tx_by_id(&dmc->shadow_list, tx_id);
	list_del(&tx->tlist);
	spin_unlock_irqrestore(&dmc->shadow_lock, flags);

	if (tx == NULL)
		return NULL;

	tx->tx_status = TX_IN_ABORT;

	//tx_cache_prepare_cache_sets(dmc, tx);
	tx_cache_prepare_abort(dmc, tx);
	__abort_tx(dmc, tx);
	tx_cache_release_cache_sets(dmc, tx);

	tx->tx_status = TX_ABORTED;
	
	return tx;
}

/*
static inline void tx_block_set_shadow(struct tx_block *old, struct tx_block *new)
{
	old->dep.shadow = new;
	new->dep.old = old;
}
*/

/*
 * Invoke real commit function.
 * All blocks participated into transaction is flushed to source device.
 */

int do_commit_tx (struct cache_c *dmc, struct tx_unit *tx)
{
	/*
	 * To do.
	 */
	return TX_SUCCESS;
}

static inline void tx_cache_move_shadow_to_persist (struct cache_c *dmc, struct tx_unit *tx)
{
	struct tx_block *block;
	unsigned set_num;
	int log_index = 0;

	list_for_each_entry(block, &tx->blist, blist) {
		if (TX_CACHE_STATE_TEST(dmc, block->index, SHADOW)) {	
			//set_num = hash_block(dmc, EIO_DBN_GET(dmc, block->index));
			set_num = block->index / dmc->assoc;

			if (set_num > dmc->num_sets)
				TX_CACHE_ERROR("set_num(%u) is over dmc->num_sets", set_num);	

			dmc->cache_sets[set_num].nr_persist++;
			atomic64_inc(&dmc->nr_persist);

			TX_CACHE_STATE_ON(dmc, block->index, PERSIST);
			TX_CACHE_STATE_OFF(dmc, block->index, SHADOW);
			
			tx->logs[log_index].index = block->index;
			tx->logs[log_index].dbn = EIO_DBN_GET(dmc, block->index);
			log_index++;
		}
	}
}

static inline int tx_cache_set_in_participant(struct tx_unit *tx, u_int32_t set) 
{
	struct mdupdate_cache_set *participant, *next;

	list_for_each_entry_safe(participant, next, &tx->pending_set_list, plist) {
		if (participant->set_num == set) 
			return 1;
	}
	
	return 0;
}

static inline int tx_cache_check_committable(struct cache_c *dmc, struct tx_unit *tx)
{
	TX_CACHE_DEBUG3("tx:%p tx_id:%d nr_block:%d nr_remain:%d", tx, tx->tx_id, tx->nr_blocks, atomic_read(&tx->nr_remain));

	if (tx->nr_blocks == 0)
		return 0;
	
	while (atomic_read(&tx->nr_remain) > 0) {
		(void)schedule_timeout(10);	
	}

	down_read(&dmc->reclaimer_lock);

	return 1;
}

#if CONFIG_TX_CACHE_DEBUG
static void tx_cache_show_log_page(struct log_page *log_page)
{
	void *pg_addr[NR_COMMIT_PAGE] = {NULL};
	struct log_header *header, *footer;
	struct log_entry *entry;
	int i, last_index, nr_pages, j;

	for (i = 0; i < log_page->nr_pages; i++)
		pg_addr[i] = kmap(log_page->log_page[i]);
	
	header = (struct log_header *)pg_addr[0];
	nr_pages = tx_cache_cal_nr_log_pages(header->nr_entries, &last_index);
	footer = (struct log_header *)pg_addr[nr_pages - 1];
	footer += last_index;	
	
	TX_CACHE_INFO("header->log_id: %d header->nr_entries: %d, footer->log_id: %d, footer->nr_entries: %d last_index: %d", 
		header->log_id, header->nr_entries, footer->log_id, footer->nr_entries, last_index);
	j = 1;
	for (j = 0; j < nr_pages; j++) {
		entry = (struct log_entry *)pg_addr[j];
		for (i = 0; i < LOG_PACK_MAX; i+=2) {
			if (i % LOG_PACK_MAX == 0)
				entry = (struct log_entry *)pg_addr[j++];
			TX_CACHE_INFO("index: %ld dbn: %ld index: %ld dbn: %ld",
					entry[i].index, entry[i].dbn, entry[i+1].index, entry[i+1].dbn);
		}
	}	

	for (i = 0; i < log_page->nr_pages; i++)
		kunmap(log_page->log_page[i]);
}

static void tx_cache_show_md_log(struct tx_unit *tx)
{
	int i;
	unsigned long len;

	len = tx->nr_blocks + tx->nr_invalidate;
	
	TX_CACHE_INFO("nr_entries: %lu", len);
	for (i = 0; i < len; i+=4) {
		if (len - i == 3) 
			TX_CACHE_INFO("%x:%x %x:%x %x:%x", 
				tx->logs[i].index, tx->logs[i].dbn, tx->logs[i+1].index, tx->logs[i+1].dbn,
				tx->logs[i+2].index, tx->logs[i+2].dbn);
		else if (len - i == 2)
			TX_CACHE_INFO("%x:%x %x:%x", 
				tx->logs[i].index, tx->logs[i].dbn, tx->logs[i+1].index, tx->logs[i+1].dbn);
		else if (len - i == 1)
			TX_CACHE_INFO("%x:%x", tx->logs[i].index, tx->logs[i].dbn);
		else if (len - i == 0)
			break;
		else
			TX_CACHE_INFO("%x:%x %x:%x %x:%x %x:%x", 
				tx->logs[i].index, tx->logs[i].dbn, tx->logs[i+1].index, tx->logs[i+1].dbn,
				tx->logs[i+2].index, tx->logs[i+2].dbn, tx->logs[i+3].index, tx->logs[i+3].dbn);	
	}
}
#else
static inline void tx_cache_show_log_page(struct log_page *log_page)
{
}
static inline void tx_cache_show_md_log(struct tx_unit *tx)
{
}
#endif

/*
 * since aoml is circular queue, 
 * we should handle two cases: normal case and overflowed case.
 * TODO: md_log should avoid to overlap with checkpoint region
 */

static void tx_cache_append_md_log(struct cache_c *dmc, struct log_page *page, const char *debug)
{
	int error, overflowed = 0;
	unsigned long start, elapsed;
	struct eio_io_region region;
	u_int32_t start_index, end_index, eoq;

	tx_cache_show_log_page(page);

	eoq = (dmc->aoml_size / SECTORS_PER_PAGE) - 1;
	
	spin_lock(&dmc->aoml_lock);
	start_index = dmc->aoml_head;
	end_index = start_index + page->nr_pages - 1;
	page->error = 0;
	
	if (end_index >= eoq) {
		overflowed = 1;
		end_index -= eoq;
		dmc->aoml_head = end_index + 1;
	} else {
		dmc->aoml_head += page->nr_pages;

		if (dmc->aoml_head >= eoq) {
			dmc->aoml_head -= eoq;
			TX_CACHE_INFO("new aoml head is %d", dmc->aoml_head);
		}
	}

	dmc->aoml_free -= page->nr_pages;

	if (dmc->aoml_free < AOML_DANGER(dmc->aoml_size)) {
		TX_CACHE_INFO("emergency: free blocks in AOML area are under 10\% of AOML\nlet's enforce compaction");
		atomic_set(&dmc->aoml_emergency, 1);
	}
	
	spin_unlock(&dmc->aoml_lock);
	start = jiffies;

	page->range.begin = start_index;
	page->range.end	= end_index;

	if (overflowed) {
		region.bdev = dmc->cache_dev->bdev;
		region.sector = dmc->md_start_sect + (start_index * SECTORS_PER_PAGE);
		region.count = (eoq - start_index) * SECTORS_PER_PAGE;

		error =
			eio_io_sync_pages(dmc, &region, WRITE, page->log_page, 
					eoq - start_index);
		
		if (error) {
			TX_CACHE_ERROR("error occurred while writing md_logs");
			page->error = error;
			goto out;
		}
		SECTOR_STATS(dmc->eio_stats.ssd_writes, to_bytes(region.count));

		region.sector = dmc->md_start_sect;
		region.count = end_index * SECTORS_PER_PAGE; 

		error =
			eio_io_sync_pages(dmc, &region, WRITE, page->log_page,
					end_index);
		
		if (error) {
			TX_CACHE_ERROR("error occurred while writing md_logs");
			page->error = error;
			goto out;
		}
		SECTOR_STATS(dmc->eio_stats.ssd_writes, to_bytes(region.count));
	} else {
		region.bdev = dmc->cache_dev->bdev;
		region.sector = dmc->md_start_sect + (start_index * SECTORS_PER_PAGE);
		region.count = page->nr_pages * SECTORS_PER_PAGE;
		TX_CACHE_DEBUG("page: %p sector: %x count: %d", page->log_page, region.sector, region.count);
		error =
			eio_io_sync_pages(dmc, &region, WRITE | REQ_SYNC, page->log_page,
					page->nr_pages);
		if (error) {
			TX_CACHE_ERROR("error occurred while writing md_logs");
			page->error = error;
			goto out;
		}
		SECTOR_STATS(dmc->eio_stats.ssd_writes, to_bytes(region.count));
	}
	/* we just count the number of page writes, not sectors */
	atomic64_add(page->nr_pages, &dmc->eio_stats.md_ssd_writes);	
	if (!strcmp(debug, "clean")) 
		atomic64_add(page->nr_pages, &dmc->eio_stats.cleaning_ssd_writes);		
out:
	elapsed = (long)jiffies_to_msecs(jiffies - start);
	TX_CACHE_DEBUG3("(%s) appending md log(nr_pages: %d, start: %u end: %u eoq: %u logid: %d) takes %ld msecs", 
		debug, page->nr_pages, start_index, end_index, eoq, page->log_pack->header.log_id, elapsed);
}

static void tx_cache_append_checkpoint(struct cache_c *dmc, struct log_page *page, const char *debug)
{
	int error, overflowed = 0;
	unsigned long start, elapsed;
	struct eio_io_region region;
	u_int32_t start_index, end_index, eoq;

	tx_cache_show_log_page(page);

	eoq = (dmc->aoml_size / SECTORS_PER_PAGE) + 
		(dmc->checkpoint_size / SECTORS_PER_PAGE) - 1;
	
	spin_lock(&dmc->checkpoint_lock);
	start_index = dmc->checkpoint_head;
	end_index = start_index + page->nr_pages - 1;
	page->error = 0;
	
	if (end_index >= eoq) {
		overflowed = 1;
		end_index -= eoq;
		dmc->checkpoint_head = end_index + 1;
	} else {
		dmc->checkpoint_head += page->nr_pages;

		if (dmc->checkpoint_head >= eoq) {
			dmc->checkpoint_head -= eoq;
			TX_CACHE_INFO("new checkpoint head is %d", dmc->checkpoint_head);
		}
	}
	
	spin_unlock(&dmc->checkpoint_lock);
	start = jiffies;

	page->range.begin = start_index;
	page->range.end	= end_index;

	if (overflowed) {
		region.bdev = dmc->cache_dev->bdev;
		region.sector = dmc->md_start_sect + (start_index * SECTORS_PER_PAGE);
		region.count = (eoq - start_index) * SECTORS_PER_PAGE;

		error =
			eio_io_sync_pages(dmc, &region, WRITE, page->log_page, 
					eoq - start_index);
		
		if (error) {
			TX_CACHE_ERROR("error occurred while writing md_logs");
			page->error = error;
			goto out;
		}
		SECTOR_STATS(dmc->eio_stats.ssd_writes, to_bytes(region.count));

		region.sector = dmc->md_start_sect;
		region.count = end_index * SECTORS_PER_PAGE; 

		error =
			eio_io_sync_pages(dmc, &region, WRITE, page->log_page,
					end_index);
		
		if (error) {
			TX_CACHE_ERROR("error occurred while writing md_logs");
			page->error = error;
			goto out;
		}
		SECTOR_STATS(dmc->eio_stats.ssd_writes, to_bytes(region.count));
	} else {
		region.bdev = dmc->cache_dev->bdev;
		region.sector = dmc->md_start_sect + (start_index * SECTORS_PER_PAGE);
		region.count = page->nr_pages * SECTORS_PER_PAGE;
		TX_CACHE_DEBUG("page: %p sector: %x count: %d", page->log_page, region.sector, region.count);
		error =
			eio_io_sync_pages(dmc, &region, WRITE | REQ_SYNC, page->log_page,
					page->nr_pages);
		if (error) {
			TX_CACHE_ERROR("error occurred while writing md_logs");
			page->error = error;
			goto out;
		}
		SECTOR_STATS(dmc->eio_stats.ssd_writes, to_bytes(region.count));
	}
	/* we just count the number of page writes, not sectors */
	atomic64_add(page->nr_pages, &dmc->eio_stats.md_ssd_writes);	
out:
	elapsed = (long)jiffies_to_msecs(jiffies - start);
	TX_CACHE_DEBUG3("(%s) appending md log(nr_pages: %d, start: %u end: %u eoq: %u) takes %ld msecs", 
		debug, page->nr_pages, start_index, end_index, eoq, elapsed);
}

void tx_cache_make_checkpoint_on_recovery(struct cache_c *dmc)
{
	int nr_entries = 0;
	int i, log_size, nr_log_pages, log_idx, ret;
	struct log_entry *checkpoint;
	struct log_page page;
	struct log_pack *pack;
	void *pg_virt_addr[NR_RECOVERY_PAGE] = {NULL};
	struct md_checkpoint *md_checkpoint = NULL;

	for (i = 0; i < dmc->size; i++) 
		if (TX_CACHE_STATE_TEST(dmc, i, PERSIST))
			nr_entries++;

	checkpoint = (struct log_entry *)kzalloc(sizeof(struct log_entry) * nr_entries, GFP_KERNEL);

	for (i = 0; i < dmc->size; i++) {
		if (TX_CACHE_STATE_TEST(dmc, i, PERSIST)) {
			checkpoint[i].index = i;
			checkpoint[i].dbn = EIO_DBN_GET(dmc, i);
		}
	}

	log_size = nr_entries + 2; //including header and footer
	nr_log_pages = log_size / LOG_PACK_MAX;
	
	if (log_size % LOG_PACK_MAX)
		nr_log_pages++;
	
	TX_CACHE_DEBUG3("nr_log_pages: %d", nr_log_pages);

	pack = (struct log_pack *)kzalloc(sizeof(struct log_pack), GFP_KERNEL);
	pack->logs = checkpoint;

	if (nr_log_pages > NR_MD_PAGE) {
		page.log_page = kmalloc(sizeof(struct page *) * nr_log_pages, GFP_KERNEL);
		TX_CACHE_ERROR("nr_log_pages(%d) are larger than NR_MD_PAGE", nr_log_pages);

		if (!page.log_page) {
			TX_CACHE_ERROR("log_page allocation failed!");
			kfree(pack);
			return -ENOMEM;
		}

		ret = eio_alloc_wb_pages(page.log_page, nr_log_pages);
		
		if (ret) {
			TX_CACHE_ERROR("page allocation failed!");
			kfree(pack);
			kfree(page.log_page);
			page.log_page = NULL;

			return -ENOMEM;
		}
	} else
		page.log_page = dmc->commit_pages;

	pack->header.log_id = atomic_inc_return(&aoml_log_id);
	TX_CACHE_SET_COMPACT_LOG(pack->header.log_type);
	pack->header.nr_entries = log_size;
	
	page.log_pack = pack;
	page.nr_pages = nr_log_pages;

	for (i = 0; i < nr_log_pages; i++) 
		pg_virt_addr[i] = kmap(page.log_page[i]);
	
	log_idx = 0;
	ret = 0;
	
	for (i = 0; i < nr_log_pages; i++) {
		log_idx = tx_cache_packing_md_log(dmc, pg_virt_addr[i], 
						pack, log_idx);
	}
	
	for (i = 0; i < nr_log_pages; i++) 
		kunmap(page.log_page[i]);

	tx_cache_append_checkpoint(dmc, &page, "compaction");
	
	SECTOR_STATS(dmc->eio_stats.compaction_ssd_writes, 
		to_bytes(page.nr_pages * SECTORS_PER_PAGE));
	atomic64_add(page.nr_pages, &dmc->nr_ckpt_pages);
	
	md_checkpoint = (struct md_checkpoint *)kmalloc(sizeof(struct md_checkpoint), GFP_KERNEL);
	EIO_ASSERT(md_checkpoint != NULL);

	md_checkpoint->log_id = page.log_pack->header.log_id;
	md_checkpoint->range.begin = page.range.begin;
	md_checkpoint->range.end = page.range.end;

	list_add_tail(&md_checkpoint->list, &dmc->checkpoint_list);
	atomic_inc(&dmc->nr_checkpoints);

	EIO_ASSERT(atomic64_read(&dmc->nr_mdlog_pages) >= 0);
	
	kfree(page.log_pack->logs); 
	kfree(page.log_pack); 

	if (nr_log_pages > NR_MD_PAGE) {
		eio_free_wb_pages(page.log_page, page.nr_pages);
		kfree(page.log_page);
	}

	if (page.error) 
		TX_CACHE_ERROR("error occurred while append md_log");
}


static int tx_cache_log_entry_compare_by_index(const void *lhs, const void *rhs)
{
	struct log_sort_node *l_entry, *r_entry;

	l_entry = (struct log_sort_node *)lhs;
	r_entry = (struct log_sort_node *)rhs;

	if (l_entry->entry->index < r_entry->entry->index) 
		return 1;
	if (l_entry->entry->index > r_entry->entry->index)
		return -1;

	EIO_ASSERT(l_entry->entry->index == r_entry->entry->index);
	EIO_ASSERT(l_entry->log_id != r_entry->log_id);	
	
	/* indexes are same, let's compare log_id */
	if (l_entry->log_id < r_entry->log_id)
		return 1;
	if (l_entry->log_id > r_entry->log_id)
		return -1;
	/* never reached here */	
	BUG();
	return 0;
}

static int tx_cache_log_entry_compare_by_dbn(const void *lhs, const void *rhs)
{
	struct log_sort_node *l_entry, *r_entry;

	l_entry = (struct log_sort_node *)lhs;
	r_entry = (struct log_sort_node *)rhs;

	if (l_entry->entry->dbn < r_entry->entry->dbn) 
		return 1;
	if (l_entry->entry->dbn > r_entry->entry->dbn)
		return -1;

	EIO_ASSERT(l_entry->entry->dbn == r_entry->entry->dbn);
	//EIO_ASSERT(l_entry->log_id != r_entry->log_id);	
	
	/* DBNs are same, let's compare log_id */
	if (l_entry->log_id < r_entry->log_id)
		return 1;
	if (l_entry->log_id > r_entry->log_id)
		return -1;
	
	/* 
	 * corner case: DBNs of entries are 0 and log_ids are same, 
	 * we must not remove any of two entries
	 */	

	EIO_ASSERT(l_entry->log_id == r_entry->log_id);

	if (l_entry->entry->dbn == 0 && r_entry->entry->dbn == 0) {
		EIO_ASSERT(l_entry->entry->index != r_entry->entry->index);

		if (l_entry->entry->index > r_entry->entry->index) 
			return -1;
		else
			return 1;
	} else {
		TX_CACHE_ERROR("i will die: (%d-%lu) (%d-%lu)", l_entry->log_id, l_entry->entry->dbn, r_entry->log_id, r_entry->entry->dbn);

		BUG();
	}
	
	return 0;
}

static unsigned tx_cache_count_unique_set_by_index(struct log_sort_node *pool, unsigned total)
{
	int i;
	unsigned count = 0, clean = 0, skipped = 0;
	u_int64_t last_index;

	last_index = pool[0].entry->index;
	count++;

	for (i = 1; i < total; i++) {
		if (last_index == pool[i].entry->index) {	
			skipped++;
			continue;
		}
		
		if (TX_CACHE_GET_LOG_TYPE(pool[i].log_type) == CLEAN_LOG) 
			clean++;
		
		count++;
		last_index = pool[i].entry->index;
	}

	TX_CACHE_DEBUG2("clean entries: %u skipped: %u", clean, skipped);
	return count;
}

static unsigned tx_cache_count_unique_set_by_dbn(struct log_sort_node *pool, unsigned total)
{
	int i;
	unsigned count = 0, clean = 0, skipped = 0, invalidate = 0;
	u_int64_t last_index, last_dbn;

	last_dbn = pool[0].entry->dbn;
	last_index = pool[0].entry->index;
	count++;
	
	for (i = 1; i < total; i++) {
		if (TX_CACHE_GET_LOG_TYPE(pool[i].log_type) == CLEAN_LOG) {
			clean++;
			count++;
			continue;
		}
		
		if (last_dbn == pool[i].entry->dbn) {
			if (last_dbn == 0 && pool[i].entry->index != last_index) {
				invalidate++;
				count++;
			} else
				skipped++;
			continue;
		}
		
		count++;
		last_dbn = pool[i].entry->dbn;
		last_index = pool[i].entry->index;
	}

	TX_CACHE_DEBUG2("clean entries: %u skipped: %u invalidated: %u", clean, skipped, invalidate);
	return count;
}

static void tx_cache_merge_cardinal_set_by_index(struct log_sort_node *pool, unsigned total, unsigned cardinal)
{
	int i, j;
	u_int64_t last_index;

	last_index = pool[0].entry->index;
	j = 1;

	for (i = 1; i < total; i++)	{
		if (last_index == pool[i].entry->index)
			continue;
	
		pool[j] = pool[i];
		last_index = pool[i].entry->index;
		j++;
	}

	EIO_ASSERT(j == cardinal);
}

static void tx_cache_merge_cardinal_set_by_dbn(struct log_sort_node *pool, unsigned total, unsigned cardinal)
{
	int i, j;
	u_int64_t last_dbn, last_index;

	last_dbn = pool[0].entry->dbn;
	j = 1;

	for (i = 1; i < total; i++)	{
		if (TX_CACHE_GET_LOG_TYPE(pool[i].log_type) == CLEAN_LOG) {
			pool[j] = pool[i];
			j++;
			continue;
		}

		if (last_dbn == pool[i].entry->dbn) 
			if (last_dbn != 0)
				continue;

		pool[j] = pool[i];
		last_dbn = pool[i].entry->dbn;
		last_index = pool[i].entry->index;
		j++;
	}

	EIO_ASSERT(j == cardinal);
}

static void tx_cache_log_entry_swap(void *a, void *b, int size)
{
	struct log_sort_node temp;

	temp = *(struct log_sort_node *)a;
	*(struct log_sort_node *)a = *(struct log_sort_node *)b;
	*(struct log_sort_node *)b = temp;
}

static unsigned tx_cache_dedup_by_index(struct log_sort_node *pool, unsigned total)
{
	unsigned cardinality;

	TX_CACHE_DEBUG2("before sort");
	sort(pool, total, sizeof(struct log_sort_node), 
		&tx_cache_log_entry_compare_by_index, 
		&tx_cache_log_entry_swap);
	
	/* count unique index */
	cardinality = tx_cache_count_unique_set_by_index(pool, total);
	TX_CACHE_DEBUG2("cardinatlity: %d", cardinality);

	/* make cardinal set */
	tx_cache_merge_cardinal_set_by_index(pool, total, cardinality);

	return cardinality;
}

static unsigned tx_cache_dedup_by_dbn(struct log_sort_node *pool, unsigned total)
{
	unsigned cardinality;
	
	TX_CACHE_DEBUG2("before sort");
	sort(pool, total, sizeof(struct log_sort_node), 
		&tx_cache_log_entry_compare_by_dbn, 
		&tx_cache_log_entry_swap);
	
	/* count unique index */
	cardinality = tx_cache_count_unique_set_by_dbn(pool, total);
	TX_CACHE_DEBUG2("cardinatlity: %d", cardinality);

	/* make cardinal set */
	tx_cache_merge_cardinal_set_by_dbn(pool, total, cardinality);

	return cardinality;
}

static inline void tx_cache_copy_merged_set(struct log_sort_node *pool, struct log_entry *compacted, unsigned total)
{
	int i;

	for (i = 0; i < total; i++) 
		compacted[i] = *(pool[i].entry);
}

static struct log_entry *tx_cache_get_cardinal_set(struct list_head *list, unsigned total, unsigned *cardinal)
{
	int i, pool_index = 0;
	struct log_pack *pack;
	struct log_sort_node *pool;
	struct log_entry *compacted_entries;
	unsigned cardinality;
	
	pool = (struct log_sort_node *)kmalloc(sizeof(struct log_sort_node) * total, GFP_KERNEL);

	if (pool == NULL) {
		TX_CACHE_ERROR("pool allocation failed");
		return NULL;
	}

	TX_CACHE_DEBUG2("before pooling, total entries: %u", total);
	/* since we select more recent log, we read logs backward */
	list_for_each_entry_reverse(pack, list, log_list) {
		for (i = 0; i < pack->nr_entries; i++, pool_index++)	
			pool[pool_index] = pack->sort_logs[i];
	}
	/*
	for (i = 0; i < total; i+=4)
		TX_CACHE_DEBUG2("%d:%d:%d:%d  %d:%d:%d:%d  %d:%d:%d:%d  %d:%d:%d:%d",
			pool[i].index, pool[i].dbn, pool[i].log_id, pool[i].log_type,
			pool[i+1].index, pool[i+1].dbn, pool[i+1].log_id, pool[i+1].log_type,
			pool[i+2].index, pool[i+2].dbn, pool[i+2].log_id, pool[i+2].log_type,
			pool[i+3].index, pool[i+3].dbn, pool[i+3].log_id, pool[i+3].log_type
			);
	*/

	/* step1: deduplication by index */
	cardinality = tx_cache_dedup_by_index(pool, total);

	/* step2: deduplication by dbn */
	cardinality = tx_cache_dedup_by_dbn(pool, cardinality);
	*cardinal = cardinality;
	compacted_entries = (struct log_entry *)kmalloc(sizeof(struct log_entry) * cardinality, GFP_KERNEL);

	if (compacted_entries == NULL) {
		TX_CACHE_ERROR("compacted entreis allocation failed!");
		goto pool_free;
	}

	tx_cache_copy_merged_set(pool, compacted_entries, cardinality);

pool_free:
	kfree(pool);

	return compacted_entries;
}

unsigned tx_cache_cal_nr_log_pages(u_int64_t nr_entries, unsigned *last_index)
{
	unsigned nr_pages = 0;
	
	if (nr_entries <= LOG_PACK_MAX) { 
		if (last_index != NULL)
			*last_index = nr_entries - 1;
		return 1;
	}
	
	//nr_pages = 1;
	//nr_entries -= LOG_PACK_MAX_HEAD;
	
	nr_pages = nr_entries / LOG_PACK_MAX;
	
	if (last_index != NULL)
		*last_index = (nr_entries - 1) % LOG_PACK_MAX;
	
	if (nr_entries % LOG_PACK_MAX)
		nr_pages++;

	return nr_pages;
}

/* if log is valid; return 0, otherwise return 1 */
int tx_cache_log_is_valid(struct log_header *header, struct log_header *footer)
{
	if ((header->log_type == footer->log_type) && 
		(header->log_id == footer->log_id) &&
		(header->nr_entries == footer->nr_entries))
		return 0;
	return 1;
}

static int tx_cache_copy_log_to_sort_node(struct log_pack *pack, struct log_entry *entries, int type, int index, u_int32_t log_type)
{
	int i;

	EIO_ASSERT(pack != NULL);
	EIO_ASSERT(entries != NULL);
	
	if (type == PAGE_HEAD) {
		for (i = 0; index < pack->nr_entries && i < LOG_PACK_MAX_HEAD; i++) {
			pack->sort_logs[index].entry = &entries[i+1];
			pack->sort_logs[index].log_id = pack->header.log_id;
			pack->sort_logs[index].log_type = log_type;
			index++;
		}
	} else if (type == PAGE_FOOT) {
		for (i = 0; index < pack->nr_entries && i < LOG_PACK_MAX_HEAD; i++) {
			pack->sort_logs[index].entry = &entries[i];
			pack->sort_logs[index].log_id = pack->header.log_id;
			pack->sort_logs[index].log_type = log_type;
			index++;
		}
	} else {
		for (i = 0; index < pack->nr_entries && i < LOG_PACK_MAX; i++) {
			pack->sort_logs[index].entry = &entries[i];
			pack->sort_logs[index].log_id = pack->header.log_id;
			pack->sort_logs[index].log_type = log_type;
			index++;
		}
	}

	return index;
}

static int tx_cache_do_compaction(struct cache_c *dmc, struct log_page *log_page, int pg_read)
{
	struct log_header *header, *footer;
	struct log_pack *pack, *compacted_pack, *pack_free;
	struct list_head log_list;
	void *src_virt_addr[NR_MDLOGS_COMPACT] = {NULL};
	unsigned nr_pages, last_index = 0, total_entries = 0, cardinality = 0;
	int i, log_index, pg_index, stopped_pg_index, end_pg_index;
	u_int32_t largest_log_id = 0;

	INIT_LIST_HEAD(&log_list);
	compacted_pack = NULL;

	for (pg_index = 0; pg_index < pg_read; pg_index++) 
		src_virt_addr[pg_index] = kmap(dmc->compaction_pages[pg_index]);

	stopped_pg_index = pg_index = log_index = 0;
	for (i = 0; i < pg_read;) {
		header = (struct log_header *)src_virt_addr[pg_index];	
		nr_pages = tx_cache_cal_nr_log_pages(header->nr_entries, &last_index);
		end_pg_index = pg_index + nr_pages - 1;
		TX_CACHE_DEBUG("nr_entries: %lu nr_pages: %d end_pg_index: %d", header->nr_entries, nr_pages, end_pg_index);

		if (end_pg_index >= NR_MDLOGS_COMPACT) {
			/* end condition:
			 * last log can cross NR_MDLOGS_COMPACT
			 * in this case, we just skip last mdlog
			 */
			TX_CACHE_DEBUG2("log(%u) will be compacted at next compaction period (end_pg_index: %u, pg_index: %u nr_pages: %u",
				header->log_id, end_pg_index, pg_index, nr_pages);
			break;
		}
	
		footer = (struct log_header *)src_virt_addr[end_pg_index];
		footer += last_index;
		//footer = (struct log_header *)&src_virt_addr[end_pg_index][last_index];

		TX_CACHE_DEBUG("header->log_id: %d header->log_type: %d header->nr_entries: %ld footer->log_id: %d footer->log_type: %d footer->nr_entries: %ld nr_pages: %d",
			header->log_id, header->log_type, header->nr_entries, 
			footer->log_id, footer->log_type, footer->nr_entries, nr_pages);	
		
		if (tx_cache_log_is_valid(header, footer)) {
			TX_CACHE_ERROR("log(%u) is invalid", header->log_id);
			goto err;
		}

		pack = (struct log_pack *)kmalloc(sizeof(struct log_pack), GFP_KERNEL);
		pack->header = *header;
		pack->nr_entries = header->nr_entries - 2;	//excluding header and footer
		list_add(&pack->log_list, &log_list);
		total_entries += pack->nr_entries;
		
		if (header->log_id > largest_log_id)
			largest_log_id = header->log_id;

		pack->sort_logs = (struct log_sort_node *)kmalloc(
			sizeof(struct log_sort_node) * pack->nr_entries,
			GFP_KERNEL);

		EIO_ASSERT(pack->sort_logs != NULL);

		log_index = 0;
		pg_index = stopped_pg_index;

		while (pg_index <= end_pg_index) {
			int type;

			if (pg_index == stopped_pg_index)
				type = PAGE_HEAD;
			else if (pg_index == end_pg_index)
				type = PAGE_FOOT;
			else
				type = PAGE_NORM;

			log_index = tx_cache_copy_log_to_sort_node(pack, (struct log_entry *)src_virt_addr[pg_index], 
				type, log_index, header->log_type);
			pg_index++;
		}
		TX_CACHE_DEBUG2("stopped_pg_index: %d, pg_index: %d", stopped_pg_index, pg_index);	
		stopped_pg_index = pg_index;
		i += nr_pages;
	}
	
	compacted_pack = (struct log_pack *)kmalloc(sizeof(struct log_pack), GFP_KERNEL);
	compacted_pack->logs = tx_cache_get_cardinal_set(&log_list, total_entries, &cardinality);
	TX_CACHE_DEBUG3("the number of unique set is %u", cardinality);

	EIO_ASSERT(compacted_pack != NULL && compacted_pack->logs != NULL);
	
	TX_CACHE_SET_COMPACT_LOG(compacted_pack->header.log_type);
	compacted_pack->header.log_id = largest_log_id;
	compacted_pack->nr_entries = 
		compacted_pack->header.nr_entries = cardinality + 2;//including header and footer

	list_for_each_entry_safe(pack, pack_free, &log_list, log_list) {
		kfree(pack->sort_logs);
		list_del(&pack->log_list);
		kfree(pack);
	}

err:
	for (i = 0; i < pg_read; i++) 
		kunmap(dmc->compaction_pages[i]);
	
	log_page->log_pack = compacted_pack;
	log_page->nr_pages = tx_cache_cal_nr_log_pages(compacted_pack->nr_entries, NULL);	

	return stopped_pg_index;
}

#define IS_IN_MD_RANGE(point, range) (point >= range.begin && point <= range.end)

enum {
	INTERSECT_BEGIN = 0,
	INTERSECT_END,
	INTERSECT_BOTH,
	SUBSET_LEFTMOST,
	SUBSET,
	DISJOINT,
};

static inline int __md_range_overflow(u_int32_t begin, u_int32_t end)
{
	if (begin > end)
		return 1;
	return 0;	
}

static inline int MD_RANGE_OVERFLOW(struct md_range range) 
{
	return __md_range_overflow(range.begin, range.end);
}

static inline int MD_RANGE_CHECK(struct md_range range, u_int32_t begin, u_int32_t end, int overflowed, int eoq)
{
	/* 
	 * case 1:
	 * 1.1
	 * |<--------------scan area------------->|
	 * |   <--checkpoint range-->             |
	 * or
	 * 1.2
	 * |<--------------scan area------------->|
	 * |<--checkpoint range-->                |
	 * or
	 * 1.3
	 * |--->    <------------scan area--------|
	 * |         <--checkpoint range-->       |
	 * or	 
	 * 1.4
	 * |--->    <------------scan area--------|
	 * |        <--checkpoint range-->        |
	 * or 
	 * 1.5
	 * |----scan area------------>         <--|
	 * |<--checkpoint range-->                |
	 * or
	 * 1.6
	 * |----scan area------------>         <--|
	 * |-checkpoint range-->                <-|
	 */
	if (range.begin == begin)
		return SUBSET_LEFTMOST;

	if (overflowed) {
		//if (range.begin >= begin && range.end <= eoq)
		if (MD_RANGE_OVERFLOW(range) && range.begin >= begin && range.end <= end)
			return SUBSET;
		else if (!MD_RANGE_OVERFLOW(range) && range.end <= end)
			return SUBSET;
		else if (!MD_RANGE_OVERFLOW(range) && range.begin > begin && range.end < eoq)
			return SUBSET;
	}

	if (range.begin > begin && range.end <= end)
		return SUBSET;

	/* 
	 * case 2: begin of checkpoint range intersects scanning area
	 * |<------------scan area---------->       |
	 * |                  <--checkpoint range-->|
	 * or
	 * |----->               <-----scan area----|
	 * |-checkpoint range-->                  <-|
	 * or
	 * |---->                  <-----scan area--|
	 * |<--checkpoint range-->                  |
	 */
	if (overflowed) {
		if (MD_RANGE_OVERFLOW(range) && range.begin >= begin && range.end >= end)
			return INTERSECT_BEGIN;
		if (!MD_RANGE_OVERFLOW(range) && range.begin <= end && range.end >= end) 
			return INTERSECT_BEGIN;
	}

	if (!overflowed && range.begin > begin && range.begin <= end) { 
		if (MD_RANGE_OVERFLOW(range) && range.end < begin)	
			return INTERSECT_BEGIN;
		if (!MD_RANGE_OVERFLOW(range) && range.end > end)
			return INTERSECT_BEGIN;
	}

	/*
	 * case 3: begin and end of checkpoint range intersect scanning area
	 * |<------------scan area---------->       |
	 * |-->                  <--checkpoint range|
	 * or
	 * |--->       <-----------scan area--------|
	 * |<--checkpoint range-->                  |
	 * or
	 * |--->       <-----------scan area--------|
	 * |-checkpoint range-->                  <-|
	 */
	 if (overflowed) {
	 	if (MD_RANGE_OVERFLOW(range) && range.end >= begin && range.begin > begin && range.begin <= end)
	 		return INTERSECT_BOTH;
	 	if (!MD_RANGE_OVERFLOW(range) && range.begin <= end && range.end >= begin)
	 		return INTERSECT_BOTH;
	 }
	 
	 if (!overflowed) {

	 }
	 	
	/* 
	 * case 4: end of checkpoint range intersects scanning area
	 * |         <----------scan area----------->|
	 * |<--checkpoint range-->                   |
	 * or
	 * |---->                    <----scan area--|
	 * |         <--checkpoint range-->          |
	 * or
	 * |            <-------scan area------->    |
	 * |checkpoint range-->                   <--|
	 * or
	 * | area-------->                    <--scan|
	 * |int range-->                   <--checkpo|
	 */
	if (overflowed) {
		if (MD_RANGE_OVERFLOW(range) && range.end >= end && range.begin > begin && range.end < begin)
			return INTERSECT_END;
		if (!MD_RANGE_OVERFLOW(range) && range.begin < begin && range.end >= begin && range.end >= end)
			return INTERSECT_END;
	}
	
	if (!overflowed && range.end < end && range.end >= begin) {
		if (MD_RANGE_OVERFLOW(range) && range.begin >= end)
			return INTERSECT_END;
		if (!MD_RANGE_OVERFLOW(range) && range.begin < begin)
			return INTERSECT_END;
	}

	/* 
	 * case 5:
	 * |                         <----scan area---->|
	 * |<--checkpoint range-->       			    |
	 * or
	 * |<----scan area---->       				    |
	 * |                      <--checkpoint range-->|
	 */
	return DISJOINT;
}

static inline int __md_range_length(u_int32_t eoq, u_int32_t begin, u_int32_t end)
{
	int length = 0;
	
	if (__md_range_overflow(begin, end)) // overflow case
		length = eoq - begin + end + 1;	
		//length = (eoq - begin + 1) + end + 1;	
	else
		length = end - begin + 1;
	return length;
}

static inline int MD_RANGE_LENGTH(u_int32_t eoq, struct md_range range)
{
	return __md_range_length(eoq, range.begin, range.end);
}

static int tx_cache_get_scanning_area(struct cache_c *dmc, struct md_range *range, u_int32_t begin_idx)
{
	struct md_checkpoint *check;
	int i = 0;
	int overflowed = 0;
	int scan_budget = NR_MDLOGS_COMPACT;
	u_int32_t begin, end, eoq;

	eoq = (dmc->aoml_size / SECTORS_PER_PAGE) - 1;

	begin = begin_idx;
	end = begin_idx + NR_MDLOGS_COMPACT - 1;

	if (end >= eoq) {
		end -= eoq;
		overflowed = 1;
	}

	if (overflowed) {
		range[0].begin = begin;
		range[0].end = eoq - 1;
		range[1].begin = 0;
		range[1].end = end;

		return 1;
	} 
	
	range[0].begin = begin;
	range[0].end = end;

	return 0;

	/* below codes are deprecated */
	if (list_empty(&dmc->checkpoint_list)) {
		if (end >= eoq) {
			end -= eoq;

			range[0].begin = begin_idx;
			range[0].end = eoq - 1;
			range[1].begin = 0;
			range[1].end = end;

			return 1;
		} else {
			range[0].begin = begin_idx;
			range[0].end = end;

			return 0;
		}
	}

	/* step1: select beginning point */
	list_for_each_entry(check, &dmc->checkpoint_list, list) {
		if (IS_IN_MD_RANGE(begin_idx, check->range)) {
			begin = check->range.end + 1;	
			end = begin + NR_MDLOGS_COMPACT - 1;
		}
	}

	if (end >= eoq) {
		/*
		range[i].begin = begin;
		range[i].end = eoq - 1;
		begin = 0;
		end -= eoq;
		i++;
		*/
		end -= eoq;
		overflowed = 1;
	}
	

	/* step2: check other checkpoints in scanning area */
	list_for_each_entry(check, &dmc->checkpoint_list, list) {
retry:
		switch (MD_RANGE_CHECK(check->range, begin, end, overflowed, eoq)) {
			case SUBSET_LEFTMOST:
				begin = check->range.end + 1;	
				if (begin == eoq) 
					begin = 0;
				
				end = begin + scan_budget - 1;
				
				if (end >= eoq) {
					overflowed = 1;
					//range[i].begin = begin;
					//range[i].end = eoq - 1;
					//begin = 0;
					end -= eoq;
					//i++;
				} else 
					overflowed = 0;
				continue;
			case SUBSET:
				range[i].begin = begin;
				if (overflowed) {
					if (MD_RANGE_OVERFLOW(check->range))
						range[i].end = check->range.begin - 1;
					else {
						/*
						 * |--->    <------------scan area--------|
						 * |         <--checkpoint range-->       |
						 */
						if (check->range.begin > end) {
							range[i].end = check->range.begin - 1;
						}
						/*
						 * |----scan area------------>         <--|
						 * |<--checkpoint range-->                |
						 */
						else if (check->range.begin == 0)
							range[i].end = eoq - 1;
						/*
						 * |----scan area------------>         <--|
						 * |  <--checkpoint range-->              |
						 */
						else {
							range[i].end = eoq - 1;
							scan_budget -= MD_RANGE_LENGTH(eoq, range[i]);
							i++;
							range[i].begin = 0;
							range[i].end = check->range.begin - 1;
						}
					}
					
					/* case 1.5 */
					scan_budget -= MD_RANGE_LENGTH(eoq, range[i]);
				
					if (check->range.end == eoq - 1)
						begin = 0;
					else
						begin = check->range.end + 1;
					end = begin + scan_budget - 1;
				} else {
					range[i].end = check->range.begin - 1;

					scan_budget -= MD_RANGE_LENGTH(eoq, range[i]);
					begin = check->range.end + 1;
					end = begin + scan_budget - 1;
				}
				break;
			case INTERSECT_BEGIN:
				range[i].begin = begin;
				if (overflowed) {
					if (check->range.begin == 0)
						range[i].end = eoq - 1;
					else {
						/*
						 * |---->                   <-----scan area--|
						 * | <--checkpoint range-->                  |
						 */
						if (check->range.end < range[i].begin && !MD_RANGE_OVERFLOW(check->range)) {
							range[i].end = eoq - 1;
							scan_budget -= MD_RANGE_LENGTH(eoq, range[i]);
							i++;
							range[i].begin = 0;
						}
						
						range[i].end = check->range.begin - 1;
					}
				} else
					range[i].end = check->range.begin - 1;
				
				scan_budget -= MD_RANGE_LENGTH(eoq, range[i]);
					
				if (check->range.end == eoq - 1)
					begin = 0;
				else
					begin = check->range.end + 1;
					
				end = begin + scan_budget - 1;
				break;	
			case INTERSECT_END:
				if (overflowed) {
					if (check->range.end == eoq - 1)
						begin = 0;
					else
						begin = check->range.end + 1;
				} else 
					begin = check->range.end + 1;

				end = begin + scan_budget - 1;
				
				if (end >= eoq) {
					overflowed = 1;
					end -= eoq;
				}
				continue;
			case INTERSECT_BOTH:
				/* this case very hard to occur, make INTERSECT_BEGIN case */
				TX_CACHE_DEBUG("INSERSECT_BOTH: begin(%d) end(%d)", begin, end);
				if (overflowed) {
					if (check->range.end == eoq - 1)
 						begin = 0;
					else 
						begin = check->range.end + 1;
				} else 
					begin = check->range.end + 1;
				
				end = begin + scan_budget - 1;

				if (end >= eoq) {
					overflowed = 1;
					end -= eoq;
				}
				goto retry;
			case DISJOINT:
				/*
				range[i].begin = begin;
				
				if (overflowed) {
					range[i].end = eoq;
					i++;
					range[i].begin = 0;
					end -= eoq;
				}

				range[i].end = end;
				goto range_end;
				*/
				continue;
		}//switch
	
		overflowed = 0;
		if (end >= eoq) {
			overflowed = 1;
			end -= eoq;
		}
		i++;
	}//list_for_each_entry

	range[i].begin = begin;

	if (overflowed) {
		range[i].end = eoq - 1;
		i++;
		range[i].begin = 0;
		end -= eoq;
	}

	range[i].end = end;

//range_end:
	TX_CACHE_DEBUG("endindex: %d begin: %d end: %d", i, range[i].begin, range[i].end);
	return i;
}

/* 
 * tx_cache_compact_md_log: 
 * do compact accumlated md logs into single checkpoint.
 */

static int tx_cache_compact_md_log(struct cache_c *dmc)
{
	unsigned long begin, flags = 0;
	u_int32_t begin_idx, eoq = 0;
	struct eio_io_region region;
	void *pg_virt_addr[NR_MDLOGS_COMPACT] = {NULL};
	struct log_page log_page;
	int error, i, stopped_page, log_idx;
	int page_idx;
	int compacted_pages, range_idx_end = 0;
	int end_page;
	struct md_checkpoint *checkpoint = NULL;
	struct md_range compact_range[NR_MDLOGS_COMPACT] = {{0,0},};
	
	begin = jiffies;

	spin_lock_irqsave(&dmc->aoml_lock, flags);
	begin_idx = dmc->aoml_rear;

	range_idx_end = tx_cache_get_scanning_area(dmc, compact_range, begin_idx);
	TX_CACHE_DEBUG("range_idx_end is %d", range_idx_end);
	
	for (i = 0; i <= range_idx_end; i++) 
		TX_CACHE_DEBUG("start: %d end: %d", compact_range[i].begin, compact_range[i].end);

	/*
	end_idx = begin_idx + NR_MDLOGS_COMPACT - 1;
	 */
	eoq = (dmc->aoml_size / SECTORS_PER_PAGE) - 1;
	
	spin_unlock_irqrestore(&dmc->aoml_lock, flags);
	
	region.bdev = dmc->cache_dev->bdev;
	page_idx = 0;
	
	/* step1: read from aoml in SSD */
	for (i = 0; i <= range_idx_end; i++) {
		struct page **pages;
	
		region.sector = dmc->md_start_sect + compact_range[i].begin * SECTORS_PER_PAGE;
		region.count = MD_RANGE_LENGTH(eoq, compact_range[i]) * SECTORS_PER_PAGE;
		pages = &dmc->compaction_pages[page_idx];
		page_idx += MD_RANGE_LENGTH(eoq, compact_range[i]);
		
		error = eio_io_sync_pages(dmc, &region, READ, pages, 
			MD_RANGE_LENGTH(eoq, compact_range[i]));
		
		if (error) {
			TX_CACHE_ERROR("error occurred while scanning md_logs");
			goto error;
		}
		
		SECTOR_STATS(dmc->eio_stats.ssd_reads, to_bytes(region.count));
		SECTOR_STATS(dmc->eio_stats.compaction_ssd_reads, to_bytes(region.count));
	}
	
	/* step2: do compaction */
	stopped_page = tx_cache_do_compaction(dmc, &log_page, page_idx);
	compacted_pages = stopped_page;
	end_page = 0;

	/* stopped_page is relative position in dmc->compaction_page 
	 * so we should find address of md_log at AOML area 
	 */
	for (i = 0; i <= range_idx_end; i++) {
		if (end_page + MD_RANGE_LENGTH(eoq, compact_range[i]) >= stopped_page) {
			stopped_page -= end_page;
			end_page = compact_range[i].begin + stopped_page;
			break;
		}
		
		end_page += MD_RANGE_LENGTH(eoq, compact_range[i]);	
	}

	/* overflowed case */
	if (end_page >= eoq)
		end_page -= eoq;
	
	spin_lock_irqsave(&dmc->aoml_lock, flags);
	dmc->aoml_rear = end_page;
	spin_unlock_irqrestore(&dmc->aoml_lock, flags);
	TX_CACHE_DEBUG("aoml_rear: %d", dmc->aoml_rear);

	/* step3: write compacted log to head of aoml area in SSD */
	for (i = 0; i < log_page.nr_pages; i++) 
		pg_virt_addr[i] = kmap(dmc->compaction_pages[i]);
	
	log_idx = 0;	
	for (i = 0; i < log_page.nr_pages; i++) {
		log_idx = tx_cache_packing_md_log(dmc, pg_virt_addr[i], 
						log_page.log_pack, log_idx);
	}

	for (i = 0; i < log_page.nr_pages; i++) 
		kunmap(dmc->compaction_pages[i]);
	
	TX_CACHE_DEBUG3("log compaction takes %lu msec", jiffies_to_msecs(jiffies - begin));
	
	log_page.log_page = dmc->compaction_pages;	
	//tx_cache_append_md_log(dmc, &log_page, "compaction");
	tx_cache_append_checkpoint(dmc, &log_page, "compaction");

	/* TODO: nr_mdlog_pages or other stats should count 
		the number of compacted pages */

	SECTOR_STATS(dmc->eio_stats.compaction_ssd_writes, 
		to_bytes(log_page.nr_pages * SECTORS_PER_PAGE));
	atomic64_sub(compacted_pages, &dmc->nr_mdlog_pages);
	atomic64_add(log_page.nr_pages, &dmc->nr_ckpt_pages);
	TX_CACHE_DEBUG3("nr_mdlog_pages: %ld, nr_ckpt_pages: %ld", 
		atomic64_read(&dmc->nr_mdlog_pages), 
		atomic64_read(&dmc->nr_ckpt_pages));

	checkpoint = (struct md_checkpoint *)kmalloc(sizeof(struct md_checkpoint), GFP_KERNEL);
	EIO_ASSERT(checkpoint != NULL);

	checkpoint->log_id = log_page.log_pack->header.log_id;
	checkpoint->range.begin = log_page.range.begin;
	checkpoint->range.end = log_page.range.end;
	TX_CACHE_DEBUG("checkpoint log_id(%u) range(%u:%u)", checkpoint->log_id, checkpoint->range.begin, checkpoint->range.end);

	list_add_tail(&checkpoint->list, &dmc->checkpoint_list);
	atomic_inc(&dmc->nr_checkpoints);

	EIO_ASSERT(atomic64_read(&dmc->nr_mdlog_pages) >= 0);
	
	kfree(log_page.log_pack->logs); 
	kfree(log_page.log_pack); 

	if (log_page.error) {
		TX_CACHE_ERROR("error occurred while append md_log");
		return -EINVAL;
	}

error:
	//return log_page.nr_pages;
	return compacted_pages;
}

/* merge compacted checkpoints 
 * in merge process, all the invalidate/clean logs are removed
 */

int tx_cache_merge_checkpoints(struct cache_c *dmc)
{
	return 0;		
}

int tx_cache_compaction_thread_proc(void *context)
{
	struct cache_c *dmc = (struct cache_c *)context;
	unsigned long flags = 0;
	unsigned nr_pages;
	int reclaimed = 0;
	int merge = 0, merged;

	dmc->compaction_thread_running = 1;
	set_user_nice(current, 5);

	while (!dmc->sysctl_active.fast_remove) {
		struct compaction_msg *msg;
		LIST_HEAD(compaction_list);
		
		spin_lock_irqsave(&dmc->compaction_lock, flags);
		
		while (list_empty(&dmc->compaction_q) && !dmc->sysctl_active.fast_remove)
			EIO_WAIT_EVENT(&dmc->compaction_event, &dmc->compaction_lock, flags);

		/* time to exit */
		if (dmc->sysctl_active.fast_remove) {
			TX_CACHE_INFO("exit path");
			spin_unlock_irqrestore(&dmc->compaction_lock, flags);
			break;
		}

		list_add(&compaction_list, &dmc->compaction_q);
		list_del_init(&dmc->compaction_q);
		//INIT_LIST_HEAD(&dmc->compaction_q);
		
		spin_unlock_irqrestore(&dmc->compaction_lock, flags);
		
		atomic_inc(&dmc->in_compaction);
		msg = (struct compaction_msg *) list_entry((&compaction_list)->next, 
				struct compaction_msg, mlist);
		list_del(&msg->mlist);
		TX_CACHE_DEBUG("msg: %ld, %ld", msg->issue_time, msg->nr_logs);
		
		/* we handle the last msg only */
		while (!list_empty(&compaction_list)) {
			kfree(msg);
			msg = (struct compaction_msg *) list_entry((&compaction_list)->next, 
					struct compaction_msg, mlist);
			if (msg->type == NEED_MERGING)
				merge = 1;
			list_del(&msg->mlist);
		}

		kfree(msg);
compaction_required:	
		/* check the free blocks of aoml
		 * if cache in an emergent state, compaction enforced and block the commit 
		 */
		if (atomic_read(&dmc->aoml_emergency) == 1)
			down_write(&dmc->reclaimer_lock);
		nr_pages = tx_cache_compact_md_log(dmc);

		spin_lock_irqsave(&dmc->aoml_lock, flags);	
			dmc->aoml_free += nr_pages;

			if (dmc->aoml_free > AOML_WARN(dmc->aoml_size)) {
				atomic_set(&dmc->aoml_emergency, 0);
				reclaimed = 1;
			}
		spin_unlock_irqrestore(&dmc->aoml_lock, flags);
	
		if (reclaimed) {
			up_write(&dmc->reclaimer_lock);
			reclaimed = 0;
		}
	
		if (atomic64_read(&dmc->nr_mdlog_pages) > MDLOGS_SAFE(dmc->aoml_size)) {
			schedule_timeout(HZ/2);	//sleep 500 ms
			
			goto compaction_required;
		}

		if (merge) {
			merged = tx_cache_merge_checkpoints(dmc);
			EIO_ASSERT(merged >= 0);
			atomic_sub(merged, &dmc->nr_checkpoints);
		}

		atomic64_set(&dmc->last_compaction, jiffies);
		atomic_dec(&dmc->in_compaction);
		merge = 0;
	}

	dmc->compaction_thread_running = 0;
	eio_thread_exit(0);

	return 0;
}

static void tx_cache_comply_periodic_compaction(struct cache_c *dmc)
{
#if COMPACTION_ENABLE
	struct compaction_msg *msg;
	unsigned long flags = 0;
	u_int64_t nr_logs;

	if (!TX_CACHE_IN_COMPACTION(dmc) && TX_CACHE_NEED_PERIODIC_COMPACTION(dmc) &&
		(nr_logs = atomic64_read(&dmc->nr_mdlog_pages)) > NR_MDLOGS_COMPACT) {
		TX_CACHE_INFO("periodic compaction occurred");
		msg = (struct compaction_msg *)kmalloc(
				sizeof(struct compaction_msg), GFP_KERNEL);
		msg->issue_time = jiffies;
		msg->nr_logs = nr_logs;	
		msg->type = NEED_COMPACTION;
		spin_lock_irqsave(&dmc->compaction_lock, flags);
		list_add_tail(&msg->mlist, &dmc->compaction_q);

		EIO_SET_EVENT_AND_UNLOCK(&dmc->compaction_event, 
				&dmc->compaction_lock, flags);
	}
#endif
}

static void tx_cache_comply_compaction_threshold(struct cache_c *dmc)
{
#if COMPACTION_ENABLE
	struct compaction_msg *msg;
	unsigned long flags = 0;
	u_int64_t nr_logs;

	if ((nr_logs = atomic64_read(&dmc->nr_mdlog_pages)) > MDLOGS_WARN(dmc->aoml_size) && 
		!TX_CACHE_IN_COMPACTION(dmc)) {
		TX_CACHE_INFO("compaction occurred");
		msg = (struct compaction_msg *)kmalloc(
				sizeof(struct compaction_msg), GFP_KERNEL);
		msg->issue_time = jiffies;
		msg->nr_logs = nr_logs;	
		msg->type = NEED_COMPACTION;
		spin_lock_irqsave(&dmc->compaction_lock, flags);
		list_add_tail(&msg->mlist, &dmc->compaction_q);

		EIO_SET_EVENT_AND_UNLOCK(&dmc->compaction_event, 
				&dmc->compaction_lock, flags);
	}
	//tx_cache_comply_periodic_compaction(dmc);
#endif
}

static void tx_cache_wrap_up_commit(struct cache_c *dmc, struct tx_unit *tx, struct log_page *page)
{
	struct log_pack *pack = page->log_pack;
	
	if (page->nr_pages > NR_MD_PAGE) {
		eio_free_wb_pages(page->log_page, page->nr_pages);
		kfree(page->log_page);
	} else
		page->log_page = NULL;
	/* pack->logs == tx->logs */
	pack->logs = NULL;

	/* TODO: 	one optimization is available.
				we can maintain committed log_packs 
				until compaction thread reuse it.
	 */
	kfree(tx->logs);
	kfree(pack);
	
}

/* The parameter tx must be given after detaching from dmc->shadow_list */

static struct tx_unit *__commit_tx(struct cache_c *dmc, struct tx_unit *tx)
{
	struct tx_block *block, *block_next;
	int header;
	int mode = 0;
	unsigned long inval_index = 0;
	u_int32_t oldset, curset;
	struct log_page log_page;
	unsigned long begin_time = jiffies;
	unsigned long elapsed;
	index_t old_index;
	/*
#if CONFIG_DELAYED_MD_WRITE
	struct mdupdate_cache_set *dirty_set;
#endif
 	*/
#if CONFIG_SET_SHARING	
	struct set_chain *chain;
	struct list_head temp_list, return_list;
	INIT_LIST_HEAD(&temp_list);
	INIT_LIST_HEAD(&return_list);

	struct return_set {
		int	set_num;
		struct set_chain *chain;
		struct list_head list;
	};

	struct return_set *return_set, *temp_set;
#endif
	mode = dmc->mode;

	if (mode == CACHE_MODE_WB) {
		/* for committing, txcache MUST hold the locks of participated cache_sets */
		//tx_cache_prepare_cache_sets(dmc, tx);
		tx_cache_acquire_locks_participants(dmc, tx);
		tx->tx_status = TX_IN_COMMIT;
		/*
		 * For write-back mode, just changes state of cache blocks
		 * as persistent state.
		 */
		tx->logs = (struct log_entry *)kzalloc((tx->nr_blocks + tx->nr_invalidate) * sizeof(struct log_entry), GFP_NOWAIT); 
		tx_cache_move_shadow_to_persist(dmc, tx);
		
		/*
		 * For compromising implementation effort, 
		 * uses list-based block list.
		 * it will be modified to 
		 * rb-tree based block list for improving search speed.
		 */
		TX_CACHE_DEBUG("let's start commit tx(%ld)", tx->tx_id);	
		inval_index = tx->nr_blocks;
		
		list_for_each_entry_safe(block, block_next, &tx->blist, blist) {
			curset = block->index / dmc->assoc;
			/*
			 * if the block has a persistent copy
			 */
			if (TX_CACHE_TEST_LINK(dmc, block->index)) {
				old_index = TX_CACHE_GET_LINK_INDEX(dmc, block->index);
				oldset = old_index / dmc->assoc;

				if (curset == oldset) {
					if (!TX_CACHE_TEST_LINK(dmc, old_index)) {
						/* it means that, old persistent tx_block 
						 * is already cleaned or in cleaning process.
						 * thus, we don't have to do for it.
						 */
						TX_CACHE_DEBUG("link(%ld - %ld) cleaned or cleaning", block->index, old_index);
						goto link_clear;		
					}

					dmc->cache_sets[oldset].nr_dirty--;	
					dmc->cache_sets[oldset].nr_persist--;
					
					atomic64_dec(&dmc->nr_persist);
					tx->logs[inval_index].index = old_index;
					tx->logs[inval_index].dbn = 0;
					TX_CACHE_SET_INVAL_ENTRY(tx->logs[inval_index].index);
					inval_index++;

					eio_invalidate_md(dmc, old_index);
					TX_CACHE_CLEAR_LINK(dmc, old_index);
					atomic_dec(&dmc->nr_tx_block);
				} 
#if CONFIG_SET_SHARING
				else if (tx_cache_set_in_participant(tx, oldset)){
					/* rented set is participated into this transaction */
					
					if (!TX_CACHE_TEST_LINK(dmc, old_index)) 
						/* this block is in cleaning process */
						goto link_clear;
					
					chain = dmc->cache_sets[oldset].set_chain;

					if (!chain) {
						/* if oldset is master set, it does not have set_chain */
						TX_CACHE_DEBUG("oldset(%d) is master set of curset(%d)", oldset, curset);
						chain = dmc->cache_sets[curset].set_chain;
						
						if (!chain) {
							TX_CACHE_DEBUG("there is no chain, let's clear %d", block->index);
							goto link_clear;
						}

						EIO_ASSERT(chain->master_set == oldset);
						
						dmc->cache_sets[oldset].nr_dirty--;
						dmc->cache_sets[oldset].nr_persist--;
						
						atomic64_dec(&dmc->nr_persist);

						tx->logs[inval_index].index = old_index;
						tx->logs[inval_index].dbn = 0;
						TX_CACHE_SET_INVAL_ENTRY(tx->logs[inval_index].index);
						inval_index++;

						eio_invalidate_md(dmc, old_index);
						TX_CACHE_CLEAR_LINK(dmc, old_index);
						atomic_dec(&dmc->nr_tx_block);
					} else {
						/* oldset is rented set */
						if (!TX_CACHE_TEST_LINK(dmc, old_index))
							goto link_clear;

						TX_CACHE_STATE_OFF(dmc, old_index, PERSIST);
						TX_CACHE_DEBUG("rented entry returned: curset: %d oldset: %d", curset, oldset);

						dmc->cache_sets[oldset].nr_dirty--;	
						dmc->cache_sets[oldset].nr_persist--;
						dmc->cache_sets[oldset].nr_rented--;

						if(dmc->cache_sets[oldset].nr_rented == 0){
							TX_CACHE_DEBUG("set(%ld) is returned", oldset);

							if (dmc->cache_sets[oldset].set_chain != NULL) {
								/* already returned */
								chain = dmc->cache_sets[oldset].set_chain;
								header = chain->master_set;
								dmc->set_headers[header].nr_rent--;

								list_del(&chain->chain);
								dealloc_set_chain(chain);

								dmc->set_headers[header].current_set = header;
								dmc->cache_sets[oldset].set_chain = NULL;
							} else
								TX_CACHE_DEBUG3("old cache block (%d) already returned", old_index);
						}
						atomic64_dec(&dmc->nr_persist);

						tx->logs[inval_index].index = old_index;
						tx->logs[inval_index].dbn = 0;
						TX_CACHE_SET_INVAL_ENTRY(tx->logs[inval_index].index);
						inval_index++;

						eio_invalidate_md(dmc, old_index);
						TX_CACHE_CLEAR_LINK(dmc, old_index);
						atomic_dec(&dmc->nr_tx_block);				
					}
				} else {
					/* rented set does not participate into this transaction */
					struct tx_block *old_block = alloc_tx_block();
					old_block->index = old_index;	
					list_add_tail(&old_block->blist, &temp_list);
				}
#endif
link_clear:
				TX_CACHE_CLEAR_LINK(dmc, block->index);
			} 
			list_del(&block->blist);
			dealloc_tx_block(block);
		}
		
#if CONFIG_SET_SHARING
		list_for_each_entry_safe(block, block_next, &temp_list, blist) {
			oldset = block->index / dmc->assoc;
			old_index = block->index;

			spin_lock(&dmc->cache_sets[oldset].cs_lock);
			
			if (!TX_CACHE_TEST_LINK(dmc, block->index)) {
				spin_unlock(&dmc->cache_sets[oldset].cs_lock);
				goto link_clear2;
			}
			
			TX_CACHE_STATE_OFF(dmc, old_index, PERSIST);

			dmc->cache_sets[oldset].nr_dirty--;	
			dmc->cache_sets[oldset].nr_persist--;
			dmc->cache_sets[oldset].nr_rented--;
			
			if (dmc->cache_sets[oldset].nr_rented == 0) {
				struct set_chain *chain;
	
				TX_CACHE_DEBUG("set(%d) is returned", oldset);
				
				if (dmc->cache_sets[oldset].set_chain != NULL) {
					struct return_set *set = 
						(struct return_set *)kzalloc(sizeof(struct return_set), GFP_NOWAIT);
					TX_CACHE_DEBUG3("set(%d) is returned, chain: %p", oldset, dmc->cache_sets[oldset].set_chain);
					set->chain = dmc->cache_sets[oldset].set_chain;
					set->set_num = dmc->cache_sets[oldset].set_chain->master_set;
					list_add(&set->list, &return_list);
					//header = dmc->cache_sets[oldset].set_chain->master_set;	
					//chain = dmc->cache_sets[oldset].set_chain;
					dmc->cache_sets[oldset].set_chain = NULL;
				} else 
					TX_CACHE_DEBUG3("set(%d) is already returned", oldset);
			}

			tx->logs[inval_index].index = block->index;
			tx->logs[inval_index].dbn = 0;
			TX_CACHE_SET_INVAL_ENTRY(tx->logs[inval_index].index);
			inval_index++;

			atomic64_dec(&dmc->nr_persist);
			
			eio_invalidate_md(dmc, block->index);
			TX_CACHE_CLEAR_LINK(dmc, old_index);
			spin_unlock(&dmc->cache_sets[oldset].cs_lock);
			/*
			if (returned) {
				spin_lock(&dmc->set_headers[header].header_lock);
				
				list_del(&chain->chain);
				dealloc_set_chain(chain);
				
				dmc->set_headers[header].nr_rent--;
				dmc->set_headers[header].current_set = header;
				
				spin_unlock(&dmc->set_headers[header].header_lock);
				returned = 0;
			}
			*/
link_clear2:
			/* if invalidated block is not in pending mdupdate cache set, 
			   new mdreq should be allocated and pended */
			list_del(&block->blist);
			dealloc_tx_block(block);
			atomic_dec(&dmc->nr_tx_block);
			TX_CACHE_DEBUG("%d is returned from %d", block->index, oldset);
		}
#endif	//CONFIG_SET_SHARING
	
		if (tx->nr_blocks + tx->nr_invalidate != inval_index) {
			/* cleaning occurred before committnig tx */
			TX_CACHE_DEBUG("cleaning occurred before committing tx, nr_blocks: %ld nr_invalidate: %ld inval_index: %ld", 
				tx->nr_blocks, tx->nr_invalidate, inval_index);
			tx->nr_invalidate = inval_index - tx->nr_blocks;
		}
#if CONFIG_DELAYED_MD_WRITE
		TX_CACHE_DEBUG("before releasing locks");
		tx_cache_release_locks_participants(dmc, tx);

		list_for_each_entry_safe(return_set, temp_set, &return_list, list) {
			header = return_set->set_num;	
			spin_lock(&dmc->set_headers[header].header_lock);
			
			list_del(&return_set->chain->chain);
			dealloc_set_chain(return_set->chain);
				
			dmc->set_headers[header].nr_rent--;
			dmc->set_headers[header].current_set = header;
				
			spin_unlock(&dmc->set_headers[header].header_lock);
			list_del(&return_set->list);
			kfree(return_set);
		}

		tx_cache_show_md_log(tx);
		
		/* dealloc dirty set inform */
		tx_cache_prepare_commit_log(dmc, tx, &log_page);
		
		/* append md log */
		tx_cache_append_md_log(dmc, &log_page, "commit");	//appending works synchronously
		atomic64_add(log_page.nr_pages, &dmc->nr_mdlog_pages);

		EIO_ASSERT(log_page.error == 0);

		TX_CACHE_DEBUG("writing md_log(%u) is completed", log_page.log_pack->header.log_id);

		tx_cache_wrap_up_commit(dmc, tx, &log_page);
		tx_cache_touch_participants(dmc, tx);
		tx_cache_release_cache_sets(dmc, tx);
		
		up_read(&dmc->reclaimer_lock);

		tx_cache_comply_compaction_threshold(dmc);
#endif
		if (tx->tx_status == TX_IN_ABORT) {
			tx_cache_prepare_abort(dmc, tx);
			__abort_tx(dmc, tx);
		} else {
			tx->tx_status = TX_COMMITTED;
			elapsed = (long)jiffies_to_msecs(jiffies - begin_time);
			atomic64_add(elapsed, &dmc->eio_stats.committime_ms);
			atomic64_inc(&dmc->eio_stats.commitcount);
			TX_CACHE_INFO("committing tx(%d) is completed", tx->tx_id);
		}
	} else if (mode == CACHE_MODE_WT) {
		do_commit_tx(dmc, tx);
	} else
		return NULL;

	return tx;
}

struct tx_unit *commit_tx(struct cache_c* dmc, unsigned long tx_id)
{	
	struct tx_unit *tx;
	int ret;
	unsigned long flags;
	
	spin_lock_irqsave(&dmc->shadow_lock, flags);
	tx = search_tx_by_id(&dmc->shadow_list, tx_id);
	
	if (!tx) {
		spin_unlock_irqrestore(&dmc->shadow_lock, flags);
		TX_CACHE_ERROR("can't find tx by %ld", tx_id);
		return NULL;
	}
	
	if (tx->tx_id)
		list_del(&tx->tlist);

	TX_CACHE_DEBUG("detaching tx(%ld:%p) from shadow list", tx_id, tx);
	spin_unlock_irqrestore(&dmc->shadow_lock, flags);
	
	ret = tx_cache_check_committable(dmc, tx);

	if (ret == 0) {
		/* transaction does not contain any cache blocks */
		tx->tx_status = TX_COMMITTED;
		atomic64_inc(&dmc->eio_stats.commitcount);
		TX_CACHE_INFO("committing empty tx(%d) is completed", tx->tx_id);

		return tx;
	}

	return __commit_tx(dmc, tx);
}

#if CONFIG_DELAYED_MD_WRITE

static struct mdupdate_cache_set *tx_cache_find_mdupdate_set(struct list_head *list, index_t set_index)
{
	struct mdupdate_cache_set *dirty_set;

	list_for_each_entry(dirty_set, list, plist) {
		if (dirty_set->set_num == set_index)
			return dirty_set;
	}

	return NULL;
}

static struct mdupdate_cache_set *tx_cache_alloc_mdupdate_set(void)
{
	return kmem_cache_zalloc(tx_set_cache, GFP_NOWAIT);
}

static void tx_cache_dealloc_mdupdate_set(struct mdupdate_cache_set *set)
{
	kmem_cache_free(tx_set_cache, set);	
}

static int tx_cache_add_mdupdate_set_properly(struct list_head *list, struct mdupdate_cache_set *dirty_set)
{
	struct mdupdate_cache_set *set, *prev = NULL;

	if (list_empty(list)) {
		list_add_tail(&dirty_set->plist, list);
		return 0;
	}

	list_for_each_entry(set, list, plist) {
		if (set->set_num <= dirty_set->set_num) {
			prev = set;
			continue;
		}
		else
			break;
	}

	if (!prev) {
		TX_CACHE_DEBUG("no list found");
		return -1;
	}
	list_add(&dirty_set->plist, &prev->plist);
	return 0;
}

#endif	//CONFIG_DELAYED_MD_WRITE
#endif	//CONFIG_TX_CACHE

#if CONFIG_SET_SHARING
static struct set_chain *alloc_set_chain(void)
{
	return kmem_cache_zalloc(set_chain_cache, GFP_NOWAIT);
}

static void dealloc_set_chain(struct set_chain *chain) 
{
	kmem_cache_free(set_chain_cache, chain);
}

/* 
 * tx_cache_recover_rented_set:
 * this function tries to recover last state of cache.
 * especially, it recovers rental-state by reading 
 * recovered in-core metadata.
 * rental state is composed of sets and their chaining.
 */
static int tx_cache_recover_rented_set(struct cache_c *dmc)
{
	unsigned long set, master_set, index;
	unsigned long set_border;
	unsigned long flag;
	int	nr_recover = 0;
	struct set_chain *chain;

	/* in this loop, all of the metadata are scanned.
	 * then, it decides rental state of the set by
	 * checking dbn of each index. 
	 */
	for (set = 0; set < dmc->num_sets; set++) {
		index = set * dmc->assoc;
		set_border = index + dmc->assoc - 1;
		
		spin_lock_irqsave(&dmc->cache_sets[set].cs_lock, flag);
		while (index <= set_border) {
			if (TX_CACHE_STATE_RENTED(dmc, index)) {
				master_set = hash_block(dmc, EIO_DBN_GET(dmc, index));
				/* a set can be rented by only one master set */	
				EIO_ASSERT(master_set != set);
				
				chain = alloc_set_chain();
				chain->master_set = master_set;
				chain->rental_set = set;
				
				dmc->cache_sets[set].set_chain = chain;
				list_add_tail(&chain->chain, &dmc->set_headers[master_set].chain);
				
				dmc->set_headers[master_set].nr_rent++;
				dmc->cache_sets[set].nr_rented++;
				nr_recover++;
				
				break;
			}
			index++;
		}
		spin_unlock_irqrestore(&dmc->cache_sets[set].cs_lock, flag);
	}
	TX_CACHE_INFO("rented set list recovery complete");

	return nr_recover;
}

/* detach/attach functions MUST be called after acquiring appropriate lock */
static void 
tx_cache_detach_set(struct cache_c *dmc, unsigned set)
{
	struct cache_set *cache_set;

	/* each set only accessed by owner and renting entity both */

	cache_set = &dmc->cache_sets[set];
	cache_set->nr_hold++;

	TX_CACHE_DEBUG("%d hold count: %d caller: %ld", set, cache_set->nr_hold, callee_loc);
	if (cache_set->nr_hold != 1) 
		/* do nothing */
		return ;

	if (cache_set->flags & SETFLAG_IN_SORTED) {
		spin_lock(&dmc->sorted_list_lock);
		rb_erase(&cache_set->node, &dmc->sorted_list);
		spin_unlock(&dmc->sorted_list_lock);
	} else if (cache_set->nr_rented > 0) {
		spin_lock(&dmc->rented_list_lock);
		list_del(&cache_set->empty_list);
		spin_unlock(&dmc->rented_list_lock);
	} else {
		spin_lock(&dmc->empty_list_lock);
		list_del(&cache_set->empty_list);
		spin_unlock(&dmc->empty_list_lock);
	}
}

/* A set is re-attached to sorted list 
 unless the set becomes empty state */
static void 
tx_cache_attach_set(struct cache_c *dmc, unsigned set)
{
	struct cache_set *cache_set, *parent_set;
	struct rb_node	**new, *parent;
	cache_set = &dmc->cache_sets[set];
	
	cache_set->nr_hold--;
	
	/* cache_set->nr_hold must not be negative integer */
	EIO_ASSERT(cache_set->nr_hold >= 0);

	if (cache_set->nr_hold > 0) 
		/* do nothing */
		return ;
	
	TX_CACHE_DEBUG("%d hold count: %d caller: %ld nr_dirty: %u", set, cache_set->nr_hold, callee_loc, dmc->cache_sets[set].nr_dirty);

	if (cache_set->nr_dirty == 0) {
		spin_lock(&dmc->empty_list_lock);
		list_add_tail(&cache_set->empty_list, &dmc->empty_list);
		cache_set->flags &= ~SETFLAG_IN_SORTED;
		spin_unlock(&dmc->empty_list_lock);
	} else if (cache_set->nr_rented > 0) {
		spin_lock(&dmc->rented_list_lock);
		list_add_tail(&cache_set->empty_list, &dmc->rented_list);
		TX_CACHE_DEBUG("rented set (%ld) is pended into rented list", set);
		cache_set->flags &= ~SETFLAG_IN_SORTED;
		spin_unlock(&dmc->rented_list_lock);
	} else {	
		spin_lock(&dmc->sorted_list_lock);
		new = &dmc->sorted_list.rb_node;
		parent = NULL;
		
		while (*new) {
			int ret;
			
			parent_set = (struct cache_set *)rb_entry(*new, struct cache_set, node);
			ret = cache_set->nr_dirty - parent_set->nr_dirty;
			parent = *new;

			if (ret <= 0) 
				new = &parent->rb_left;
			else 
				new = &parent->rb_right;
		}

		rb_link_node(&cache_set->node, parent, new);
		rb_insert_color(&cache_set->node, &dmc->sorted_list);
		cache_set->flags |= SETFLAG_IN_SORTED;
		
		spin_unlock(&dmc->sorted_list_lock);
		TX_CACHE_DEBUG("set(%d) is attached", set);
	}
}

/* nolock function only be called by rent function */

static void 
tx_cache_attach_set_nolock(struct cache_c *dmc, unsigned set)
{
	struct cache_set *cache_set, *parent_set;
	struct rb_node	**new, *parent;
	cache_set = &dmc->cache_sets[set];
	
	cache_set->nr_hold--;
	
	/* cache_set->nr_hold must not be negative integer */
	EIO_ASSERT(cache_set->nr_hold >= 0);

	if (cache_set->nr_hold > 0) 
		/* do nothing */
		return ;
	
	TX_CACHE_DEBUG("%d hold count: %d caller: %ld nr_dirty: %u", set, cache_set->nr_hold, callee_loc, dmc->cache_sets[set].nr_dirty);

	if (cache_set->nr_dirty == 0) {
		list_add_tail(&cache_set->empty_list, &dmc->empty_list);
		cache_set->flags &= ~SETFLAG_IN_SORTED;
	} else if (cache_set->nr_rented > 0) {
		list_add_tail(&cache_set->empty_list, &dmc->rented_list);
		cache_set->flags &= ~SETFLAG_IN_SORTED;
	} else {	
		new = &dmc->sorted_list.rb_node;
		parent = NULL;
		
		while (*new) {
			int ret;
			
			parent_set = (struct cache_set *)rb_entry(*new, struct cache_set, node);
			ret = cache_set->nr_dirty - parent_set->nr_dirty;
			parent = *new;

			if (ret <= 0) 
				new = &parent->rb_left;
			else 
				new = &parent->rb_right;
		}

		rb_link_node(&cache_set->node, parent, new);
		rb_insert_color(&cache_set->node, &dmc->sorted_list);
		cache_set->flags |= SETFLAG_IN_SORTED;
		
		TX_CACHE_DEBUG("set(%d) is attached", set);
	}
}

#define calculate_set_num(root, set, ptype) (((unsigned long)set - (unsigned long)root) / sizeof(ptype))
#define tx_cache_empty_set_empty(dmc) (list_empty(&dmc->empty_list))

/* tx_cache_get_empty|sorted_set detach set from list and return the number of the set */

static unsigned
tx_cache_get_empty_set(struct cache_c *dmc)
{
	unsigned set_num;
	struct cache_set *empty_set;
	unsigned long flag;

	local_irq_save(flag);
	spin_lock(&dmc->empty_list_lock);
	
	empty_set = list_first_entry(&dmc->empty_list, struct cache_set, empty_list);
	set_num = calculate_set_num(&dmc->cache_sets[0], empty_set, struct cache_set);
	spin_unlock(&dmc->empty_list_lock);
	
	TX_CACHE_DEBUG("pick the empty set(%u)", set_num);
	
	spin_lock(&dmc->cache_sets[set_num].cs_lock);
	tx_cache_detach_set(dmc, set_num);
	spin_unlock(&dmc->cache_sets[set_num].cs_lock);
	local_irq_restore(flag);
	
	atomic64_inc(&dmc->eio_stats.rentsetcount);

	return set_num;
}

static unsigned
tx_cache_get_sorted_set(struct cache_c *dmc)
{
	unsigned set_num, retry_set_num;
	struct cache_set *sorted_set = NULL, *next = NULL;
	unsigned long flag;
	struct list_head retry_list;
	INIT_LIST_HEAD(&retry_list);

	local_irq_save(flag);
	spin_lock(&dmc->sorted_list_lock);

	if (RB_EMPTY_ROOT(&dmc->sorted_list)) { 
		TX_CACHE_ERROR("sorted list exhausted");
		BUG();
	}

	sorted_set = rb_entry(rb_first(&dmc->sorted_list), struct cache_set, node);

find_set_retry:
	set_num = calculate_set_num(&dmc->cache_sets[0], sorted_set, struct cache_set);

	TX_CACHE_DEBUG("set_num: %u sorted_set: %p nr_dirty: %d", set_num, sorted_set, sorted_set->nr_dirty);
	spin_unlock(&dmc->sorted_list_lock);
	
	spin_lock(&dmc->cache_sets[set_num].cs_lock);
	tx_cache_detach_set(dmc, set_num);

	/* 
	 * if the rented set is already rented by other set, 
	 * we should re-find the set 
	 */
	if (dmc->cache_sets[set_num].nr_rented > 0) {
		TX_CACHE_DEBUG("pick the rented set(%u) let's retry", set_num);
		list_add_tail(&sorted_set->empty_list, &retry_list);
		spin_unlock(&dmc->cache_sets[set_num].cs_lock);
		spin_lock(&dmc->sorted_list_lock);	
		sorted_set = rb_entry(rb_first(&dmc->sorted_list), struct cache_set, node);

		goto find_set_retry;
	}
	
	spin_unlock(&dmc->cache_sets[set_num].cs_lock);
	//spin_unlock(&dmc->sorted_list_lock);
	local_irq_restore(flag);
	
	TX_CACHE_DEBUG("pick the sorted set(%u)", set_num);

	if (!list_empty(&retry_list)) {
		list_for_each_entry_safe(sorted_set, next, &retry_list, empty_list) {
			list_del(&sorted_set->empty_list);
			retry_set_num = calculate_set_num(&dmc->cache_sets[0], sorted_set, struct cache_set);
			
			spin_lock(&dmc->cache_sets[retry_set_num].cs_lock);
			PREPARE_LAST_LOC(__LINE__);
			tx_cache_attach_set(dmc, retry_set_num);
			spin_unlock(&dmc->cache_sets[retry_set_num].cs_lock);
		}
	}
	atomic64_inc(&dmc->eio_stats.rentsetcount);

	return set_num;
}

#define tx_cache_rent_set(dmc) (tx_cache_empty_set_empty(dmc) ? tx_cache_get_sorted_set(dmc) : tx_cache_get_empty_set(dmc))
#else	//CONFIG_SET_SHARING
static int tx_cache_recover_rented_set(struct cache_c *dmc)
{
	return 0;
}
#endif	//CONFIG_SET_SHARING

static void bc_addfb(struct bio_container *bc, struct eio_bio *ebio)
{
	atomic_inc(&bc->bc_holdcount);

	ebio->eb_bc = bc;
}

static void bc_put(struct bio_container *bc, unsigned int doneio)
{
	struct cache_c *dmc;
	int data_dir;
	long elapsed;
#ifdef CONFIG_TX_CACHE
	struct tx_unit *tx;
#endif

	if (atomic_dec_and_test(&bc->bc_holdcount)) {
		if (bc->bc_dmc->mode == CACHE_MODE_WB)
			eio_release_io_resources(bc->bc_dmc, bc);
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(3,14,0))
		bc->bc_bio->bi_iter.bi_size = 0;
#else 
		bc->bc_bio->bi_size = 0;
#endif 
		dmc = bc->bc_dmc;

#ifdef CONFIG_TX_CACHE
		TX_CACHE_DEBUG("%p - %d", bc, bc->tx->tx_id);
		if (bc->tx) {
			if (bc->tx->tx_status == TX_ABORTED) {
				PREPARE_LAST_LOC(__LINE__);
				dealloc_tx(dmc, bc->tx);	
				bc->tx = NULL;
			}
			else if (bc->single_req_tx) {
				if (bc->tx->nr_blocks != 0) {
					//temporary solution: 
					//if (tx_cache_check_committable(dmc, bc->tx))
						//;
						//tx = __commit_tx(dmc, bc->tx);	
					//else
						tx = bc->tx;
				} else {
					TX_CACHE_INFO("empty transaction, skip commit");
					tx = bc->tx;
				}
				
				if (tx) {
					PREPARE_LAST_LOC(__LINE__);
					dealloc_tx(dmc, tx);
				}
				bc->tx = NULL;
			}
		}
#endif	//CONFIG_TX_CACHE
		/* update iotime for latency */
		data_dir = bio_data_dir(bc->bc_bio);
		elapsed = (long)jiffies_to_msecs(jiffies - bc->bc_iotime);

		if (data_dir == READ)
			atomic64_add(elapsed, &dmc->eio_stats.rdtime_ms);
		else
			atomic64_add(elapsed, &dmc->eio_stats.wrtime_ms);
			
		bio_endio(bc->bc_bio, bc->bc_error);
		atomic64_dec(&bc->bc_dmc->nr_ios);
		kfree(bc);
	}
}

static void eb_endio(struct eio_bio *ebio, int error)
{
	EIO_ASSERT(ebio->eb_bc);
	
#ifdef CONFIG_TX_CACHE
	TX_CACHE_DEBUG("ebio->index: %d ebio->eb_iotype: %d bc: %p bc->tx: %p bc->tx_id: %d bc->bc_mdwait: %d",
		ebio->eb_index, ebio->eb_iotype, ebio->eb_bc, ebio->eb_bc->tx, ebio->eb_bc->tx->tx_id, atomic_read(&ebio->eb_bc->bc_mdwait));
#endif
	/*Propagate only main io errors and sizes*/
	if (ebio->eb_iotype == EB_MAIN_IO) {
		if (error)
			ebio->eb_bc->bc_error = error;
		bc_put(ebio->eb_bc, ebio->eb_size);
	} else
		bc_put(ebio->eb_bc, 0);
	ebio->eb_bc = NULL;
	kfree(ebio);
}

#ifdef CONFIG_TX_CACHE
static inline void tx_cache_eb_endio(struct eio_bio *ebio, int error, struct tx_unit *tx) 
{
	eb_endio(ebio, error);
	
	if (tx) 
		atomic_dec(&tx->nr_remain);
}
#endif

static int
eio_io_async_bvec(struct cache_c *dmc, struct eio_io_region *where, int rw,
		  struct bio_vec *pages, unsigned nr_bvecs, eio_notify_fn fn,
		  void *context, int hddio)
{
	struct eio_io_request req;
	int error = 0;

	memset((char *)&req, 0, sizeof(req));

	if (unlikely(CACHE_DEGRADED_IS_SET(dmc))) {
		if (where->bdev != dmc->disk_dev->bdev) {
			pr_err
				("eio_io_async_bvec: Cache is in degraded mode.\n");
			pr_err
				("eio_io_async_Bvec: Can not issue i/o to ssd device.\n");
			return -ENODEV;
		}
	}

	req.mtype = EIO_BVECS;
	req.dptr.pages = pages;
	req.num_bvecs = nr_bvecs;
	req.notify = fn;
	req.context = context;
	req.hddio = hddio;

	error = eio_do_io(dmc, where, rw, &req);

	return error;
}

static void
eio_flag_abios(struct cache_c *dmc, struct eio_bio *abio, int invalidated)
{
	struct eio_bio *nbio;

	while (abio) {
		int invalidate;
		unsigned long flags;
		int cwip_on = 0;
		int dirty_on = 0;
		int callendio = 0;
		nbio = abio->eb_next;

		EIO_ASSERT(!(abio->eb_iotype & EB_INVAL) || abio->eb_index == -1);
		invalidate = !invalidated && (abio->eb_iotype & EB_INVAL);

		spin_lock_irqsave(&dmc->cache_sets[abio->eb_cacheset].cs_lock,
				  flags);

		if (abio->eb_index != -1) {
			if (EIO_CACHE_STATE_GET(dmc, abio->eb_index) & DIRTY)
				dirty_on = 1;

			if (unlikely
				    (EIO_CACHE_STATE_GET(dmc, abio->eb_index) &
				    CACHEWRITEINPROG))
				cwip_on = 1;
		}

		if (dirty_on) {
			/*
			 * For dirty blocks, we don't change the cache state flags.
			 * We however, need to end the ebio, if this was the last
			 * hold on it.
			 */
			if (atomic_dec_and_test(&abio->eb_holdcount)) {
				callendio = 1;
				/* We shouldn't reach here when the DIRTY_INPROG flag
				 * is set on the cache block. It should either have been
				 * cleared to become DIRTY or INVALID elsewhere.
				 */
				EIO_ASSERT(EIO_CACHE_STATE_GET(dmc, abio->eb_index)
					   != DIRTY_INPROG);
			}
		} else if (abio->eb_index != -1) {
			if (invalidate) {
				if (cwip_on)
					EIO_CACHE_STATE_ON(dmc, abio->eb_index,
							   QUEUED);
				else {
	TX_CACHE_ERROR("INVALIDATE block(%ld)",abio->eb_index);
					EIO_CACHE_STATE_SET(dmc, abio->eb_index,
							    INVALID);
					atomic64_dec_if_positive(&dmc->
								 eio_stats.
								 cached_blocks);
				}
			} else {
				if (cwip_on)
					EIO_CACHE_STATE_OFF(dmc, abio->eb_index,
							    DISKWRITEINPROG);
				else {
					if (EIO_CACHE_STATE_GET
						    (dmc, abio->eb_index) & QUEUED) {
	TX_CACHE_ERROR("INVALIDATE block(%ld)", abio->eb_index);
						EIO_CACHE_STATE_SET(dmc,
								    abio->
								    eb_index,
								    INVALID);
						atomic64_dec_if_positive(&dmc->
									 eio_stats.
									 cached_blocks);
					} else {
						EIO_CACHE_STATE_SET(dmc,
								    abio->
								    eb_index,
								    VALID);
					}
				}
			}
		} else {
			EIO_ASSERT(invalidated || invalidate);
			if (invalidate)
				eio_inval_block(dmc, abio->eb_sector);
		}
		spin_unlock_irqrestore(&dmc->cache_sets[abio->eb_cacheset].
				       cs_lock, flags);
		if (!cwip_on && (!dirty_on || callendio))
			eb_endio(abio, 0);
		abio = nbio;
	}
}

static void eio_disk_io_callback(int error, void *context)
{
	struct kcached_job *job;
	struct eio_bio *ebio;
	struct cache_c *dmc;
	unsigned long flags;
	unsigned eb_cacheset;

	flags = 0;
	job = (struct kcached_job *)context;
	dmc = job->dmc;
	ebio = job->ebio;

	EIO_ASSERT(ebio != NULL);
	eb_cacheset = ebio->eb_cacheset;

	if (unlikely(error))
		dmc->eio_errors.disk_read_errors++;

	spin_lock_irqsave(&dmc->cache_sets[eb_cacheset].cs_lock, flags);
	/* Invalidate the cache block */
	EIO_CACHE_STATE_SET(dmc, ebio->eb_index, INVALID);
	atomic64_dec_if_positive(&dmc->eio_stats.cached_blocks);
	spin_unlock_irqrestore(&dmc->cache_sets[eb_cacheset].cs_lock, flags);

	if (unlikely(error))
		pr_err("disk_io_callback: io error %d block %llu action %d",
		       error,
		       (unsigned long long)job->job_io_regions.disk.sector,
		       job->action);

	eb_endio(ebio, error);
	ebio = NULL;
	job->ebio = NULL;
	eio_free_cache_job(job);
	job = NULL;
}

static void eio_uncached_read_done(struct kcached_job *job)
{
	struct eio_bio *ebio = job->ebio;
	struct cache_c *dmc = job->dmc;
	struct eio_bio *iebio;
	struct eio_bio *nebio;
	unsigned long flags = 0;

	if (ebio->eb_bc->bc_dir == UNCACHED_READ) {
		EIO_ASSERT(ebio != NULL);
		iebio = ebio->eb_next;
		while (iebio != NULL) {
			nebio = iebio->eb_next;
			if (iebio->eb_index != -1) {
				spin_lock_irqsave(&dmc->
						  cache_sets[iebio->
							     eb_cacheset].
						  cs_lock, flags);
				if (unlikely
					    (EIO_CACHE_STATE_GET(dmc, iebio->eb_index) &
					    QUEUED)) {
					EIO_CACHE_STATE_SET(dmc,
							    iebio->eb_index,
							    INVALID);
					atomic64_dec_if_positive(&dmc->
								 eio_stats.
								 cached_blocks);
				} else
				if (EIO_CACHE_STATE_GET
					    (dmc,
					    iebio->eb_index) & CACHEREADINPROG) {
					/*turn off the cache read in prog flag*/
					EIO_CACHE_STATE_OFF(dmc,
							    iebio->eb_index,
							    BLOCK_IO_INPROG);
				} else
					/*Should never reach here*/
					EIO_ASSERT(0);
				spin_unlock_irqrestore(&dmc->
						       cache_sets[iebio->
								  eb_cacheset].
						       cs_lock, flags);
			}
			eb_endio(iebio, 0);
			iebio = nebio;
		}
		eb_endio(ebio, 0);
		eio_free_cache_job(job);
	} else if (ebio->eb_bc->bc_dir == UNCACHED_READ_AND_READFILL) {
		/*
		 * Kick off the READFILL. It will also do a read
		 * from SSD, in case of ALREADY_DIRTY block
		 */
		job->action = READFILL;
		eio_enqueue_readfill(dmc, job);
	} else
		/* Should never reach here for uncached read */
		EIO_ASSERT(0);
}

static void eio_io_callback(int error, void *context)
{
	struct kcached_job *job = (struct kcached_job *)context;
	struct cache_c *dmc = job->dmc;

	job->error = error;
	INIT_WORK(&job->work, eio_post_io_callback);
	queue_work(dmc->callback_q, &job->work);
	return;
}

static void eio_post_io_callback(struct work_struct *work)
{
	struct kcached_job *job;
	struct cache_c *dmc;
	struct eio_bio *ebio;
	unsigned long flags = 0;
	index_t index;
	unsigned eb_cacheset;
	u_int8_t cstate;
	int callendio = 0;
	int error;
	
	job = container_of(work, struct kcached_job, work);
	dmc = job->dmc;
	index = job->index;
	error = job->error;

	EIO_ASSERT(index != -1 || job->action == WRITEDISK
		   || job->action == READDISK);
	ebio = job->ebio;
	EIO_ASSERT(ebio != NULL);
	EIO_ASSERT(ebio->eb_bc);

	eb_cacheset = ebio->eb_cacheset;
	if (error)
		pr_err("io_callback: io error %d block %llu action %d",
		       error,
		       (unsigned long long)job->job_io_regions.disk.sector,
		       job->action);

	switch (job->action) {
	case WRITEDISK:

		atomic64_inc(&dmc->eio_stats.writedisk);
		if (unlikely(error))
			dmc->eio_errors.disk_write_errors++;
		if (unlikely(error) || (ebio->eb_iotype & EB_INVAL))
			eio_inval_range(dmc, ebio->eb_sector, ebio->eb_size);
		if (ebio->eb_next)
			eio_flag_abios(dmc, ebio->eb_next,
				       error || (ebio->eb_iotype & EB_INVAL));
		eb_endio(ebio, error);
		job->ebio = NULL;
		eio_free_cache_job(job);
		return;

	case READDISK:

		if (unlikely(error) || unlikely(ebio->eb_iotype & EB_INVAL)
		    || CACHE_DEGRADED_IS_SET(dmc)) {
			if (error)
				dmc->eio_errors.disk_read_errors++;
			eio_inval_range(dmc, ebio->eb_sector, ebio->eb_size);
			eio_flag_abios(dmc, ebio->eb_next, 1);
		} else if (ebio->eb_next) {
			eio_uncached_read_done(job);
			return;
		}
		eb_endio(ebio, error);
		job->ebio = NULL;
		eio_free_cache_job(job);
		return;

	case READCACHE:

		/*atomic64_inc(&dmc->eio_stats.readcache);*/
		/*SECTOR_STATS(dmc->eio_stats.ssd_reads, ebio->eb_size);*/
		EIO_ASSERT(EIO_DBN_GET(dmc, index) ==
			   EIO_ROUND_SECTOR(dmc, ebio->eb_sector));
		cstate = EIO_CACHE_STATE_GET(dmc, index);
		/* We shouldn't reach here for DIRTY_INPROG blocks. */
		EIO_ASSERT(cstate != DIRTY_INPROG);
		if (unlikely(error)) {
			dmc->eio_errors.ssd_read_errors++;
			/* Retry read from HDD for non-DIRTY blocks. */
			if (cstate != ALREADY_DIRTY) {
				spin_lock_irqsave(&dmc->cache_sets[eb_cacheset].
						  cs_lock, flags);
				EIO_CACHE_STATE_OFF(dmc, ebio->eb_index,
						    CACHEREADINPROG);
				EIO_CACHE_STATE_ON(dmc, ebio->eb_index,
						   DISKREADINPROG);
				spin_unlock_irqrestore(&dmc->
						       cache_sets[eb_cacheset].
						       cs_lock, flags);

				eio_push_ssdread_failures(job);
				schedule_work(&_kcached_wq);

				return;
			}
		}
		callendio = 1;
		break;

	case READFILL:

		/*atomic64_inc(&dmc->eio_stats.readfill);*/
		/*SECTOR_STATS(dmc->eio_stats.ssd_writes, ebio->eb_size);*/
		EIO_ASSERT(EIO_DBN_GET(dmc, index) == ebio->eb_sector);
		if (unlikely(error))
			dmc->eio_errors.ssd_write_errors++;
		if (!(EIO_CACHE_STATE_GET(dmc, index) & CACHEWRITEINPROG)) {
			pr_debug("DISKWRITEINPROG absent in READFILL \
				sector %llu io size %u\n",
				(unsigned long long)ebio->eb_sector,
			       ebio->eb_size);
		}
		callendio = 1;
		break;

	case WRITECACHE:

		/*SECTOR_STATS(dmc->eio_stats.ssd_writes, ebio->eb_size);*/
		/*atomic64_inc(&dmc->eio_stats.writecache);*/
		cstate = EIO_CACHE_STATE_GET(dmc, index);

		if (EIO_DBN_GET(dmc, index) != EIO_ROUND_SECTOR(dmc, ebio->eb_sector))
			TX_CACHE_ERROR("index: %d dbn: %ld, ebio->eb_index: %d dbn: %ld", 
				index, EIO_DBN_GET(dmc, index),
				ebio->eb_index, EIO_ROUND_SECTOR(dmc, ebio->eb_sector));
		EIO_ASSERT(EIO_DBN_GET(dmc, index) ==
			   EIO_ROUND_SECTOR(dmc, ebio->eb_sector));
		/* CWIP is a must for WRITECACHE, except when it is DIRTY */
		EIO_ASSERT(cstate & (CACHEWRITEINPROG | DIRTY));
		if (likely(error == 0)) {
			/* If it is a DIRTY inprog block, proceed for metadata update */
			if (cstate == DIRTY_INPROG) {
#ifdef CONFIG_TX_CACHE
				//bc = job->ebio->eb_bc;
				//TX_CACHE_DEBUG("job: %p bc: %p %d %d bc->bc_mdwait: %d bc->tx: %p index: %d", job, bc, sizeof(*bc), sizeof(struct bio_container), atomic_read(&bc->bc_mdwait), bc->tx, job->ebio->eb_index);
#endif
				eio_md_write(job);
				return;
			}
		} else {
			/* TODO: ask if this if condition is required */
			if (dmc->mode == CACHE_MODE_WT)
				dmc->eio_errors.disk_write_errors++;
			dmc->eio_errors.ssd_write_errors++;
		}
		job->ebio = NULL;
		break;

	default:
		pr_err("io_callback: invalid action %d", job->action);
		return;
	}

	spin_lock_irqsave(&dmc->cache_sets[eb_cacheset].cs_lock, flags);

	cstate = EIO_CACHE_STATE_GET(dmc, index);
	EIO_ASSERT(!(cstate & INVALID));

	if (unlikely
		    ((job->action == WRITECACHE) && !(cstate & DISKWRITEINPROG))) {
		/*
		 * Can reach here in 2 cases:
		 * 1. Uncached write case, where WRITEDISK has finished first
		 * 2. Cached write case
		 *
		 * For DIRTY or DIRTY inprog cases, use eb holdcount to determine
		 * if end ebio can be called. This is because, we don't set DWIP etc
		 * flags on those and we have to avoid double end ebio call
		 */
		EIO_ASSERT((cstate != DIRTY_INPROG) || error);
		callendio = 1;
		if ((cstate & DIRTY)
		    && !atomic_dec_and_test(&ebio->eb_holdcount))
			callendio = 0;
	}

	if (cstate & DISKWRITEINPROG) {
		/* uncached write and WRITEDISK is not yet finished */
		EIO_ASSERT(!(cstate & DIRTY));      /* For dirty blocks, we can't have DWIP flag */
		if (error)
			EIO_CACHE_STATE_ON(dmc, index, QUEUED);
		EIO_CACHE_STATE_OFF(dmc, index, CACHEWRITEINPROG);
	} else if (unlikely(error || (cstate & QUEUED))) {
		/* Error or QUEUED is set: mark block as INVALID for non-DIRTY blocks */
		if (cstate != ALREADY_DIRTY) {
			TX_CACHE_DEBUG("INVALIDATE block(%ld)", index);
			EIO_CACHE_STATE_SET(dmc, index, INVALID);
			atomic64_dec_if_positive(&dmc->eio_stats.cached_blocks);
		}
	} else if (cstate & VALID) {
		EIO_CACHE_STATE_OFF(dmc, index, BLOCK_IO_INPROG);
		/*
		 * If we have NO_SSD_IO_INPROG flag set, then this block needs to be
		 * invalidated. There are three things that can happen -- (i) error,
		 * (ii) IOs are queued on this block, and (iii) success.
		 *
		 * If there was an error or if the QUEUED bit was set, then the logic
		 * in the if part will take care of setting the block to INVALID.
		 * Therefore, this is the success path where we invalidate if need be.
		 */

		/*
		 * TBD
		 * NO_SSD_IO_INPROG need to be differently handled, in case block is DIRTY
		 */
		if ((cstate & NO_SSD_IO_INPROG) == NO_SSD_IO_INPROG)
			EIO_CACHE_STATE_OFF(dmc, index, VALID);
	}

	if (callendio) {
#if CONFIG_SET_SHARING
		PREPARE_LAST_LOC(__LINE__);
		tx_cache_attach_set(dmc, ebio->eb_cacheset);
#endif
		spin_unlock_irqrestore(&dmc->cache_sets[eb_cacheset].cs_lock, flags);
#ifdef CONFIG_TX_CACHE
		if (job->action == WRITECACHE)
			tx_cache_eb_endio(ebio, error, ebio->eb_bc->tx);
		else
			eb_endio(ebio, error);
#else
		eb_endio(ebio, error);
#endif
	} else 
		spin_unlock_irqrestore(&dmc->cache_sets[eb_cacheset].cs_lock, flags);

	eio_free_cache_job(job);
	job = NULL;

}

/*
 * This function processes the kcached_job that
 * needs to be scheduled on disk after ssd read failures.
 */
void eio_ssderror_diskread(struct kcached_job *job)
{
	struct cache_c *dmc;
	struct eio_bio *ebio;
	index_t index;
	int error;
	unsigned long flags = 0;

	dmc = job->dmc;
	error = 0;

	/*
	 * 1. Extract the ebio which needs to be scheduled on disk.
	 * 2. Verify cache block state is VALID
	 * 3. Make sure that the cache state in not IOINPROG
	 */
	/* Reset the ssd read error in the job. */
	job->error = 0;
	ebio = job->ebio;
	index = ebio->eb_index;

	EIO_ASSERT(index != -1);

	spin_lock_irqsave(&dmc->cache_sets[index / dmc->assoc].cs_lock, flags);
	EIO_ASSERT(EIO_CACHE_STATE_GET(dmc, index) & DISKREADINPROG);
	spin_unlock_irqrestore(&dmc->cache_sets[index / dmc->assoc].cs_lock,
			       flags);

	EIO_ASSERT(ebio->eb_dir == READ);

	atomic64_inc(&dmc->eio_stats.readdisk);
	SECTOR_STATS(dmc->eio_stats.disk_reads, ebio->eb_size);
	job->action = READDISK;

	error = eio_io_async_bvec(dmc, &job->job_io_regions.disk, ebio->eb_dir,
				  ebio->eb_bv, ebio->eb_nbvec,
				  eio_disk_io_callback, job, 1);

	/*
	 * In case of disk i/o submission error clear ebio and kcached_job.
	 * This would return the actual read that was issued on ssd.
	 */
	if (error)
		goto out;

	return;

out:
	/* We failed to submit the I/O to dm layer. The corresponding
	 * block should be marked as INVALID by turning off already set
	 * flags.
	 */
	spin_lock_irqsave(&dmc->cache_sets[index / dmc->assoc].cs_lock, flags);
	EIO_CACHE_STATE_SET(dmc, ebio->eb_index, INVALID);
	spin_unlock_irqrestore(&dmc->cache_sets[index / dmc->assoc].cs_lock,
			       flags);

	atomic64_dec_if_positive(&dmc->eio_stats.cached_blocks);

	eb_endio(ebio, error);
	ebio = NULL;
	job->ebio = NULL;
	eio_free_cache_job(job);
}

/* Adds clean set request to clean queue. */
static void eio_addto_cleanq(struct cache_c *dmc, index_t set, int whole)
{
	unsigned long flags = 0;

	spin_lock_irqsave(&dmc->cache_sets[set].cs_lock, flags);

	if (dmc->cache_sets[set].flags & SETFLAG_CLEAN_INPROG) {
		/* Clean already in progress, just add to clean pendings */
		spin_unlock_irqrestore(&dmc->cache_sets[set].cs_lock, flags);
		return;
	}

	dmc->cache_sets[set].flags |= SETFLAG_CLEAN_INPROG;
	if (whole)
		dmc->cache_sets[set].flags |= SETFLAG_CLEAN_WHOLE;

	spin_unlock_irqrestore(&dmc->cache_sets[set].cs_lock, flags);

	spin_lock_irqsave(&dmc->clean_sl, flags);
	list_add_tail(&dmc->cache_sets[set].list, &dmc->cleanq);
	atomic64_inc(&dmc->clean_pendings);
	EIO_SET_EVENT_AND_UNLOCK(&dmc->clean_event, &dmc->clean_sl, flags);
	return;
}

/*
 * Clean thread loops forever in this, waiting for
 * new clean set requests in the clean queue.
 */
int eio_clean_thread_proc(void *context)
{
	struct cache_c *dmc = (struct cache_c *)context;
	unsigned long flags = 0;
	u_int64_t systime;
	index_t index;

	/* Sync makes sense only for writeback cache */
	EIO_ASSERT(dmc->mode == CACHE_MODE_WB);

	dmc->clean_thread_running = 1;

	/*
	 * Using sysctl_fast_remove to stop the clean thread
	 * works for now. Should have another flag specifically
	 * for such notification.
	 */
	for (; !dmc->sysctl_active.fast_remove; ) {
		LIST_HEAD(setlist);
		struct cache_set *set;
#ifdef CONFIG_TX_CACHE	
		spin_lock_irqsave(&dmc->pers_list_lock, flags);
		eio_comply_dirty_thresholds(dmc, -1);
		spin_unlock_irqrestore(&dmc->pers_list_lock, flags);
#else
		eio_comply_dirty_thresholds(dmc, -1);
#endif

		if (dmc->sysctl_active.do_clean) {
			/* pause the periodic clean */
			cancel_delayed_work_sync(&dmc->clean_aged_sets_work);

			/* clean all the sets */
			eio_clean_all(dmc);

			/* resume the periodic clean */
#ifdef CONFIG_TX_CACHE
			spin_lock_irqsave(&dmc->pers_list_lock, flags);
			dmc->is_clean_aged_sets_sched = 0;
			if (dmc->sysctl_active.time_based_clean_interval
				&& atomic64_read(&dmc->nr_persist)) {
				schedule_delayed_work(&dmc->clean_aged_sets_work,
					dmc->sysctl_active.time_based_clean_interval * 60 * HZ);
				dmc->is_clean_aged_sets_sched = 1;
			}
			spin_unlock_irqrestore(&dmc->pers_list_lock, flags);
#else
			spin_lock_irqsave(&dmc->dirty_set_lru_lock, flags);
			dmc->is_clean_aged_sets_sched = 0;
			if (dmc->sysctl_active.time_based_clean_interval
			    && atomic64_read(&dmc->nr_dirty)) {
				/* there is a potential race here, If a sysctl changes
				   the time_based_clean_interval to 0. However a strong
				   synchronisation is not necessary here
				 */
				schedule_delayed_work(&dmc->
						      clean_aged_sets_work,
						      dmc->sysctl_active.
						      time_based_clean_interval
						      * 60 * HZ);
				dmc->is_clean_aged_sets_sched = 1;
			}
			spin_unlock_irqrestore(&dmc->dirty_set_lru_lock, flags);
#endif		//CONFIG_TX_CACHE
		}
		if (dmc->sysctl_active.fast_remove)
			break;

		spin_lock_irqsave(&dmc->clean_sl, flags);

		while (!
		       ((!list_empty(&dmc->cleanq))
			|| dmc->sysctl_active.fast_remove
			|| dmc->sysctl_active.do_clean))
			EIO_WAIT_EVENT(&dmc->clean_event, &dmc->clean_sl,
				       flags);

		/*
		 * Move cleanq elements to a private list for processing.
		 */

		list_add(&setlist, &dmc->cleanq);
		list_del(&dmc->cleanq);
		INIT_LIST_HEAD(&dmc->cleanq);

		spin_unlock_irqrestore(&dmc->clean_sl, flags);

		systime = jiffies;
		while (!list_empty(&setlist)) {
			set =
				list_entry((&setlist)->next, struct cache_set,
					   list);
			list_del(&set->list);
			index = set - dmc->cache_sets;
			
			if (!(dmc->sysctl_active.fast_remove)) {
				eio_clean_set(dmc, index,
					      set->flags & SETFLAG_CLEAN_WHOLE,
					      0);
			} else {

				/*
				 * Since we are not cleaning the set, we should
				 * put the set back in the lru list so that
				 * it is picked up at a later point.
				 * We also need to clear the clean inprog flag
				 * otherwise this set would never be cleaned.
				 */

				spin_lock_irqsave(&dmc->cache_sets[index].
						  cs_lock, flags);
				dmc->cache_sets[index].flags &=
					~(SETFLAG_CLEAN_INPROG |
					  SETFLAG_CLEAN_WHOLE);
#ifdef CONFIG_TX_CACHE
				spin_lock(&dmc->pers_list_lock);
				if (dmc->pers_sets[index].q_flag & IN_QUEUE)
					tx_cache_pers_touch(dmc, index, dmc->cache_sets[index].nr_persist);
				spin_unlock(&dmc->pers_list_lock);
				spin_unlock_irqrestore(&dmc->cache_sets[index].
						       cs_lock, flags);
#else
				spin_unlock_irqrestore(&dmc->cache_sets[index].
						       cs_lock, flags);
				spin_lock_irqsave(&dmc->dirty_set_lru_lock,
						  flags);
				lru_touch(dmc->dirty_set_lru, index, systime);
				spin_unlock_irqrestore(&dmc->dirty_set_lru_lock,
						       flags);
#endif	//CONFIG_TX_CACHE
			}
			atomic64_dec(&dmc->clean_pendings);
		}
	}

	/* notifier for cache delete that the clean thread has stopped running */
	dmc->clean_thread_running = 0;

	eio_thread_exit(0);

	/*Should never reach here*/
	return 0;
}

/*
 * Cache miss support. We read the data from disk, write it to the ssd.
 * To avoid doing 1 IO at a time to the ssd, when the IO is kicked off,
 * we enqueue it to a "readfill" queue in the cache in cache sector order.
 * The worker thread can then issue all of these IOs and do 1 unplug to
 * start them all.
 *
 */
static void eio_enqueue_readfill(struct cache_c *dmc, struct kcached_job *job)
{
	unsigned long flags = 0;
	struct kcached_job **j1, *next;
	int do_schedule = 0;

	spin_lock_irqsave(&dmc->cache_spin_lock, flags);
	/* Insert job in sorted order of cache sector */
	j1 = &dmc->readfill_queue;
	while (*j1 != NULL && (*j1)->job_io_regions.cache.sector <
	       job->job_io_regions.cache.sector)
		j1 = &(*j1)->next;
	next = *j1;
	*j1 = job;
	job->next = next;
	do_schedule = (dmc->readfill_in_prog == 0);
	spin_unlock_irqrestore(&dmc->cache_spin_lock, flags);
	if (do_schedule)
		schedule_work(&dmc->readfill_wq);
}

void eio_do_readfill(struct work_struct *work)
{
	struct kcached_job *job, *joblist;
	struct eio_bio *ebio;
	unsigned long flags = 0;
	struct kcached_job *nextjob = NULL;
	struct cache_c *dmc = container_of(work, struct cache_c, readfill_wq);

	spin_lock_irqsave(&dmc->cache_spin_lock, flags);
	if (dmc->readfill_in_prog)
		goto out;
	dmc->readfill_in_prog = 1;
	while (dmc->readfill_queue != NULL) {
		joblist = dmc->readfill_queue;
		dmc->readfill_queue = NULL;
		spin_unlock_irqrestore(&dmc->cache_spin_lock, flags);
		for (job = joblist; job != NULL; job = nextjob) {
			struct eio_bio *iebio;
			struct eio_bio *next;

			nextjob = job->next;    /* save for later because 'job' will be freed */
			EIO_ASSERT(job->action == READFILL);
			/* Write to cache device */
			ebio = job->ebio;
			iebio = ebio->eb_next;
			EIO_ASSERT(iebio);
#ifdef CONFIG_TX_CACHE
			TX_CACHE_DEBUG("job: %p", job);
#endif
			/* other iebios are anchored on this bio. Create
			 * jobs for them and then issue ios
			 */
			do {
				struct kcached_job *job;
				int err;
				unsigned long flags;
				index_t index;
				next = iebio->eb_next;
				index = iebio->eb_index;
				if (index == -1) {
					CTRACE("eio_do_readfill:1\n");
					/* Any INPROG(including DIRTY_INPROG) case would fall here */
#if CONFIG_SET_SHARING
					/* Because of set sharing, this is wrong path */
					TX_CACHE_DEBUG("read index is -1");
#endif
					eb_endio(iebio, 0);
					iebio = NULL;
				} else {
					spin_lock_irqsave(&dmc->
							  cache_sets[iebio->
								     eb_cacheset].
							  cs_lock, flags);
					/* If this block was already  valid, we don't need to write it */
					if (unlikely
						    (EIO_CACHE_STATE_GET(dmc, index) &
						    QUEUED)) {
						/*An invalidation request is queued. Can't do anything*/
						CTRACE("eio_do_readfill:2\n");
						EIO_CACHE_STATE_SET(dmc, index,
								    INVALID);
						spin_unlock_irqrestore(&dmc->
								       cache_sets
								       [iebio->
									eb_cacheset].
								       cs_lock,
								       flags);
						atomic64_dec_if_positive(&dmc->
									 eio_stats.
									 cached_blocks);
						eb_endio(iebio, 0);
						iebio = NULL;
					} else
					if ((EIO_CACHE_STATE_GET(dmc, index)
					     & (VALID | DISKREADINPROG))
					    == (VALID | DISKREADINPROG)) {
						/* Do readfill. */
						EIO_CACHE_STATE_SET(dmc, index,
								    VALID |
								    CACHEWRITEINPROG);
						EIO_ASSERT(EIO_DBN_GET(dmc, index)
							   == iebio->eb_sector);
						spin_unlock_irqrestore(&dmc->
								       cache_sets
								       [iebio->
									eb_cacheset].
								       cs_lock,
								       flags);
						job =
							eio_new_job(dmc, iebio,
								    iebio->
								    eb_index);
						if (unlikely(job == NULL))
							err = -ENOMEM;
						else {
							err = 0;
							job->action = READFILL;
							atomic_inc(&dmc->
								   nr_jobs);
							SECTOR_STATS(dmc->
								     eio_stats.
								     ssd_readfills,
								     iebio->
								     eb_size);
							SECTOR_STATS(dmc->
								     eio_stats.
								     ssd_writes,
								     iebio->
								     eb_size);
							atomic64_inc(&dmc->
								     eio_stats.
								     readfill);
							atomic64_inc(&dmc->
								     eio_stats.
								     writecache);
							err =
								eio_io_async_bvec
									(dmc,
									&job->
									job_io_regions.
									cache, WRITE,
									iebio->eb_bv,
									iebio->eb_nbvec,
									eio_io_callback,
									job, 0);
						}
						if (err) {
							pr_err
								("eio_do_readfill: IO submission failed, block %llu",
								EIO_DBN_GET(dmc,
									    index));
							spin_lock_irqsave(&dmc->
									  cache_sets
									  [iebio->
									   eb_cacheset].
									  cs_lock,
									  flags);
							EIO_CACHE_STATE_SET(dmc,
									    iebio->
									    eb_index,
									    INVALID);
							spin_unlock_irqrestore
								(&dmc->
								cache_sets[iebio->
									   eb_cacheset].
								cs_lock, flags);
							atomic64_dec_if_positive
								(&dmc->eio_stats.
								cached_blocks);
							eb_endio(iebio, err);

							if (job) {
								eio_free_cache_job
									(job);
								job = NULL;
							}
						}
					} else if (EIO_CACHE_STATE_GET(dmc, index) == ALREADY_DIRTY) {

						spin_unlock_irqrestore(&dmc->
								       cache_sets
								       [iebio->
									eb_cacheset].
								       cs_lock,
								       flags);

						/*
						 * DIRTY block handling:
						 * Read the dirty data from the cache block to update
						 * the data buffer already read from the disk
						 */
						job =
							eio_new_job(dmc, iebio,
								    iebio->
								    eb_index);
						if (unlikely(job == NULL))
							err = -ENOMEM;
						else {
							job->action = READCACHE;
							SECTOR_STATS(dmc->
								     eio_stats.
								     ssd_reads,
								     iebio->
								     eb_size);
							atomic64_inc(&dmc->
								     eio_stats.
								     readcache);
							err =
								eio_io_async_bvec
									(dmc,
									&job->
									job_io_regions.
									cache, READ,
									iebio->eb_bv,
									iebio->eb_nbvec,
									eio_io_callback,
									job, 0);
						}

						if (err) {
							pr_err
								("eio_do_readfill: dirty block read IO submission failed, block %llu",
								EIO_DBN_GET(dmc,
									    index));
							/* can't invalidate the DIRTY block, just return error */
							eb_endio(iebio, err);
							if (job) {
								eio_free_cache_job
									(job);
								job = NULL;
							}
						}
					} else
					if ((EIO_CACHE_STATE_GET(dmc, index)
					     & (VALID | CACHEREADINPROG))
					    == (VALID | CACHEREADINPROG)) {
						/*turn off the cache read in prog flag
						   don't need to write the cache block*/
						CTRACE("eio_do_readfill:3\n");
						EIO_CACHE_STATE_OFF(dmc, index,
								    BLOCK_IO_INPROG);
						spin_unlock_irqrestore(&dmc->
								       cache_sets
								       [iebio->
									eb_cacheset].
								       cs_lock,
								       flags);
						eb_endio(iebio, 0);
						iebio = NULL;
					} else {
						panic("Unknown condition");
						spin_unlock_irqrestore(&dmc->
								       cache_sets
								       [iebio->
									eb_cacheset].
								       cs_lock,
								       flags);
					}
				}
				iebio = next;
			} while (iebio);
			eb_endio(ebio, 0);
			ebio = NULL;
			eio_free_cache_job(job);
		}
		spin_lock_irqsave(&dmc->cache_spin_lock, flags);
	}
	dmc->readfill_in_prog = 0;
out:
	spin_unlock_irqrestore(&dmc->cache_spin_lock, flags);
	atomic64_inc(&dmc->eio_stats.ssd_readfill_unplugs);
	eio_unplug_cache_device(dmc);
}

/*
 * Map a block from the source device to a block in the cache device.
 */
static u_int32_t hash_block(struct cache_c *dmc, sector_t dbn)
{
	u_int32_t set_number;

	set_number = eio_hash_block(dmc, dbn);
	return set_number;
}

static void
find_valid_dbn(struct cache_c *dmc, sector_t dbn,
	       index_t start_index, index_t *index)
{
	index_t i;
	index_t end_index = start_index + dmc->assoc;

	for (i = start_index; i < end_index; i++) {
		if ((EIO_CACHE_STATE_GET(dmc, i) & VALID)
		    && EIO_DBN_GET(dmc, i) == dbn) {
			*index = i;
			if ((EIO_CACHE_STATE_GET(dmc, i) & BLOCK_IO_INPROG) ==
			    0)
				eio_policy_reclaim_lru_movetail(dmc, i,
								dmc->
								policy_ops);
			return;
		}
	}
	*index = -1;
}

static index_t find_invalid_dbn(struct cache_c *dmc, index_t start_index)
{
	index_t i;
	index_t end_index = start_index + dmc->assoc;

	/* Find INVALID slot that we can reuse */
	for (i = start_index; i < end_index; i++) {
		if (EIO_CACHE_STATE_GET(dmc, i) == INVALID) {
			eio_policy_reclaim_lru_movetail(dmc, i,
							dmc->policy_ops);
			return i;
		}
	}
	return -1;
}

#ifdef CONFIG_TX_CACHE
static index_t find_invalid_dbn_shadow(struct cache_c *dmc, index_t start_index, index_t curr_index)
{
	index_t i;
	index_t end_index = start_index + dmc->assoc;

	if (curr_index < start_index || curr_index >= end_index)
		curr_index = start_index;
	else 
		curr_index++;

	for (i = curr_index; i < end_index; i++) {
		if (EIO_CACHE_STATE_GET(dmc, i) == INVALID) {
			eio_policy_reclaim_lru_movetail(dmc, i, dmc->policy_ops);

			return i;
		}
	}
	return -1;
}
#endif
/* Search for a slot that we can reclaim */
static void
find_reclaim_dbn(struct cache_c *dmc, index_t start_index, index_t *index)
{
	eio_find_reclaim_dbn(dmc->policy_ops, start_index, index);
}

void eio_set_warm_boot(void)
{
	eio_force_warm_boot = 1;
	return;
}

/*
 * dbn is the starting sector.
 */
static int
eio_lookup(struct cache_c *dmc, struct eio_bio *ebio, index_t *index)
{
	sector_t dbn = EIO_ROUND_SECTOR(dmc, ebio->eb_sector);
	u_int32_t set_number;
	index_t invalid, oldest_clean = -1;
	index_t start_index;

	/*ASK it is assumed that the lookup is being done for a single block*/
	set_number = hash_block(dmc, dbn);
	start_index = dmc->assoc * set_number;
	find_valid_dbn(dmc, dbn, start_index, index);
	if (*index >= 0)
		/* We found the exact range of blocks we are looking for */
		return VALID;

	invalid = find_invalid_dbn(dmc, start_index);
	if (invalid == -1)
		/* We didn't find an invalid entry, search for oldest valid entry */
		find_reclaim_dbn(dmc, start_index, &oldest_clean);
	/*
	 * Cache miss :
	 * We can't choose an entry marked INPROG, but choose the oldest
	 * INVALID or the oldest VALID entry.
	 */
	*index = start_index + dmc->assoc;
	if (invalid != -1) {
		*index = invalid;
		return INVALID;
	} else if (oldest_clean != -1) {
		*index = oldest_clean;
		return VALID;
	}
	return -1;
}

#ifdef CONFIG_TX_CACHE
/* we separate oldest_clean lookup from original lookup function */
static int
tx_cache_lookup(struct cache_c *dmc, struct eio_bio *ebio, index_t *index)
{
	sector_t dbn = EIO_ROUND_SECTOR(dmc, ebio->eb_sector);
	u_int32_t set_number;
	index_t invalid = -1;
	index_t start_index;

	/*ASK it is assumed that the lookup is being done for a single block*/
	//set_number = hash_block(dmc, dbn);
	set_number = ebio->eb_cacheset;
	start_index = dmc->assoc * set_number;
	find_valid_dbn(dmc, dbn, start_index, index);
	
	if (*index >= 0)
		/* We found the exact range of blocks we are looking for */
		return VALID;

	invalid = find_invalid_dbn(dmc, start_index);
	/*
	 * Cache miss :
	 * We can't choose an entry marked INPROG, but choose the oldest
	 * INVALID or the oldest VALID entry.
	 */
	*index = start_index + dmc->assoc;
	if (invalid != -1) {
		*index = invalid;
		return INVALID;
	}
	return -1;

}

static int
tx_cache_lookup_oldest_clean(struct cache_c *dmc, struct eio_bio *ebio, index_t *index)
{
	u_int32_t set_number;
	index_t oldest_clean = -1;
	index_t start_index;

	set_number = ebio->eb_cacheset;
	start_index = dmc->assoc * set_number;
	
	find_reclaim_dbn(dmc, start_index, &oldest_clean);

	if (oldest_clean != -1) {
		*index = oldest_clean;
		return VALID;
	}

	return -1;
}
/* this function finds invalid/valid cache block without tx-flag */
static int 
tx_cache_lookup_shadow(struct cache_c *dmc, struct eio_bio *ebio, index_t *index)
{
	//sector_t dbn = EIO_ROUND_SECTOR(dmc, ebio->eb_sector);
	u_int32_t set_number;
	index_t invalid, oldest_clean = -1;
	index_t start_index;
	
	set_number = ebio->eb_cacheset;
	start_index = dmc->assoc * set_number;
	invalid = -1;

retry:
	invalid = find_invalid_dbn_shadow(dmc, start_index, invalid);
	if (invalid == -1)
		find_reclaim_dbn(dmc, start_index, &oldest_clean);
	
	*index = start_index + dmc->assoc;

	if (invalid != -1) {
		if (TX_CACHE_STATE_TEST(dmc, invalid, SHADOW) || TX_CACHE_STATE_TEST(dmc, invalid, PERSIST)) {
			TX_CACHE_DEBUG("dangerous choice(%d), let's retry", invalid);
			goto retry;
		}
		*index = invalid;
		return INVALID;
	} else if (oldest_clean != -1) {
		if (TX_CACHE_STATE_TEST(dmc, oldest_clean, SHADOW) || TX_CACHE_STATE_TEST(dmc, oldest_clean, PERSIST)) {
			TX_CACHE_DEBUG("dangerous choice(oldest:%d), let's retry", oldest_clean);
			goto retry;
		}
		TX_CACHE_DEBUG("oldest clean(%d)", oldest_clean);
		*index = oldest_clean;
		return VALID;
	}

	return -1;
}

#endif	//CONFIG_TX_CACHE
#if CONFIG_SET_SHARING

/* caller must hold header lock 
 * if the function find matched block, set the index value and return with appropriate set lock 
 */
static int 
tx_cache_lookup_rental_set_span(struct cache_c *dmc, struct eio_bio *ebio, index_t *index, int skip)
{
	index_t rent_index;
	struct set_chain *chain;
	unsigned orig_set;
	unsigned long flag;
	int ret, find = 0;

	orig_set = ebio->eb_cacheset; 
	
	list_for_each_entry(chain, &dmc->set_headers[orig_set].chain, chain) {
		ebio->eb_cacheset = chain->rental_set;

		if (skip != -1 && ebio->eb_cacheset == skip)
			continue;

		spin_lock(&dmc->cache_sets[ebio->eb_cacheset].cs_lock);		
		ret = tx_cache_lookup(dmc, ebio, &rent_index);

		if (ret == VALID && TX_CACHE_STATE_RENTED(dmc, rent_index)) {
			TX_CACHE_DEBUG("find %d in rented set %d", rent_index, ebio->eb_cacheset);
			//spin_unlock(&dmc->cache_sets[ebio->eb_cacheset].cs_lock);		
			*index = rent_index;
			find = 1;
			break;
		}
		spin_unlock(&dmc->cache_sets[ebio->eb_cacheset].cs_lock);		
	}

	if (!find) 
		ebio->eb_cacheset = orig_set;

	return find;
}
#endif
#if CONFIG_DELAYED_MD_WRITE
static void eio_post_delayed_mdupdate(struct work_struct *work)
{
	struct mdupdate_request *mdreq;
	struct cache_set *set;
	struct cache_c *dmc;
	int error;
	struct tx_unit *tx;
	index_t set_index;

	mdreq = container_of(work, struct mdupdate_request, work);
	EIO_ASSERT(mdreq != NULL);

	dmc = mdreq->dmc;
	EIO_ASSERT(dmc);
	set_index = mdreq->set;
	set = &dmc->cache_sets[set_index];
	tx = mdreq->tx;
	error = mdreq->error;

	/*
	 * Update dirty inprog blocks.
	 * On error, convert them to INVALID
	 * On success, convert them to ALREADY_DIRTY
	 */
	if (unlikely(error)) {
		TX_CACHE_ERROR("error occurred");
		tx->tx_status	= TX_IN_ABORT;
	} 

	set->mdreq = NULL;
	
	/* End the processed I/Os */
	/*
	 * No more pending mdupdates.
	 * Free the mdreq.
	 */	
	if (mdreq->mdblk_bvecs) {
		eio_free_wb_bvecs(mdreq->mdblk_bvecs,
				  mdreq->mdbvec_count,
				  SECTORS_PER_PAGE);
		kfree(mdreq->mdblk_bvecs);
	}

	kfree(mdreq);
	
	if (!atomic_dec_and_test(&tx->holdcount))
		return;
	
	EIO_SET_EVENT_NOLOCK(&tx->mdreq_complete);
}

static void eio_delayed_mdupdate_callback(int error, void *context)
{
	struct work_struct *work = (struct work_struct *)context;
	struct mdupdate_request *mdreq;

	mdreq = container_of(work, struct mdupdate_request, work);

	if (error && !(mdreq->error))
		mdreq->error = error;
	if (!atomic_dec_and_test(&mdreq->holdcount)) {
		return;
	}
	
	INIT_WORK(&mdreq->work, eio_post_delayed_mdupdate);
	queue_work(mdreq->dmc->mdupdate_q, &mdreq->work);
}

static void eio_do_delayed_mdupdate(struct work_struct *work)
{
	struct mdupdate_request *mdreq;
	struct cache_set *set;
	struct cache_c *dmc;
	index_t i;
	index_t start_index;
	index_t end_index;
	struct eio_io_region region;
	int error, j;
	int startbit, endbit;
	int rw_flags = 0;

	mdreq = container_of(work, struct mdupdate_request, work);
	dmc = mdreq->dmc;
	set = &dmc->cache_sets[mdreq->set];
	mdreq->error = 0; 
	EIO_ASSERT(mdreq->mdblk_bvecs); 
	
	start_index = mdreq->set * dmc->assoc;
	end_index = start_index + dmc->assoc;
	
	/*	
	 * Initiate the I/O to SSD for on-disk md update.
	 * TBD. Optimize to write only the affected blocks
	 */

	region.bdev = dmc->cache_dev->bdev;

	atomic_set(&mdreq->holdcount, 1);
	for (i = 0; i < mdreq->mdbvec_count; i++) {
		if (!mdreq->sector_bits[i])
			continue;
		startbit = -1;
		j = 0;
		while (startbit == -1) {
			if (mdreq->sector_bits[i] & (1 << j))
				startbit = j;
			j++;
		}
		endbit = -1;
		j = 7;
		while (endbit == -1) {
			if (mdreq->sector_bits[i] & (1 << j))
				endbit = j;
			j--;
		}
		EIO_ASSERT(startbit <= endbit && startbit >= 0 && startbit <= 7 &&
			   endbit >= 0 && endbit <= 7);
		EIO_ASSERT(dmc->assoc != 128 || endbit <= 3);
		region.sector =
			dmc->md_start_sect + INDEX_TO_MD_SECTOR(start_index) +
			i * SECTORS_PER_PAGE + startbit;
		region.count = endbit - startbit + 1;
		mdreq->mdblk_bvecs[i].bv_offset = to_bytes(startbit);
		mdreq->mdblk_bvecs[i].bv_len = to_bytes(region.count);

		EIO_ASSERT(region.sector <=
			   (dmc->md_start_sect + INDEX_TO_MD_SECTOR(end_index)));
		atomic64_inc(&dmc->eio_stats.md_ssd_writes);
		SECTOR_STATS(dmc->eio_stats.ssd_writes, to_bytes(region.count));
		atomic_inc(&mdreq->holdcount);

		/*
		 * Set SYNC for making metadata
		 * writes as high priority.
		 */
		rw_flags = WRITE | REQ_SYNC;
		error = eio_io_async_bvec(dmc, &region, rw_flags,
					  &mdreq->mdblk_bvecs[i], 1,
					  eio_delayed_mdupdate_callback, work, 0);
		if (error && !(mdreq->error))
			mdreq->error = error;
	}
	
	if (atomic_dec_and_test(&mdreq->holdcount)) {
		INIT_WORK(&mdreq->work, eio_post_delayed_mdupdate);
		queue_work(dmc->mdupdate_q, &mdreq->work);
	}
}

static void __tx_cache_delayed_mdupdate(struct bio_container *bc)
{
	unsigned long flags = 0;
	index_t set_index = -1;
	struct eio_bio *ebio, *nebio, *pebio;
	struct cache_c *dmc = bc->bc_dmc;
	int do_pending = 0;
	int error = 0, ret;
	struct mdupdate_cache_set *dirty_set = NULL;
	struct tx_unit *tx;
	u_int64_t tx_flag;

	tx = bc->tx;
	ebio = bc->bc_mdlist;

	/*
	spin_lock_irqsave(&dmc->shadow_lock, flags);
	tx = search_tx_by_id(&dmc->shadow_list, ebio->tx_id);
	spin_unlock_irqrestore(&dmc->shadow_lock, flags);
	*/

	TX_CACHE_DEBUG("bc: %p, bc->bc_setspan: %p ebio->set: %u ebio->eb_index: %d tx: %p", bc, bc->bc_setspan, ebio->eb_cacheset, ebio->eb_index, tx);
	if (atomic_read(&tx->mdupdate_cnt) > 0) {
		//error = eio_alloc_mdreqs(dmc, bc);
		atomic_dec(&tx->mdupdate_cnt);
	}

	spin_lock_irqsave(&tx->mdupdate_lock, flags);
	while (ebio) {
		if (ebio->eb_cacheset != set_index) {
			set_index = ebio->eb_cacheset;
			dirty_set = tx_cache_find_mdupdate_set(&tx->pending_set_list, set_index);
			
			if (!dirty_set) {	
				dirty_set = tx_cache_alloc_mdupdate_set();
				//pending_new = 1;
				do_pending = 1;
				dirty_set->set_num = set_index;
				dirty_set->set_header = ebio->eb_cacheheader;
			} 
			
			spin_lock(&dmc->cache_sets[set_index].cs_lock);
		}
		EIO_ASSERT(ebio->eb_cacheset == set_index);
		
		tx_flag = TX_CACHE_STATE_GET(dmc, ebio->eb_index);
		EIO_CACHE_STATE_SET(dmc, ebio->eb_index, ALREADY_DIRTY | tx_flag);

		TX_CACHE_DEBUG("ebio->index: %d state: %x tx_flag: %x dirty_set: %p", ebio->eb_index, EIO_CACHE_STATE_GET(dmc, ebio->eb_index), tx_flag, dirty_set);
		dmc->cache_sets[set_index].nr_dirty++;
		atomic64_inc(&dmc->nr_dirty);
	
#if 0
		if (!dirty_set->mdreq) {
			/* Pick up one mdreq from bc */
			mdreq = bc->mdreqs;
			EIO_ASSERT(mdreq != NULL);
			bc->mdreqs = bc->mdreqs->next;
			mdreq->next = NULL;
			mdreq->dmc = dmc;
			mdreq->set = set_index;
			dirty_set->mdreq = mdreq;
			dirty_set->set_num = set_index;
			mdreq->tx = tx;
			do_pending = 1;
		} else {
			mdreq = dirty_set->mdreq;
		}	
#else
		/*
		if (!dirty_set->pending) {
			dirty_set->pending = 1;
			dirty_set->set_num = set_index;
			do_pending = 1;
		} else {

		}
		*/
#endif
		/*
		 * if the set already has mdreq
		 */

		pebio = ebio;
		ebio = ebio->eb_next;
		
		if (!ebio || ebio->eb_cacheset != set_index) {
			spin_unlock(&dmc->cache_sets[set_index].cs_lock);
			
			if (do_pending) {
				//start_index = set_index * dmc->assoc;
				//EIO_ASSERT(EIO_CACHE_STATE_GET(dmc, ebio->eb_index) ==
						//DIRTY_INPROG);

				//blk_index = pebio->eb_index - start_index;
				//pindex = INDEX_TO_MD_PAGE(blk_index);
				//blk_index = INDEX_TO_MD_PAGE_OFFSET(blk_index);
				//mdreq->sector_bits[pindex] |= (1 << INDEX_TO_MD_SECTOR(blk_index));
			
				//if (pending_new) {
				ret = tx_cache_add_mdupdate_set_properly(&tx->pending_set_list, dirty_set);
				if (ret < 0)
					list_add(&dirty_set->plist, &tx->pending_set_list);
					//pending_new = 0;
				//}
				
				//atomic64_inc(&dmc->eio_stats.md_write_dirty);
				do_pending = 0;
			}
		}
	}
	spin_unlock_irqrestore(&tx->mdupdate_lock, flags);

	/*
	 * TODO:
	 *	If dirty set of mdreqs is already in pending list,
	 *  bc->mdreqs still be remained.
	 *  So, we free mdreqs, but if we can allocate mdreqs more precisely,
	 *  this freeing step can be removed.
	 */
#if 0
	while (bc->mdreqs) {
		TX_CACHE_DEBUG("freeing remained mdreqs");
		mdreq = bc->mdreqs;
		bc->mdreqs = mdreq->next;
		if (mdreq->mdblk_bvecs) {
			eio_free_wb_bvecs(mdreq->mdblk_bvecs,
					  mdreq->mdbvec_count,
					  SECTORS_PER_PAGE);
			kfree(mdreq->mdblk_bvecs);
		}
		kfree(mdreq);		
	}

	bc->mdreqs = NULL;
#endif
	ebio = bc->bc_mdlist;

	while (ebio) {
#if CONFIG_SET_SHARING
		spin_lock_irqsave(&dmc->cache_sets[ebio->eb_cacheset].cs_lock, flags);
		PREPARE_LAST_LOC(__LINE__);
		tx_cache_attach_set(dmc, ebio->eb_cacheset);
		spin_unlock_irqrestore(&dmc->cache_sets[ebio->eb_cacheset].cs_lock, flags);
#endif
		nebio = ebio->eb_next;
		tx_cache_eb_endio(ebio, error, tx);
		ebio = nebio;
	}
}

#else	//CONFIG_DELAYED_MD_WRITE

/* Do metadata update for a set */
static void eio_do_mdupdate(struct work_struct *work)
{
	struct mdupdate_request *mdreq;
	struct cache_set *set;
	struct cache_c *dmc;
	unsigned long flags;
	index_t i;
	index_t start_index;
	index_t end_index;
	index_t min_index;
	index_t max_index;
	struct flash_cacheblock *md_blocks;
	struct eio_bio *ebio;
	u_int8_t cstate;
	struct eio_io_region region;
	unsigned pindex;
	int error, j;
	index_t blk_index;
	int k;
	void *pg_virt_addr[2] = { NULL };
	u_int8_t sector_bits[2] = { 0 };
	int startbit, endbit;
	int rw_flags = 0;

	mdreq = container_of(work, struct mdupdate_request, work);
	dmc = mdreq->dmc;
	set = &dmc->cache_sets[mdreq->set];
	mdreq->error = 0; 
	EIO_ASSERT(mdreq->mdblk_bvecs); 
	/*
	 * Currently, md_size is 8192 bytes, mdpage_count is 2 pages maximum.
	 */

	EIO_ASSERT(mdreq->mdbvec_count && mdreq->mdbvec_count <= 2);
	EIO_ASSERT((dmc->assoc == 512) || mdreq->mdbvec_count == 1);
	for (k = 0; k < (int)mdreq->mdbvec_count; k++)
		pg_virt_addr[k] = kmap(mdreq->mdblk_bvecs[k].bv_page);

	spin_lock_irqsave(&set->cs_lock, flags);

	start_index = mdreq->set * dmc->assoc;
	end_index = start_index + dmc->assoc;

	pindex = 0;
	md_blocks = (struct flash_cacheblock *)pg_virt_addr[pindex];
	j = MD_BLOCKS_PER_PAGE;

	/* initialize the md blocks to write */
	for (i = start_index; i < end_index; i++) {
		cstate = EIO_CACHE_STATE_GET(dmc, i);
		md_blocks->dbn = cpu_to_le64(EIO_DBN_GET(dmc, i));
		if (cstate == ALREADY_DIRTY)
			md_blocks->cache_state = cpu_to_le64((VALID | DIRTY));
		else
			md_blocks->cache_state = cpu_to_le64(INVALID);
#ifdef CONFIG_TX_CACHE
		if (TX_CACHE_STATE_TEST(dmc, i, PERSIST))
			md_blocks->cache_state |= cpu_to_le64(PERSIST);
#endif
		md_blocks++;
		j--;

		if ((j == 0) && (++pindex < mdreq->mdbvec_count)) {
			md_blocks =
				(struct flash_cacheblock *)pg_virt_addr[pindex];
			j = MD_BLOCKS_PER_PAGE;
		}

	}

	/* Update the md blocks with the pending mdlist */
	min_index = start_index;
	max_index = start_index;

	pindex = 0;
	md_blocks = (struct flash_cacheblock *)pg_virt_addr[pindex];

	ebio = mdreq->pending_mdlist;
	while (ebio) {
	//	pr_info("%s: cache block(%ld) state is %x\n", __func__, ebio->eb_index, EIO_CACHE_STATE_GET(dmc, ebio->eb_index));
		EIO_ASSERT(EIO_CACHE_STATE_GET(dmc, ebio->eb_index) ==
			   DIRTY_INPROG);

		blk_index = ebio->eb_index - start_index;
		pindex = INDEX_TO_MD_PAGE(blk_index);
		blk_index = INDEX_TO_MD_PAGE_OFFSET(blk_index);
		sector_bits[pindex] |= (1 << INDEX_TO_MD_SECTOR(blk_index));

		md_blocks = (struct flash_cacheblock *)pg_virt_addr[pindex];
		md_blocks[blk_index].cache_state = (VALID | DIRTY);

		if (min_index > ebio->eb_index)
			min_index = ebio->eb_index;

		if (max_index < ebio->eb_index)
			max_index = ebio->eb_index;

		ebio = ebio->eb_next;
	}

	/*
	 * Below code may be required when selective pages need to be
	 * submitted for metadata update. Currently avoiding the optimization
	 * for correctness validation.
	 */

	/*
	   min_cboff = (min_index - start_index) / MD_BLOCKS_PER_CBLOCK(dmc);
	   max_cboff = (max_index - start_index) / MD_BLOCKS_PER_CBLOCK(dmc);
	   write_size = ((uint32_t)(max_cboff - min_cboff + 1)) << dmc->block_shift;
	   EIO_ASSERT(write_size && (write_size <= eio_to_sector(mdreq->md_size)));
	 */

	/* Move the pending mdlist to inprog list */
	mdreq->inprog_mdlist = mdreq->pending_mdlist;
	mdreq->pending_mdlist = NULL;

	spin_unlock_irqrestore(&set->cs_lock, flags);

	for (k = 0; k < (int)mdreq->mdbvec_count; k++)
		kunmap(mdreq->mdblk_bvecs[k].bv_page);

	/*
	 * Initiate the I/O to SSD for on-disk md update.
	 * TBD. Optimize to write only the affected blocks
	 */

	region.bdev = dmc->cache_dev->bdev;
	/*region.sector = dmc->md_start_sect + INDEX_TO_MD_SECTOR(start_index) +
	   (min_cboff << dmc->block_shift); */

	atomic_set(&mdreq->holdcount, 1);
	for (i = 0; i < mdreq->mdbvec_count; i++) {
		if (!sector_bits[i])
			continue;
		startbit = -1;
		j = 0;
		while (startbit == -1) {
			if (sector_bits[i] & (1 << j))
				startbit = j;
			j++;
		}
		endbit = -1;
		j = 7;
		while (endbit == -1) {
			if (sector_bits[i] & (1 << j))
				endbit = j;
			j--;
		}
		EIO_ASSERT(startbit <= endbit && startbit >= 0 && startbit <= 7 &&
			   endbit >= 0 && endbit <= 7);
		EIO_ASSERT(dmc->assoc != 128 || endbit <= 3);
		region.sector =
			dmc->md_start_sect + INDEX_TO_MD_SECTOR(start_index) +
			i * SECTORS_PER_PAGE + startbit;
		region.count = endbit - startbit + 1;
		mdreq->mdblk_bvecs[i].bv_offset = to_bytes(startbit);
		mdreq->mdblk_bvecs[i].bv_len = to_bytes(region.count);

		EIO_ASSERT(region.sector <=
			   (dmc->md_start_sect + INDEX_TO_MD_SECTOR(end_index)));
		atomic64_inc(&dmc->eio_stats.md_ssd_writes);
		SECTOR_STATS(dmc->eio_stats.ssd_writes, to_bytes(region.count));
		atomic_inc(&mdreq->holdcount);

		/*
		 * Set SYNC for making metadata
		 * writes as high priority.
		 */
		rw_flags = WRITE | REQ_SYNC;
		error = eio_io_async_bvec(dmc, &region, rw_flags,
					  &mdreq->mdblk_bvecs[i], 1,
					  eio_mdupdate_callback, work, 0);
		if (error && !(mdreq->error))
			mdreq->error = error;
	}
	if (atomic_dec_and_test(&mdreq->holdcount)) {
		INIT_WORK(&mdreq->work, eio_post_mdupdate);
		queue_work(dmc->mdupdate_q, &mdreq->work);
	}
}

/* Callback function for ondisk metadata update */
static void eio_mdupdate_callback(int error, void *context)
{
	struct work_struct *work = (struct work_struct *)context;
	struct mdupdate_request *mdreq;

	mdreq = container_of(work, struct mdupdate_request, work);
	if (error && !(mdreq->error))
		mdreq->error = error;
	if (!atomic_dec_and_test(&mdreq->holdcount))
		return;
	INIT_WORK(&mdreq->work, eio_post_mdupdate);
	queue_work(mdreq->dmc->mdupdate_q, &mdreq->work);
}


static void eio_post_mdupdate(struct work_struct *work)
{
	struct mdupdate_request *mdreq;
	struct cache_set *set;
	struct cache_c *dmc;
	unsigned long flags;
	struct eio_bio *ebio;
	struct eio_bio *nebio;
	int more_pending_mdupdates = 0;
	int error;
	index_t set_index;
#ifdef CONFIG_TX_CACHE
	u_int64_t tx_flags = 0;
#endif

	mdreq = container_of(work, struct mdupdate_request, work);

	dmc = mdreq->dmc;
	EIO_ASSERT(dmc);
	set_index = mdreq->set;
	set = &dmc->cache_sets[set_index];
	error = mdreq->error;

	/* Update in-core cache metadata */

	spin_lock_irqsave(&set->cs_lock, flags);

	/*
	 * Update dirty inprog blocks.
	 * On error, convert them to INVALID
	 * On success, convert them to ALREADY_DIRTY
	 */
	ebio = mdreq->inprog_mdlist;
	while (ebio) {
		EIO_ASSERT(EIO_CACHE_STATE_GET(dmc, ebio->eb_index) ==
			   DIRTY_INPROG);
		if (unlikely(error)) {
			EIO_CACHE_STATE_SET(dmc, ebio->eb_index, INVALID);
			atomic64_dec_if_positive(&dmc->eio_stats.cached_blocks);
		} else {
#ifdef CONFIG_TX_CACHE
			tx_flags = TX_CACHE_STATE_GET(dmc, ebio->eb_index);
			EIO_CACHE_STATE_SET(dmc, ebio->eb_index, ALREADY_DIRTY | tx_flags);
#else
			EIO_CACHE_STATE_SET(dmc, ebio->eb_index, ALREADY_DIRTY);
			set->nr_dirty++;
			atomic64_inc(&dmc->nr_dirty);
			atomic64_inc(&dmc->eio_stats.md_write_dirty);
#endif	//CONFIG_TX_CACHE
		}
		ebio = ebio->eb_next;
	}

	/*
	 * If there are more pending requests for md update,
	 * need to pick up those using the current mdreq.
	 */
	if (mdreq->pending_mdlist)
		more_pending_mdupdates = 1;
	else
		/* No request pending, we can free the mdreq */
		set->mdreq = NULL;

	/*
	 * After we unlock the set, we need to end the I/Os,
	 * which were processed as part of this md update
	 */

	ebio = mdreq->inprog_mdlist;
	mdreq->inprog_mdlist = NULL;

	spin_unlock_irqrestore(&set->cs_lock, flags);

	/* End the processed I/Os */
	while (ebio) {
		nebio = ebio->eb_next;
		eb_endio(ebio, error);
		ebio = nebio;
	}

	/*
	 * if dirty block was added
	 * 1. update the cache set lru list
	 * 2. check and initiate cleaning if thresholds are crossed
	 */
#ifdef CONFIG_TX_CACHE
	if (!error) {
		spin_lock_irqsave(&dmc->pers_list_lock, flags);
		eio_comply_dirty_thresholds(dmc, set_index);
		spin_unlock_irqrestore(&dmc->pers_list_lock, flags);
	}
#else
	if (!error) {
		eio_touch_set_lru(dmc, set_index);
		eio_comply_dirty_thresholds(dmc, set_index);
	}
#endif

	if (more_pending_mdupdates) {
		/*
		 * Schedule work to process the new
		 * pending mdupdate requests
		 */
		INIT_WORK(&mdreq->work, eio_do_mdupdate);
		queue_work(dmc->mdupdate_q, &mdreq->work);
	} else {
		/*
		 * No more pending mdupdates.
		 * Free the mdreq.
		 */
		if (mdreq->mdblk_bvecs) {
			eio_free_wb_bvecs(mdreq->mdblk_bvecs,
					  mdreq->mdbvec_count,
					  SECTORS_PER_PAGE);
			kfree(mdreq->mdblk_bvecs);
		}

		kfree(mdreq);
	}
}

/* Enqueue metadata update for marking dirty blocks on-disk/in-core */
static void __eio_enq_mdupdate(struct bio_container *bc)
{
	unsigned long flags = 0;
	index_t set_index;
	struct eio_bio *ebio;
	struct cache_c *dmc = bc->bc_dmc;
	struct cache_set *set = NULL;
	struct mdupdate_request *mdreq;
	int do_schedule;

	ebio = bc->bc_mdlist;
	set_index = -1;
	do_schedule = 0;
	while (ebio) {
		if (ebio->eb_cacheset != set_index) {
			set_index = ebio->eb_cacheset;
			set = &dmc->cache_sets[set_index];
			spin_lock_irqsave(&set->cs_lock, flags);
		}
		EIO_ASSERT(ebio->eb_cacheset == set_index);

		bc->bc_mdlist = ebio->eb_next;

		if (!set->mdreq) {
			/* Pick up one mdreq from bc */
			mdreq = bc->mdreqs;
			EIO_ASSERT(mdreq != NULL);
			bc->mdreqs = bc->mdreqs->next;
			mdreq->next = NULL;
			mdreq->pending_mdlist = ebio;
			mdreq->dmc = dmc;
			mdreq->set = set_index;
			set->mdreq = mdreq;
			ebio->eb_next = NULL;
			do_schedule = 1;
		} else {
			mdreq = set->mdreq;
			EIO_ASSERT(mdreq != NULL);
			ebio->eb_next = mdreq->pending_mdlist;
			mdreq->pending_mdlist = ebio;
		}

		ebio = bc->bc_mdlist;
		if (!ebio || ebio->eb_cacheset != set_index) {
			spin_unlock_irqrestore(&set->cs_lock, flags);
			if (do_schedule) {
				INIT_WORK(&mdreq->work, eio_do_mdupdate);
				queue_work(dmc->mdupdate_q, &mdreq->work);
				do_schedule = 0;
			}
		}
	}

	EIO_ASSERT(bc->bc_mdlist == NULL);
}

#endif	//CONFIG_DELAYED_MD_WRITE

static inline void eio_enq_mdupdate(struct bio_container *bc)
{
#if CONFIG_DELAYED_MD_WRITE
	__tx_cache_delayed_mdupdate(bc);
#else
	__eio_enq_mdupdate(bc);
#endif
}

/* Kick-off a cache metadata update for marking the blocks dirty */
void eio_md_write(struct kcached_job *job)
{
	struct eio_bio *ebio = job->ebio;
	struct eio_bio *nebio;
	struct eio_bio *pebio;
	struct bio_container *bc = ebio->eb_bc;
	int enqueue = 0;

	/*
	 * ebios are stored in ascending order of cache sets.
	 */
	TX_CACHE_DEBUG("job: %p bc: %p bc->dmc: %p bc->bc_mdwait: %d bc->bc_mdlist: %p bc->bc_tx: %p", 
		job, bc, bc->bc_dmc, atomic_read(&bc->bc_mdwait), bc->bc_mdlist, bc->tx);

#ifdef CONFIG_TX_CACHE
	EIO_ASSERT(atomic_read(&bc->bc_mdwait) > 0);
#else
	EIO_ASSERT(bc->bc_mdwait > 0);
#endif
	nebio = bc->bc_mdlist;
	pebio = NULL;
	while (nebio) {
		if (nebio->eb_cacheset > ebio->eb_cacheset)
			break;
		pebio = nebio;
		nebio = nebio->eb_next;
	}
	ebio->eb_next = nebio;
	if (!pebio)
		bc->bc_mdlist = ebio;
	else
		pebio->eb_next = ebio;
#ifdef CONFIG_TX_CACHE
	if (atomic_dec_and_test(&bc->bc_mdwait))
		enqueue = 1;
#else
	bc->bc_mdwait--;
	if (bc->bc_mdwait == 0)
		enqueue = 1;
#endif

	eio_free_cache_job(job);
	
	if (enqueue)
		eio_enq_mdupdate(bc);
}

#ifdef CONFIG_TX_CACHE
/* function caller should be hold pers_list_lock */
static void tx_cache_check_persist_cache_thresholds(struct cache_c *dmc)
{
	if (PERSIST_CACHE_THRESHOLD_CROSSED(dmc)) {
		int64_t required_cleans;
		int64_t enqueued_cleans;
		index_t set_index;
		unsigned long flags;
		struct set_list *set;

		spin_lock_irqsave(&dmc->clean_sl, flags);
		if (atomic64_read(&dmc->clean_pendings)
		    || dmc->clean_excess_dirty) {
			/* Already excess dirty block cleaning is in progress */
			spin_unlock_irqrestore(&dmc->clean_sl, flags);
			return;
		}
		dmc->clean_excess_dirty = 1;
		spin_unlock_irqrestore(&dmc->clean_sl, flags);

		/* Clean needs to be triggered on the cache */
		required_cleans = atomic64_read(&dmc->nr_persist) -
				  (EIO_DIV((dmc->sysctl_active.dirty_low_threshold * dmc->size),
					   100));
		enqueued_cleans = 0;

		do {
			set = tx_cache_pers_pick(dmc);

			if (set == NULL)
				break;
			
			set_index = set->set_num;	
			set->q_flag &= ~Q_MASK;
			set->q_flag |= IN_CLEANING;
			
			spin_lock_irqsave(&dmc->cache_sets[set_index].cs_lock, flags);
			enqueued_cleans += dmc->cache_sets[set_index].nr_persist;
			spin_unlock_irqrestore(&dmc->cache_sets[set_index].cs_lock, flags);
			
			eio_addto_cleanq(dmc, set_index, 1);
		} while (enqueued_cleans <= required_cleans);

		spin_lock_irqsave(&dmc->clean_sl, flags);
		dmc->clean_excess_dirty = 0;
		spin_unlock_irqrestore(&dmc->clean_sl, flags);
	}
}

/* function caller must hold pers_list_lock and appropriate cs_lock */

static void tx_cache_check_persist_set_thresholds(struct cache_c *dmc, index_t set)
{
	int ret;

	if (PERSIST_SET_THRESHOLD_CROSSED(dmc, set) && dmc->pers_sets[set].q_flag & IN_QUEUE) {
		TX_CACHE_DEBUG("set(%d) crosses threshold", set);
		ret = tx_cache_pers_rem(dmc, set);
		if (ret) {
			dmc->pers_sets[set].q_flag &= ~Q_MASK;
			dmc->pers_sets[set].q_flag |= IN_CLEANING;
			eio_addto_cleanq(dmc, set, 0);
		}
	}
}
#else
/* Ensure cache level dirty thresholds compliance. If required, trigger cache-wide clean */
static void eio_check_dirty_cache_thresholds(struct cache_c *dmc)
{
	if (DIRTY_CACHE_THRESHOLD_CROSSED(dmc)) {
		int64_t required_cleans;
		int64_t enqueued_cleans;
		u_int64_t set_time;
		index_t set_index;
		unsigned long flags;

		spin_lock_irqsave(&dmc->clean_sl, flags);
		if (atomic64_read(&dmc->clean_pendings)
		    || dmc->clean_excess_dirty) {
			/* Already excess dirty block cleaning is in progress */
			spin_unlock_irqrestore(&dmc->clean_sl, flags);
			return;
		}
		dmc->clean_excess_dirty = 1;
		spin_unlock_irqrestore(&dmc->clean_sl, flags);

		/* Clean needs to be triggered on the cache */
		required_cleans = atomic64_read(&dmc->nr_dirty) -
				  (EIO_DIV((dmc->sysctl_active.dirty_low_threshold * dmc->size),
					   100));
		enqueued_cleans = 0;

		spin_lock_irqsave(&dmc->dirty_set_lru_lock, flags);
		do {
			lru_rem_head(dmc->dirty_set_lru, &set_index, &set_time);
			if (set_index == LRU_NULL)
				break;

			enqueued_cleans += dmc->cache_sets[set_index].nr_dirty;
			spin_unlock_irqrestore(&dmc->dirty_set_lru_lock, flags);
			eio_addto_cleanq(dmc, set_index, 1);
			spin_lock_irqsave(&dmc->dirty_set_lru_lock, flags);
		} while (enqueued_cleans <= required_cleans);
		spin_unlock_irqrestore(&dmc->dirty_set_lru_lock, flags);
		spin_lock_irqsave(&dmc->clean_sl, flags);
		dmc->clean_excess_dirty = 0;
		spin_unlock_irqrestore(&dmc->clean_sl, flags);
	}
}

/* Ensure set level dirty thresholds compliance. If required, trigger set clean */
static void eio_check_dirty_set_thresholds(struct cache_c *dmc, index_t set)
{
	if (DIRTY_SET_THRESHOLD_CROSSED(dmc, set)) {
		eio_addto_cleanq(dmc, set, 0);
		return;
	}
}

#endif	//CONFIG_TX_CACHE

/* Ensure various cache thresholds compliance. If required trigger clean */
void eio_comply_dirty_thresholds(struct cache_c *dmc, index_t set)
{
	/*
	 * 1. Don't trigger new cleanings if
	 *      - cache is not wb
	 *      - autoclean threshold is crossed
	 *      - fast remove in progress is set
	 *      - cache is in failed mode.
	 * 2. Initiate set-wide clean, if set level dirty threshold is crossed
	 * 3. Initiate cache-wide clean, if cache level dirty threshold is crossed
	 */

	if (unlikely(CACHE_FAILED_IS_SET(dmc))) {
		pr_debug
			("eio_comply_dirty_thresholds: Cache %s is in failed mode.\n",
			dmc->cache_name);
		return;
	}

	if (AUTOCLEAN_THRESHOLD_CROSSED(dmc) || (dmc->mode != CACHE_MODE_WB))
		return;
#ifdef CONFIG_TX_CACHE
	if (set != -1) 
		tx_cache_check_persist_set_thresholds(dmc, set);
	tx_cache_check_persist_cache_thresholds(dmc);
#else
	if (set != -1)
		eio_check_dirty_set_thresholds(dmc, set);
	eio_check_dirty_cache_thresholds(dmc);
#endif
}

/* Do read from cache */
static void
eio_cached_read(struct cache_c *dmc, struct eio_bio *ebio, int rw_flags)
{
	struct kcached_job *job;
	index_t index = ebio->eb_index;
	int err = 0;

	job = eio_new_job(dmc, ebio, index);

	if (unlikely(job == NULL))
		err = -ENOMEM;
	else {
		job->action = READCACHE;        /* Fetch data from cache */
		atomic_inc(&dmc->nr_jobs);

		SECTOR_STATS(dmc->eio_stats.read_hits, ebio->eb_size);
		SECTOR_STATS(dmc->eio_stats.ssd_reads, ebio->eb_size);
		atomic64_inc(&dmc->eio_stats.readcache);
		
		err =
			eio_io_async_bvec(dmc, &job->job_io_regions.cache, rw_flags,
					  ebio->eb_bv, ebio->eb_nbvec,
					  eio_io_callback, job, 0);

	}
	if (err) {
		unsigned long flags;
		pr_err("eio_cached_read: IO submission failed, block %llu",
		       EIO_DBN_GET(dmc, index));
		spin_lock_irqsave(&dmc->cache_sets[ebio->eb_cacheset].cs_lock,
				  flags);
		/*
		 * For already DIRTY block, invalidation is too costly, skip it.
		 * For others, mark the block as INVALID and return error.
		 */
		if (EIO_CACHE_STATE_GET(dmc, ebio->eb_index) != ALREADY_DIRTY) {
			EIO_CACHE_STATE_SET(dmc, ebio->eb_index, INVALID);
			atomic64_dec_if_positive(&dmc->eio_stats.cached_blocks);
		}
		spin_unlock_irqrestore(&dmc->cache_sets[ebio->eb_cacheset].
				       cs_lock, flags);
		eb_endio(ebio, err);
		ebio = NULL;
		if (job) {
			job->ebio = NULL;
			eio_free_cache_job(job);
			job = NULL;
		}
	}
}

/*
 * Invalidate any colliding blocks if they are !BUSY and !DIRTY.  In BUSY case,
 * we need to wait until the underlying IO is finished, and then proceed with
 * the invalidation, so a QUEUED flag is added.
 */
static int
eio_inval_block_set_range(struct cache_c *dmc, int set, sector_t iosector,
			  unsigned iosize, int multiblk)
{
	int start_index, end_index, i;
	sector_t endsector = iosector + eio_to_sector(iosize);

	start_index = dmc->assoc * set;
	end_index = start_index + dmc->assoc;
	for (i = start_index; i < end_index; i++) {
		sector_t start_dbn;
		sector_t end_dbn;

		if (EIO_CACHE_STATE_GET(dmc, i) & INVALID)
			continue;
		start_dbn = EIO_DBN_GET(dmc, i);
		end_dbn = start_dbn + dmc->block_size;

		if (!(endsector <= start_dbn || iosector >= end_dbn)) {

			if (!
			    (EIO_CACHE_STATE_GET(dmc, i) &
			     (BLOCK_IO_INPROG | DIRTY | QUEUED))) {
				EIO_CACHE_STATE_SET(dmc, i, INVALID);
				atomic64_dec_if_positive(&dmc->eio_stats.
							 cached_blocks);
				if (multiblk)
					continue;
				return 0;
			}

			/* Skip queued flag for DIRTY(inprog or otherwise) blocks. */
			if (!(EIO_CACHE_STATE_GET(dmc, i) & (DIRTY | QUEUED)))
				/* BLOCK_IO_INPROG is set. Set QUEUED flag */
				EIO_CACHE_STATE_ON(dmc, i, QUEUED);

			if (!multiblk)
				return 1;
		}
	}
	return 0;
}

int
eio_invalidate_sanity_check(struct cache_c *dmc, u_int64_t iosector,
			    u_int64_t *num_sectors)
{
	u_int64_t disk_size;

	/*
	 * Sanity check the arguements
	 */
	if (unlikely(*num_sectors == 0)) {
		pr_info
			("invaldate_sector_range: nothing to do because number of sectors specified is zero");
		return -EINVAL;
	}

	disk_size = eio_to_sector(eio_get_device_size(dmc->disk_dev));
	if (iosector >= disk_size) {
		pr_err
			("eio_inval_range: nothing to do because starting sector is past last sector (%lu > %lu)",
			(long unsigned int)iosector, (long unsigned int)disk_size);
		return -EINVAL;
	}

	if ((iosector + (*num_sectors)) > disk_size) {
		pr_info
			("eio_inval_range: trimming range because there are less sectors to invalidate than requested. (%lu < %lu)",
			(long unsigned int)(disk_size - iosector),
			(long unsigned int)*num_sectors);
		*num_sectors = (disk_size - iosector);
	}

	return 0;
}

void eio_inval_range(struct cache_c *dmc, sector_t iosector, unsigned iosize)
{
	u_int32_t bset;
	sector_t snum;
	sector_t snext;
	unsigned ioinset;
	unsigned long flags;
	int totalsshift = dmc->block_shift + dmc->consecutive_shift;

	snum = iosector;
	while (iosize) {
		bset = hash_block(dmc, snum);
		snext = ((snum >> totalsshift) + 1) << totalsshift;
		ioinset = (unsigned)to_bytes(snext - snum);
		if (ioinset > iosize)
			ioinset = iosize;
		spin_lock_irqsave(&dmc->cache_sets[bset].cs_lock, flags);
		eio_inval_block_set_range(dmc, bset, snum, ioinset, 1);
		spin_unlock_irqrestore(&dmc->cache_sets[bset].cs_lock, flags);
		snum = snext;
		iosize -= ioinset;
	}
}

/*
 * Invalidates all cached blocks without waiting for them to complete
 * Should be called with incoming IO suspended
 */
int eio_invalidate_cache(struct cache_c *dmc)
{
	u_int64_t i = 0;
	unsigned long flags = 0;
	sector_t disk_dev_size = to_bytes(eio_get_device_size(dmc->disk_dev));

	/* invalidate the whole cache */
	for (i = 0; i < (dmc->size >> dmc->consecutive_shift); i++) {
		spin_lock_irqsave(&dmc->cache_sets[i].cs_lock, flags);
		/* TBD. Apply proper fix for the cast to disk_dev_size */
		(void)eio_inval_block_set_range(dmc, (int)i, 0,
						(unsigned)disk_dev_size, 0);
		spin_unlock_irqrestore(&dmc->cache_sets[i].cs_lock, flags);
	}                       /* end - for all cachesets (i) */

	return 0;               /* i suspect we may need to return different statuses in the future */
}                               /* eio_invalidate_cache */

static int eio_inval_block(struct cache_c *dmc, sector_t iosector)
{
	u_int32_t bset;
	int queued;

	/*Chop lower bits of iosector*/
	iosector = EIO_ROUND_SECTOR(dmc, iosector);
	bset = hash_block(dmc, iosector);
	queued = eio_inval_block_set_range(dmc, bset, iosector,
					   (unsigned)to_bytes(dmc->block_size),
					   0);

	return queued;
}

/* Serving write I/Os, that involves both SSD and HDD */
static int eio_uncached_write(struct cache_c *dmc, struct eio_bio *ebio)
{
	struct kcached_job *job;
	int err = 0;
	index_t index = ebio->eb_index;
	unsigned long flags = 0;
	u_int8_t cstate;

	if (index == -1) {
		/*
		 * No work, if block is not allocated.
		 * Ensure, invalidation of the block at the end
		 */
		ebio->eb_iotype |= EB_INVAL;
		return 0;
	}

	spin_lock_irqsave(&dmc->cache_sets[ebio->eb_cacheset].cs_lock, flags);
	cstate = EIO_CACHE_STATE_GET(dmc, index);
	EIO_ASSERT(cstate & (DIRTY | CACHEWRITEINPROG));
	if (cstate == ALREADY_DIRTY) {
		/*
		 * Treat the dirty block cache write failure as
		 * I/O failure for the entire I/O
		 * TBD
		 * Can we live without this restriction
		 */
		ebio->eb_iotype = EB_MAIN_IO;

		/*
		 * We don't set inprog flag on dirty block.
		 * In lieu of the inprog flag, we are using the
		 * eb_holdcount for dirty block, so that the
		 * endio can be called, only when the write to disk
		 * and the write to cache both complete for the ebio
		 */
		atomic_inc(&ebio->eb_holdcount);
	} else
		/* ensure DISKWRITEINPROG for uncached write on non-DIRTY blocks */
		EIO_CACHE_STATE_ON(dmc, index, DISKWRITEINPROG);

	spin_unlock_irqrestore(&dmc->cache_sets[ebio->eb_cacheset].cs_lock,
			       flags);

	job = eio_new_job(dmc, ebio, index);
	if (unlikely(job == NULL))
		err = -ENOMEM;
	else {
		job->action = WRITECACHE;
		SECTOR_STATS(dmc->eio_stats.ssd_writes, ebio->eb_size);
		atomic64_inc(&dmc->eio_stats.writecache);
		err = eio_io_async_bvec(dmc, &job->job_io_regions.cache, WRITE,
					ebio->eb_bv, ebio->eb_nbvec,
					eio_io_callback, job, 0);
	}

	if (err) {
		pr_err("eio_uncached_write: IO submission failed, block %llu",
		       EIO_DBN_GET(dmc, index));
		spin_lock_irqsave(&dmc->cache_sets[ebio->eb_cacheset].cs_lock,
				  flags);
		if (EIO_CACHE_STATE_GET(dmc, ebio->eb_index) == ALREADY_DIRTY)
			/*
			 * Treat I/O failure on a DIRTY block as failure of entire I/O.
			 * TBD
			 * Can do better error handling by invalidation of the dirty
			 * block, if the cache block write failed, but disk write succeeded
			 */
			ebio->eb_bc->bc_error = err;
		else {
			/* Mark the block as INVALID for non-DIRTY block. */
			//pr_info("%s: INVALIDATE block(%ld)\n", __func__, ebio->eb_index);
			EIO_CACHE_STATE_SET(dmc, ebio->eb_index, INVALID);
			atomic64_dec_if_positive(&dmc->eio_stats.cached_blocks);
			/* Set the INVAL flag to ensure block is marked invalid at the end */
			ebio->eb_iotype |= EB_INVAL;
			ebio->eb_index = -1;
		}
		spin_unlock_irqrestore(&dmc->cache_sets[ebio->eb_cacheset].
				       cs_lock, flags);
		if (job) {
			job->ebio = NULL;
			eio_free_cache_job(job);
			job = NULL;
		}
	}

	return err;
}

/* Serving write I/Os that can be fulfilled just by SSD */
static int
eio_cached_write(struct cache_c *dmc, struct eio_bio *ebio, int rw_flags)
{
	struct kcached_job *job;
	int err = 0;
	index_t index = ebio->eb_index;
	unsigned long flags = 0;
	u_int8_t cstate;
#ifdef CONFIG_TX_CACHE
	struct bio_container *bc;	//for debugging
	u_int32_t tx_flags = 0;
#endif
	/*
	 * WRITE (I->DV)
	 * WRITE (V->DV)
	 * WRITE (V1->DV2)
	 * WRITE (DV->DV)
	 */

	/* Possible only in writeback caching mode */
	EIO_ASSERT(dmc->mode == CACHE_MODE_WB);

	/*
	 * TBD
	 * Possibly don't need the spinlock-unlock here
	 */
	spin_lock_irqsave(&dmc->cache_sets[ebio->eb_cacheset].cs_lock, flags);
	cstate = EIO_CACHE_STATE_GET(dmc, index);
	if (!(cstate & DIRTY)) {
		EIO_ASSERT(cstate & CACHEWRITEINPROG);
		/* make sure the block is marked DIRTY inprogress */
#ifdef CONFIG_TX_CACHE
		tx_flags = TX_CACHE_STATE_GET(dmc, index);
		EIO_CACHE_STATE_SET(dmc, index, DIRTY_INPROG | tx_flags);
#else
		EIO_CACHE_STATE_SET(dmc, index, DIRTY_INPROG);
#endif
	}
	spin_unlock_irqrestore(&dmc->cache_sets[ebio->eb_cacheset].cs_lock,
			       flags);

	job = eio_new_job(dmc, ebio, index);
	if (unlikely(job == NULL))
		err = -ENOMEM;
	else {
		job->action = WRITECACHE;

		SECTOR_STATS(dmc->eio_stats.ssd_writes, ebio->eb_size);
		atomic64_inc(&dmc->eio_stats.writecache);
		EIO_ASSERT((rw_flags & 1) == WRITE);
		
#ifdef CONFIG_TX_CACHE
		bc = ebio->eb_bc;	
		TX_CACHE_DEBUG("job: %p bc: %p bc->bc_mdwait: %d bc->tx->tx_id: %d", job, bc, atomic_read(&bc->bc_mdwait), bc->tx->tx_id);
#endif
		err =
			eio_io_async_bvec(dmc, &job->job_io_regions.cache, rw_flags,
					  ebio->eb_bv, ebio->eb_nbvec,
					  eio_io_callback, job, 0);
	}

	if (err) {
		pr_err("eio_cached_write: IO submission failed, block %llu",
		       EIO_DBN_GET(dmc, index));
		spin_lock_irqsave(&dmc->cache_sets[ebio->eb_cacheset].cs_lock,
				  flags);
		
		cstate = EIO_CACHE_STATE_GET(dmc, index);
		
		if (cstate == DIRTY_INPROG) {
			/* A DIRTY(inprog) block should be invalidated on error */
			//pr_info("%s: INVALIDATE block(%ld)\n", __func__, ebio->eb_index);
			EIO_CACHE_STATE_SET(dmc, ebio->eb_index, INVALID);
			atomic64_dec_if_positive(&dmc->eio_stats.cached_blocks);
		} else
			/* An already DIRTY block don't have an option but just return error. */
			EIO_ASSERT(cstate == ALREADY_DIRTY);
		spin_unlock_irqrestore(&dmc->cache_sets[ebio->eb_cacheset].
				       cs_lock, flags);
		eb_endio(ebio, err);
		ebio = NULL;
		if (job) {
			job->ebio = NULL;
			eio_free_cache_job(job);
			job = NULL;
		}
	}

	return err;
}

static struct eio_bio *eio_new_ebio(struct cache_c *dmc, struct bio *bio,
				    unsigned *presidual_biovec, sector_t snum,
				    int iosize, struct bio_container *bc,
				    int iotype)
{
	struct eio_bio *ebio;
	int residual_biovec = *presidual_biovec;
	int numbvecs = 0;
	int ios; 

	if (residual_biovec) {
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(3,14,0))
		int bvecindex = bio->bi_iter.bi_idx;
#else 
		int bvecindex = bio->bi_idx;
#endif 
		int rbvindex;

		/* Calculate the number of bvecs required */
		ios = iosize;
		while (ios > 0) {
			int len;

			if (ios == iosize)
				len =
					bio->bi_io_vec[bvecindex].bv_len -
					residual_biovec;
			else
				len = bio->bi_io_vec[bvecindex].bv_len;

			numbvecs++;
			if (len > ios)
				len = ios;
			ios -= len;
			bvecindex++;
		}
		ebio =
			kmalloc(sizeof(struct eio_bio) +
				numbvecs * sizeof(struct bio_vec), GFP_NOWAIT);

		if (!ebio)
			return ERR_PTR(-ENOMEM);
		
		rbvindex = 0;
		ios = iosize;
		while (ios > 0) {
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(3,14,0))
			ebio->eb_rbv[rbvindex].bv_page =
				bio->bi_io_vec[bio->bi_iter.bi_idx].bv_page;
			ebio->eb_rbv[rbvindex].bv_offset =
				bio->bi_io_vec[bio->bi_iter.bi_idx].bv_offset +
				residual_biovec;
			ebio->eb_rbv[rbvindex].bv_len =
				bio->bi_io_vec[bio->bi_iter.bi_idx].bv_len -
				residual_biovec;
#else 
			ebio->eb_rbv[rbvindex].bv_page =
				bio->bi_io_vec[bio->bi_idx].bv_page;
			ebio->eb_rbv[rbvindex].bv_offset =
				bio->bi_io_vec[bio->bi_idx].bv_offset +
				residual_biovec;
			ebio->eb_rbv[rbvindex].bv_len =
				bio->bi_io_vec[bio->bi_idx].bv_len -
				residual_biovec;
#endif 
			if (ebio->eb_rbv[rbvindex].bv_len > (unsigned)ios) {
				residual_biovec += ios;
				ebio->eb_rbv[rbvindex].bv_len = ios;
			} else {
				residual_biovec = 0;
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(3,14,0))
				bio->bi_iter.bi_idx++;
#else 
				bio->bi_idx++;
#endif 
			}
			ios -= ebio->eb_rbv[rbvindex].bv_len;
			rbvindex++;
		}
		if (rbvindex != numbvecs)
			TX_CACHE_INFO("rbvindex: %d numbvecs: %d", rbvindex, numbvecs);
		EIO_ASSERT(rbvindex == numbvecs);
		ebio->eb_bv = ebio->eb_rbv;
	} else {
		ebio = kmalloc(sizeof(struct eio_bio), GFP_NOWAIT);

		if (!ebio)
			return ERR_PTR(-ENOMEM);
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(3,14,0))
		ebio->eb_bv = bio->bi_io_vec + bio->bi_iter.bi_idx;
#else 
		ebio->eb_bv = bio->bi_io_vec + bio->bi_idx;
#endif 
		ios = iosize;
		while (ios > 0) {
			numbvecs++;
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(3,14,0))
			if ((unsigned)ios < bio->bi_io_vec[bio->bi_iter.bi_idx].bv_len) {
#else 
			if ((unsigned)ios < bio->bi_io_vec[bio->bi_idx].bv_len) {
#endif 
				residual_biovec = ios;
				ios = 0;
			} else {
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(3,14,0))
				ios -= bio->bi_io_vec[bio->bi_iter.bi_idx].bv_len;
				bio->bi_iter.bi_idx++;
#else 
				ios -= bio->bi_io_vec[bio->bi_idx].bv_len;
				bio->bi_idx++;
#endif 
			}
		}
	}
	EIO_ASSERT(ios == 0);
	EIO_ASSERT(numbvecs != 0);
	*presidual_biovec = residual_biovec;

	ebio->eb_sector = snum;
	ebio->eb_cacheheader = ebio->eb_cacheset = hash_block(dmc, snum);	/*TX_CACHE: index of set header*/
	ebio->eb_size = iosize;
	ebio->eb_dir = bio_data_dir(bio);
	ebio->eb_next = NULL;
	ebio->eb_index = -1;
	ebio->eb_iotype = iotype;
	ebio->eb_nbvec = numbvecs;

#ifdef CONFIG_TX_CACHE
	ebio->tx_id		= bio->bi_tx_id;
	//pr_info("%s: ebio->tx_id (%ld)\n", __func__, bio->bi_tx_id);
#endif

	bc_addfb(bc, ebio);

	/* Always set the holdcount for eb to 1, to begin with. */
	atomic_set(&ebio->eb_holdcount, 1);

	return ebio;
}

/* Issues HDD I/O */
static void
eio_disk_io(struct cache_c *dmc, struct bio *bio,
	    struct eio_bio *anchored_bios, struct bio_container *bc,
	    int force_inval)
{
	struct eio_bio *ebio;
	struct kcached_job *job;
	int residual_biovec = 0;
	int error = 0;
	unsigned long flags;

	spin_lock_irqsave(&dmc->ebio_lock, flags);	
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(3,14,0))
	/*disk io happens on whole bio. Reset bi_iter.bi_idx*/
	bio->bi_iter.bi_idx = 0;
	ebio =
		eio_new_ebio(dmc, bio, &residual_biovec, bio->bi_iter.bi_sector,
				 bio->bi_iter.bi_size, bc, EB_MAIN_IO);
#else 
	/*disk io happens on whole bio. Reset bi_idx*/
	bio->bi_idx = 0;
	ebio =
		eio_new_ebio(dmc, bio, &residual_biovec, bio->bi_sector,
			     bio->bi_size, bc, EB_MAIN_IO);
#endif 
	spin_unlock_irqrestore(&dmc->ebio_lock, flags);	
	if (unlikely(IS_ERR(ebio))) {
		bc->bc_error = error = PTR_ERR(ebio);
		ebio = NULL;
		goto errout;
	}

	//PR_EBIO_INFO(ebio);
	
	if (force_inval)
		ebio->eb_iotype |= EB_INVAL;
	ebio->eb_next = anchored_bios;  /*Anchor the ebio list to this super bio*/
	job = eio_new_job(dmc, ebio, -1);

	if (unlikely(job == NULL)) {
		error = -ENOMEM;
		goto errout;
	}
	atomic_inc(&dmc->nr_jobs);
	if (ebio->eb_dir == READ) {
		job->action = READDISK;
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(3,14,0))
		SECTOR_STATS(dmc->eio_stats.disk_reads, bio->bi_iter.bi_size);
#else 
		SECTOR_STATS(dmc->eio_stats.disk_reads, bio->bi_size);
#endif 
		atomic64_inc(&dmc->eio_stats.readdisk);
	} else {
		job->action = WRITEDISK;
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(3,14,0))
		SECTOR_STATS(dmc->eio_stats.disk_writes, bio->bi_iter.bi_size);
#else 
		SECTOR_STATS(dmc->eio_stats.disk_writes, bio->bi_size);
#endif 
		atomic64_inc(&dmc->eio_stats.writedisk);
	}

	/*
	 * Pass the original bio flags as is, while doing
	 * read / write to HDD.
	 */
	VERIFY_BIO_FLAGS(ebio);
	error = eio_io_async_bvec(dmc, &job->job_io_regions.disk,
				  GET_BIO_FLAGS(ebio),
				  ebio->eb_bv, ebio->eb_nbvec,
				  eio_io_callback, job, 1);

	if (error) {
		job->ebio = NULL;
		eio_free_cache_job(job);
		goto errout;
	}
	return;

errout:
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(3,14,0))
	eio_inval_range(dmc, bio->bi_iter.bi_sector, bio->bi_iter.bi_size);
#else 
	eio_inval_range(dmc, bio->bi_sector, bio->bi_size);
#endif 
	eio_flag_abios(dmc, anchored_bios, error);

	if (ebio)
		eb_endio(ebio, error);
	return;
}

/*Given a sector number and biosize, returns cache io size*/
static unsigned int
eio_get_iosize(struct cache_c *dmc, sector_t snum, unsigned int biosize)
{
	unsigned int iosize;
	unsigned int swithinblock = snum & (dmc->block_size - 1);

	/*Check whether io starts at a cache block boundary*/
	if (swithinblock)
		iosize = (unsigned int)to_bytes(dmc->block_size - swithinblock);
	else
		iosize = (unsigned int)to_bytes(dmc->block_size);
	if (iosize > biosize)
		iosize = biosize;
	return iosize;
}

/* Insert a new set sequence in sorted order to existing set sequence list */
static int
insert_set_seq(struct set_seq **seq_list, index_t first_set, index_t last_set)
{
	struct set_seq *cur_seq = NULL;
	struct set_seq *prev_seq = NULL;
	struct set_seq *new_seq = NULL;

	EIO_ASSERT((first_set != -1) && (last_set != -1)
		   && (last_set >= first_set));

	for (cur_seq = *seq_list; cur_seq;
	     prev_seq = cur_seq, cur_seq = cur_seq->next) {
		if (first_set > cur_seq->last_set)
			/* go for the next seq in the sorted seq list */
			continue;

		if (last_set < cur_seq->first_set)
			/* break here to insert the new seq to seq list at this point */
			break;

		/*
		 * There is an overlap of the new seq with the current seq.
		 * Adjust the first_set field of the current seq to consume
		 * the overlap.
		 */
		if (first_set < cur_seq->first_set)
			cur_seq->first_set = first_set;

		if (last_set <= cur_seq->last_set)
			/* The current seq now fully encompasses the first and last sets */
			return 0;

		/* Increment the first set so as to start from, where the current seq left */
		first_set = cur_seq->last_set + 1;
	}

	new_seq = kzalloc(sizeof(struct set_seq), GFP_NOWAIT);
	if (new_seq == NULL)
		return -ENOMEM;
	new_seq->first_set = first_set;
	new_seq->last_set = last_set;
	if (prev_seq) {
		new_seq->next = prev_seq->next;
		prev_seq->next = new_seq;
	} else {
		new_seq->next = *seq_list;
		*seq_list = new_seq;
		TX_CACHE_DEBUG("set: %d new_seq: %p new_seq->next: %p", first_set, new_seq, new_seq->next);
	}

	return 0;
}

#if CONFIG_SET_SHARING
/* tx_cache_split_set_span returns pointer of latter set seq */
struct set_seq *
tx_cache_split_set_span(struct set_seq *split_seq, unsigned split)
{
	struct set_seq *new_seq;

	new_seq = kmalloc(sizeof(struct set_seq), GFP_NOWAIT);

	if (!new_seq)
		return NULL;

	new_seq->last_set = split_seq->last_set;
	new_seq->first_set = split + 1;
	new_seq->next = split_seq->next;
	
	split_seq->last_set = split - 1;

	return new_seq;
}

static int 
tx_cache_expand_set_span(struct cache_c *dmc, struct bio_container *bc, index_t set)
{
	int error;		
	struct set_seq *cur_seq;

	if (!bc->bc_setspan) {
		cur_seq = &bc->bc_singlesspan;
		cur_seq->first_set = set;
		cur_seq->last_set = set;
		cur_seq->next = NULL;
		bc->bc_setspan = cur_seq;

		return 0;
	} 

	if (bc->bc_setspan == &bc->bc_singlesspan) {
		cur_seq = kzalloc(sizeof(struct set_seq), GFP_NOWAIT);
		*cur_seq = bc->bc_singlesspan;
		bc->bc_setspan = cur_seq;
	}

	error = insert_set_seq(&bc->bc_setspan, set, set);
	
	if (error) 
		TX_CACHE_ERROR("fail to allocate memory for set_seq");
	else
		TX_CACHE_DEBUG("expanding set span is completed. bc->bc_mdwait: %d bc->bc_setspan: %p %d", atomic_read(&bc->bc_mdwait), bc->bc_setspan, set);

	return error;
}

static int eio_acquire_set_locks(struct cache_c *dmc, struct bio_container *bc) 
{
	return 0;
}
#else	//CONFIG_SET_SHARING
/* Acquire read/shared lock for the sets covering the entire I/O range */
static int eio_acquire_set_locks(struct cache_c *dmc, struct bio_container *bc)
{
	struct bio *bio = bc->bc_bio;
	sector_t round_sector;
	sector_t end_sector;
	sector_t set_size;
	index_t cur_set;
	index_t first_set;
	index_t last_set;
#ifndef CONFIG_TX_CACHE
	index_t i;
#endif
	struct set_seq *cur_seq;
	struct set_seq *next_seq;
	int error;

	/*
	 * Find first set using start offset of the I/O and lock it.
	 * Find next sets by adding the set offsets to the previous set
	 * Identify all the sequences of set numbers that need locking.
	 * Keep the sequences in sorted list.
	 * For each set in each sequence
	 * - acquire read lock on the set.
	 */

#if (LINUX_VERSION_CODE >= KERNEL_VERSION(3,14,0))
	round_sector = EIO_ROUND_SET_SECTOR(dmc, bio->bi_iter.bi_sector);
	set_size = dmc->block_size * dmc->assoc;
	end_sector = bio->bi_iter.bi_sector + eio_to_sector(bio->bi_iter.bi_size);
#else 
	round_sector = EIO_ROUND_SET_SECTOR(dmc, bio->bi_sector);
	set_size = dmc->block_size * dmc->assoc;
	end_sector = bio->bi_sector + eio_to_sector(bio->bi_size);
#endif 
	first_set = -1;
	last_set = -1;
	cur_set = -1;
	bc->bc_setspan = NULL;

	while (round_sector < end_sector) {
		cur_set = hash_block(dmc, round_sector);
		
		if (first_set == -1) {
			first_set = cur_set;
			last_set = cur_set;
		} else if (cur_set == (last_set + 1))
			last_set = cur_set;
		else {
			/*
			 * Add the seq of start, end set to sorted (first, last) seq list
			 * and reinit the first and last set
			 */
			error =
				insert_set_seq(&bc->bc_setspan, first_set,
					       last_set);
			if (error)
				goto err_out;
			first_set = cur_set;
			last_set = cur_set;
		}

		round_sector += set_size;
	}

	/* Add the remaining first, last set sequence */

	EIO_ASSERT((first_set != -1) && (last_set == cur_set));

	if (bc->bc_setspan == NULL) {
		/* No sequence was added, can use singlespan */
		cur_seq = &bc->bc_singlesspan;
		cur_seq->first_set = first_set;
		cur_seq->last_set = last_set;
		cur_seq->next = NULL;
		bc->bc_setspan = cur_seq;
	} else {
		error = insert_set_seq(&bc->bc_setspan, first_set, last_set);
		if (error)
			goto err_out;
	}

	/* Acquire read locks on the sets in the set span */
#ifndef CONFIG_TX_CACHE
	for (cur_seq = bc->bc_setspan; cur_seq; cur_seq = cur_seq->next)
		for (i = cur_seq->first_set; i <= cur_seq->last_set; i++)
			down_read(&dmc->cache_sets[i].rw_lock);
#endif
	return 0;

err_out:
	/* Free the seqs in the seq list, unless it is just the local seq */
	if (bc->bc_setspan != &bc->bc_singlesspan) {
		for (cur_seq = bc->bc_setspan; cur_seq; cur_seq = next_seq) {
			next_seq = cur_seq->next;
			kfree(cur_seq);
		}
	}
	return error;
}
#endif //CONFIG_SET_SHARING

/*
static int tx_cache_alloc_mdreqs(struct cache_c *dmc, struct bio_container *bc)
{
	index_t i;
}
*/

/*
 * Allocate mdreq and md_blocks for each set.
 */
static int eio_alloc_mdreqs(struct cache_c *dmc, struct bio_container *bc)
{
	index_t i;
	struct mdupdate_request *mdreq;
	int nr_bvecs, ret;
	struct set_seq *cur_seq;

	bc->mdreqs = NULL;
	TX_CACHE_DEBUG("bc->bc_setspan: %p", bc->bc_setspan);

	for (cur_seq = bc->bc_setspan; cur_seq; cur_seq = cur_seq->next) {
		TX_CACHE_DEBUG("cur_seq: %p cur_seq->next: %p", cur_seq, cur_seq->next);
		for (i = cur_seq->first_set; i <= cur_seq->last_set; i++) {
			mdreq = kzalloc(sizeof(*mdreq), GFP_NOWAIT);
			if (mdreq) {
				mdreq->md_size =
					dmc->assoc *
					sizeof(struct flash_cacheblock);
				nr_bvecs =
					IO_BVEC_COUNT(mdreq->md_size,
						      SECTORS_PER_PAGE);
				mdreq->mdblk_bvecs =
					(struct bio_vec *)
					kmalloc(sizeof(struct bio_vec) * nr_bvecs,
						GFP_KERNEL);
					//kmalloc(sizeof(struct bio_vec) * nr_bvecs,
							//GFP_NOWAIT);
				if (mdreq->mdblk_bvecs) {
					ret =
						eio_alloc_wb_bvecs(mdreq->
								mdblk_bvecs,
								nr_bvecs,
								SECTORS_PER_PAGE);
					if (ret) {
						pr_err
							("eio_alloc_mdreqs: failed to allocated pages\n");
						kfree(mdreq->mdblk_bvecs);
						mdreq->mdblk_bvecs = NULL;
					}
					mdreq->mdbvec_count = nr_bvecs;
				}
			}

			if (unlikely
				    ((mdreq == NULL) || (mdreq->mdblk_bvecs == NULL))) {
				struct mdupdate_request *nmdreq;

				mdreq = bc->mdreqs;
				while (mdreq) {
					nmdreq = mdreq->next;
					if (mdreq->mdblk_bvecs) {
						eio_free_wb_bvecs(mdreq->
								  mdblk_bvecs,
								  mdreq->
								  mdbvec_count,
								  SECTORS_PER_PAGE);
						kfree(mdreq->mdblk_bvecs);
					}
					kfree(mdreq);
					mdreq = nmdreq;
				}
				bc->mdreqs = NULL;
				return -ENOMEM;
			} else {
				mdreq->next = bc->mdreqs;
				bc->mdreqs = mdreq;
			}
		}
	}

	return 0;
}

/*
 * Release:
 * 1. the set locks covering the entire I/O range
 * 2. any previously allocated memory for md update
 */
static int
eio_release_io_resources(struct cache_c *dmc, struct bio_container *bc)
{
#ifndef CONFIG_TX_CACHE
	index_t i;
#endif
	struct mdupdate_request *mdreq;
	struct mdupdate_request *nmdreq;
	struct set_seq *cur_seq;
	struct set_seq *next_seq;

#if CONFIG_SET_SHARING
/*
	for (cur_seq = bc->bc_setspan; cur_seq; cur_seq = cur_seq->next)
		for (i = cur_seq->first_set; i <= cur_seq->last_set; i++) {
			spin_lock(&dmc->cache_sets[i].cs_lock);
			tx_cache_attach_set(dmc, i);
			spin_unlock(&dmc->cache_sets[i].cs_lock);
		}
*/
#endif
	/* Release read locks on the sets in the set span */
#ifndef CONFIG_TX_CACHE
	for (cur_seq = bc->bc_setspan; cur_seq; cur_seq = cur_seq->next)
		for (i = cur_seq->first_set; i <= cur_seq->last_set; i++)
			up_read(&dmc->cache_sets[i].rw_lock);
#endif
	/* Free the seqs in the set span, unless it is single span */
	if (bc->bc_setspan != &bc->bc_singlesspan) {
		for (cur_seq = bc->bc_setspan; cur_seq; cur_seq = next_seq) {
			next_seq = cur_seq->next;
			kfree(cur_seq);
		}
	}

	mdreq = bc->mdreqs;
	while (mdreq) {
		nmdreq = mdreq->next;
		if (mdreq->mdblk_bvecs) {
			eio_free_wb_bvecs(mdreq->mdblk_bvecs,
					  mdreq->mdbvec_count,
					  SECTORS_PER_PAGE);
			kfree(mdreq->mdblk_bvecs);
		}
		kfree(mdreq);
		mdreq = nmdreq;
	}
	bc->mdreqs = NULL;

	return 0;
}

/*
 * Decide the mapping and perform necessary cache operations for a bio request.
 */
int eio_map(struct cache_c *dmc, struct request_queue *rq, struct bio *bio)
{
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(3,14,0))
	sector_t sectors = eio_to_sector(bio->bi_iter.bi_size);
#else 
	sector_t sectors = eio_to_sector(bio->bi_size);
#endif 
	struct eio_bio *ebio = NULL;
	struct bio_container *bc;
	sector_t snum;
	unsigned int iosize;
	unsigned int totalio;
	unsigned int biosize;
	unsigned int residual_biovec;
	unsigned int force_uncached = 0;
	unsigned long flag;
	int data_dir = bio_data_dir(bio);

	/*bio list*/
	struct eio_bio *ebegin = NULL;
	struct eio_bio *eend = NULL;
	struct eio_bio *enext = NULL;

#if (LINUX_VERSION_CODE >= KERNEL_VERSION(3,14,0))
	EIO_ASSERT(bio->bi_iter.bi_idx == 0);
#else 
	EIO_ASSERT(bio->bi_idx == 0);
#endif 

	pr_debug("this needs to be removed immediately\n");

	if (bio_rw_flagged(bio, REQ_DISCARD)) {
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(3,14,0))
		pr_debug
			("eio_map: Discard IO received. Invalidate incore start=%lu totalsectors=%d.\n",
			(unsigned long)bio->bi_iter.bi_sector,
			(int)eio_to_sector(bio->bi_iter.bi_size));
#else 
		pr_debug
			("eio_map: Discard IO received. Invalidate incore start=%lu totalsectors=%d.\n",
			(unsigned long)bio->bi_sector,
			(int)eio_to_sector(bio->bi_size));
#endif 
		bio_endio(bio, 0);
		pr_err
			("eio_map: I/O with Discard flag received. Discard flag is not supported.\n");
		return 0;
	}

	if (unlikely(dmc->cache_rdonly)) {
		if (data_dir != READ) {
			bio_endio(bio, -EPERM);
			pr_debug
				("eio_map: cache is read only, write not permitted\n");
			return 0;
		}
	}

	if (sectors < SIZE_HIST)
		atomic64_inc(&dmc->size_hist[sectors]);

	if (data_dir == READ) {
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(3,14,0))
		SECTOR_STATS(dmc->eio_stats.reads, bio->bi_iter.bi_size);
#else 
		SECTOR_STATS(dmc->eio_stats.reads, bio->bi_size);
#endif 
		atomic64_inc(&dmc->eio_stats.readcount);
	} else {
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(3,14,0))
		SECTOR_STATS(dmc->eio_stats.writes, bio->bi_iter.bi_size);
#else 
		SECTOR_STATS(dmc->eio_stats.writes, bio->bi_size);
#endif 
		atomic64_inc(&dmc->eio_stats.writecount);
	}

	/*
	 * Cache FAILED mode is like Hard failure.
	 * Dont allow I/Os to go through.
	 */
	if (unlikely(CACHE_FAILED_IS_SET(dmc))) {
		/*ASK confirm that once failed is set, it's never reset*/
		/* Source device is not available. */
		CTRACE
			("eio_map:2 source device is not present. Cache is in Failed state\n");
		bio_endio(bio, -ENODEV);
		bio = NULL;
		return DM_MAPIO_SUBMITTED;
	}

	/* WB cache will never be in degraded mode. */
	if (unlikely(CACHE_DEGRADED_IS_SET(dmc))) {
		EIO_ASSERT(dmc->mode != CACHE_MODE_WB);
		force_uncached = 1;
	} else if (data_dir == WRITE && dmc->mode == CACHE_MODE_RO) {
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(3,14,0))
		if (to_sector(bio->bi_iter.bi_size) != dmc->block_size)
#else
		if (to_sector(bio->bi_size) != dmc->block_size)
#endif
			atomic64_inc(&dmc->eio_stats.uncached_map_size);
		else
			atomic64_inc(&dmc->eio_stats.uncached_map_uncacheable);
		force_uncached = 1;
	}

	/*
	 * Process zero sized bios by passing original bio flags
	 * to both HDD and SSD.
	 */
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(3,14,0))
	if (bio->bi_iter.bi_size == 0) {
#else 
	if (bio->bi_size == 0) {
#endif 
		eio_process_zero_size_bio(dmc, bio);
		return DM_MAPIO_SUBMITTED;
	}

	/* Create a bio container */
	bc = kzalloc(sizeof(struct bio_container), GFP_NOWAIT);
	if (!bc) {
		bio_endio(bio, -ENOMEM);
		return DM_MAPIO_SUBMITTED;
	}
	bc->bc_iotime = jiffies;
	bc->bc_bio = bio;
	bc->bc_dmc = dmc;
	spin_lock_init(&bc->bc_lock);
	atomic_set(&bc->bc_holdcount, 1);
#ifdef CONFIG_TX_CACHE
	atomic_set(&bc->bc_mdwait, 0);
#endif
	bc->bc_error = 0;
	bc->bc_mdlist = NULL;
	bc->bc_setspan = NULL;
	
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(3,14,0))
	snum = bio->bi_iter.bi_sector;
	totalio = bio->bi_iter.bi_size;
	biosize = bio->bi_iter.bi_size;
#else 
	snum = bio->bi_sector;
	totalio = bio->bi_size;
	biosize = bio->bi_size;
#endif 
	residual_biovec = 0;

#ifdef CONFIG_TX_CACHE
	spin_lock_irqsave(&dmc->shadow_lock, flag);
	if (!bio->bi_tx_id) {
		bc->single_req_tx = 1;
		bc->tx = __alloc_new_tx(dmc, 0);	
		//list_add_tail(&bc->tx->tlist, &dmc->shadow_list);
		dmc->nr_single_req_tx++;
	} else {
		bc->tx = search_tx_by_id(&dmc->shadow_list, bio->bi_tx_id);
		
		if(!bc->tx) {
			bc->tx = __alloc_new_tx(dmc, bio->bi_tx_id);
			list_add_tail(&bc->tx->tlist, &dmc->shadow_list);
			dmc->nr_shadow_tx++;
		}
	}
	spin_unlock_irqrestore(&dmc->shadow_lock, flag);
	TX_CACHE_DEBUG("bc: %p tx: %ld nr_ios: %ld", bc, bc->tx->tx_id, atomic64_read(&dmc->nr_ios));
#endif

	if (dmc->mode == CACHE_MODE_WB) {
		/*
		 * For writeback, the app I/O and the clean I/Os
		 * need to be exclusive for a cache set. Acquire shared
		 * lock on the cache set for app I/Os and exclusive
		 * lock on the cache set for clean I/Os.
		 */
		int ret;
		
		ret = eio_acquire_set_locks(dmc, bc);
		if (ret) {
#ifdef CONFIG_TX_CACHE
			if (bc->single_req_tx) {
				bc->tx->tx_status = TX_ABORTED;
				dmc->nr_single_req_tx--;
				PREPARE_LAST_LOC(__LINE__);
				dealloc_tx(dmc, bc->tx);		
			}
#endif
			bio_endio(bio, ret);
			kfree(bc);
			return DM_MAPIO_SUBMITTED;
		}
	}

	atomic64_inc(&dmc->nr_ios);

	/*
	 * Prepare for I/O processing.
	 * - Allocate ebios.
	 * - For reads, identify if we need to do uncached read
	 * - If force uncached I/O is set, invalidate the cache blocks for the I/O
	 * - All ebios are separated by block-width (in usual case, it is 4KB) #foon
	 */

	if (force_uncached)
		eio_inval_range(dmc, snum, totalio);
	else {
		while (biosize) {
			iosize = eio_get_iosize(dmc, snum, biosize);
			ebio = eio_new_ebio(dmc, bio, &residual_biovec, snum,
					iosize, bc, EB_SUBORDINATE_IO);
			if (IS_ERR(ebio)) {
				bc->bc_error = -ENOMEM;
				break;
			}

			/* Anchor this ebio on ebio list. Preserve the order */
			if (ebegin)
				eend->eb_next = ebio;
			else
				ebegin = ebio;
			eend = ebio;

			biosize -= iosize;
			snum += eio_to_sector(iosize);
		}
	}

	if (bc->bc_error) {
		/* Error. Do ebio and bc cleanup. */
		ebio = ebegin;
		while (ebio) {
			enext = ebio->eb_next;
			eb_endio(ebio, bc->bc_error);
			ebio = enext;
		}

		/* By now, the bc_holdcount must be 1 */
		EIO_ASSERT(atomic_read(&bc->bc_holdcount) == 1);

		/* Goto out to cleanup the bc(in bc_put()) */
		goto out;
	}

	/*
	 * Start processing of the ebios.
	 *
	 * Note: don't return error from this point on.
	 *      Error handling would be done as part of
	 *      the processing of the ebios internally.
	 */
	if (force_uncached) {
		EIO_ASSERT(dmc->mode != CACHE_MODE_WB);
		if (data_dir == READ)
			atomic64_inc(&dmc->eio_stats.uncached_reads);
		else
			atomic64_inc(&dmc->eio_stats.uncached_writes);
		eio_disk_io(dmc, bio, ebegin, bc, 1);
	} else if (data_dir == READ) {

		/* read io processing */
		eio_read(dmc, bc, ebegin);
	} else
		/* write io processing */
		eio_write(dmc, bc, ebegin);

out:

	if (bc)
		bc_put(bc, 0);

	return DM_MAPIO_SUBMITTED;
}

/*
 * Checks the cache block state, for deciding cached/uncached read.
 * Also reserves/allocates the cache block, wherever necessary.
 *
 * Return values
 * 1: cache hit
 * 0: cache miss
 * 
 * Additionally, if you use txcache stuff, below value is available.
 * 2: transaction conflict
 */
static int eio_read_peek(struct cache_c *dmc, struct eio_bio *ebio)
{
	index_t index;
	int res;
	int retval = 0;
	unsigned long flags;
	u_int8_t cstate;
#ifdef CONFIG_TX_CACHE
	struct tx_unit *tx = NULL;
	struct tx_block *block = NULL;
	int tx_id = 0;
#endif
#if CONFIG_SET_SHARING
	unsigned old_set, rent_set, master_set;
	struct set_chain *set_chain;

	spin_lock_irqsave(&dmc->set_headers[ebio->eb_cacheset].header_lock, flags);
	spin_lock(&dmc->cache_sets[ebio->eb_cacheset].cs_lock);
	
	tx_cache_detach_set(dmc, ebio->eb_cacheset);
	old_set = rent_set = master_set = ebio->eb_cacheset;
	res = tx_cache_lookup(dmc, ebio, &index);
#else
	spin_lock_irqsave(&dmc->cache_sets[ebio->eb_cacheset].cs_lock, flags);
	res = eio_lookup(dmc, ebio, &index);
#endif

	ebio->eb_index = -1;
#if CONFIG_SET_SHARING
	/* in read case, txcache should find cached block which can be in rented set 
	   or assign a rented entry */
	if (res != VALID) {
		/* at first, we should find cached block that matched with dbn */
		TX_CACHE_DEBUG("start to search rental sets");
		
		if (dmc->set_headers[master_set].nr_rent > 0) {
			if(tx_cache_lookup_rental_set_span(dmc, ebio, &index, -1)) {
				/* we find matched cache block */
				PREPARE_LAST_LOC(__LINE__);
				tx_cache_attach_set(dmc, master_set);
				
				spin_lock(&dmc->cache_sets[ebio->eb_cacheset].cs_lock);
				rent_set = ebio->eb_cacheset;
				tx_cache_detach_set(dmc, rent_set);
			} else {
				if (res == INVALID)
					goto lookup_end;

				/* we cannot find cached block in the set. So allocate new free block */
				res = tx_cache_lookup_oldest_clean(dmc, ebio, &index);

				if (res == VALID)
					/* we find unused/free block */
					goto lookup_end;
				
				/* no room in the master set. find current rented set */
				atomic64_inc(&dmc->eio_stats.noroom);
				PREPARE_LAST_LOC(__LINE__);
				tx_cache_attach_set(dmc, master_set);
			
				if (dmc->set_headers[master_set].current_set != master_set) {
					rent_set = dmc->set_headers[master_set].current_set;
					spin_lock(&dmc->cache_sets[rent_set].cs_lock);
					tx_cache_detach_set(dmc, rent_set);
				} else {
					rent_set = tx_cache_rent_set(dmc);
					spin_lock(&dmc->cache_sets[rent_set].cs_lock);
					set_chain = alloc_set_chain();
					
					if (!set_chain) {
						spin_unlock(&dmc->cache_sets[rent_set].cs_lock);
						TX_CACHE_ERROR("Unable to allocate set_chain");
						ebio->eb_iotype |= EB_INVAL;
					}

					set_chain->master_set = master_set;
					set_chain->rental_set = rent_set;

					list_add_tail(&set_chain->chain, &dmc->set_headers[master_set].chain);
					dmc->set_headers[master_set].nr_rent++;
				}
				//spin_unlock_irqrestore(&dmc->set_headers[master_set].header_lock, flags);

				//spin_lock_irqsave(&dmc->cache_sets[rent_set].cs_lock, flags);
retry_lookup:
				ebio->eb_cacheset = rent_set;
				res = tx_cache_lookup_shadow(dmc, ebio, &index);
				
				if (res < 0) {
					/* no room in the rented set. rent additional set */
					PREPARE_LAST_LOC(__LINE__);
					tx_cache_attach_set(dmc, rent_set);
					spin_unlock(&dmc->cache_sets[rent_set].cs_lock);

					rent_set = tx_cache_rent_set(dmc);
					spin_lock(&dmc->cache_sets[rent_set].cs_lock);
					
					set_chain = alloc_set_chain();
					
					if (!set_chain) {
						/* error handling */
						TX_CACHE_ERROR("Unable to allocate set_chain");
						ebio->eb_iotype |= EB_INVAL;

						goto out;
					}
					TX_CACHE_DEBUG("no room in the rented set(%d), rent another set(%d)", 
						ebio->eb_cacheset, rent_set);

					dmc->set_headers[master_set].current_set = rent_set;
					set_chain->master_set = master_set;
					set_chain->rental_set = rent_set;
					
					list_add_tail(&set_chain->chain, &dmc->set_headers[master_set].chain);
					
					dmc->set_headers[master_set].nr_rent++;
					//spin_unlock_irqrestore(&dmc->set_headers[master_set].header_lock, flags);
					
					//spin_lock_irqsave(&dmc->cache_sets[rent_set].cs_lock, flags);
					dmc->cache_sets[rent_set].set_chain = set_chain;
					tx_cache_detach_set(dmc, rent_set);
					
					goto retry_lookup;
				}

				TX_CACHE_STATE_RENT(dmc, index);
				dmc->cache_sets[rent_set].nr_rented++;
			}
		} /* nr_rent > 0*/
		else {
			if (res == INVALID)
				goto lookup_end;

			res = tx_cache_lookup_oldest_clean(dmc, ebio, &index);
		
			if (res == VALID)
				/* we find unused/free block */
				goto lookup_end;

			/* no room in the set, let us rent additional set */
retry_rent_and_lookup:
			atomic64_inc(&dmc->eio_stats.noroom);
		
			PREPARE_LAST_LOC(__LINE__);
			tx_cache_attach_set(dmc, rent_set);
			
			/* loop */
			if (rent_set != master_set) 
				spin_unlock(&dmc->cache_sets[rent_set].cs_lock);
			
			rent_set = tx_cache_rent_set(dmc);
			spin_lock(&dmc->cache_sets[rent_set].cs_lock);

			set_chain = alloc_set_chain();

			if (!set_chain) {
				/* error handling */
				spin_unlock(&dmc->cache_sets[rent_set].cs_lock);
				TX_CACHE_ERROR("Unable to allocate set_chain");
				ebio->eb_iotype |= EB_INVAL;

				goto out;
			}
			
			dmc->set_headers[master_set].current_set = rent_set;
			set_chain->master_set = master_set;
			set_chain->rental_set = rent_set;

			list_add_tail(&set_chain->chain, &dmc->set_headers[master_set].chain);

			dmc->set_headers[master_set].nr_rent++;
			//spin_unlock_irqrestore(&dmc->set_headers[master_set].header_lock, flags);
			
			dmc->cache_sets[rent_set].set_chain = set_chain;

			ebio->eb_cacheset = rent_set;
			res = tx_cache_lookup_shadow(dmc, ebio, &index);

			if (res < 0)
				goto retry_rent_and_lookup;
			
			TX_CACHE_STATE_RENT(dmc, index);
			dmc->cache_sets[rent_set].nr_rented++;
		}
	}
#else
	if (res < 0) {
		atomic64_inc(&dmc->eio_stats.noroom);
		goto out;
	}
#endif
#if CONFIG_SET_SHARING
lookup_end:
#endif
	cstate = EIO_CACHE_STATE_GET(dmc, index);

	if (cstate & (BLOCK_IO_INPROG | QUEUED))
		/*
		 * We found a valid or invalid block but an io is on, so we can't
		 * proceed. Don't invalidate it. This implies that we'll
		 * have to read from disk.
		 * Read on a DIRTY | INPROG block (block which is going to be DIRTY)
		 * is also redirected to read from disk.
		 */
		goto out;

	if (res == VALID) {
		EIO_ASSERT(cstate & VALID);
		if ((EIO_DBN_GET(dmc, index) ==
		     EIO_ROUND_SECTOR(dmc, ebio->eb_sector))) {
			/*
			 * Read/write should be done on already DIRTY block
			 * without any inprog flag.
			 * Ensure that a failure of DIRTY block read is propagated to app.
			 * non-DIRTY valid blocks should have inprog flag.
			 */
#ifdef CONFIG_TX_CACHE
			if (!ebio->tx_id) {
				tx = ebio->eb_bc->tx;

				if (!tx)
					goto skip_tx;
			}
			else {
				if (ebio->eb_bc->tx != NULL)
					tx = ebio->eb_bc->tx;
				else {
					tx_id = ebio->tx_id;
					spin_lock(&dmc->shadow_lock);
					tx = search_tx_by_id(&dmc->shadow_list, tx_id);
					spin_unlock(&dmc->shadow_lock);
				}
			}
			spin_lock(&tx->tx_lock);	
			if (TX_CACHE_STATE_TEST(dmc, index, SHADOW)) {
				if (!test_index_in_tx(tx, index)) {
					block = search_tx_block_by_index(&dmc->shadow_list, index, NULL);
					
					//if (block->dep.old && EIO_DBN_GET(dmc, block->dep.old->index) == 
											//EIO_ROUND_SECTOR(dmc, ebio->eb_sector)) {	
					if (TX_CACHE_TEST_LINK(dmc, index) && 
						EIO_DBN_GET(dmc, TX_CACHE_GET_LINK_INDEX(dmc, index)) ==
						EIO_ROUND_SECTOR(dmc, ebio->eb_sector) &&
						TX_CACHE_STATE_TEST(dmc, TX_CACHE_GET_LINK_INDEX(dmc, index), PERSIST)) {
						/*
						 * OK, we found the persistent version of target copy.
						 */
						//index = block->dep.old->index;
						index = TX_CACHE_GET_LINK_INDEX(dmc, index);
						cstate = EIO_CACHE_STATE_GET(dmc, index);
						TX_CACHE_DEBUG("using persistent copy(%ld)", index);
					} else {
						/*
						 TODO:
							The block is not in tx and doesn't have old persistent copy.
							So, it should be handled by hdd-read.
							However, since concurrency control should be provided by upper-layer,
							(e.g. filesystem) this kind of handling is not supported.
						 */
						TX_CACHE_ERROR("block(%ld) dbn(%ld)", index, 
							EIO_DBN_GET(dmc, TX_CACHE_GET_LINK_INDEX(dmc, index)));
						TX_CACHE_ERROR("TX conflict occurred!");
						TX_CACHE_ERROR("tx(%ld) will be aborted.", tx->tx_id);	

						tx->tx_status = TX_IN_ABORT;
						spin_unlock(&tx->tx_lock);
						
						ebio->eb_iotype |= EB_INVAL;
						goto out;
					}
				}
			}
			spin_unlock(&tx->tx_lock);
skip_tx:
#endif
			if (cstate == ALREADY_DIRTY) {
				ebio->eb_iotype = EB_MAIN_IO;
				/*
				 * Set to uncached read and readfill for now.
				 * It may change to CACHED_READ later, if all
				 * the blocks are found to be cached
				 */
				ebio->eb_bc->bc_dir =
					UNCACHED_READ_AND_READFILL;
			} else
				EIO_CACHE_STATE_ON(dmc, index, CACHEREADINPROG);
			retval = 1;
			ebio->eb_index = index;
			goto out;
		}// EIO_DBN_GET(dmc, index) == EIO_ROUND_SECTOR(dmc, ebio->eb_sector)

		/* cache is marked readonly. Do not allow READFILL on SSD */
		if (unlikely(dmc->cache_rdonly))
			goto out;

		/*
		 * Found a block to be recycled.
		 * Its guranteed that it will be a non-DIRTY block
		 */

		EIO_ASSERT(!(cstate & DIRTY));
		if (eio_to_sector(ebio->eb_size) == dmc->block_size) {
			/*We can recycle and then READFILL only if iosize is block size*/
			atomic64_inc(&dmc->eio_stats.rd_replace);
			EIO_CACHE_STATE_SET(dmc, index, VALID | DISKREADINPROG);
			EIO_DBN_SET(dmc, index, (sector_t)ebio->eb_sector);
			ebio->eb_index = index;
			ebio->eb_bc->bc_dir = UNCACHED_READ_AND_READFILL;
		}
		goto out;
	}
	EIO_ASSERT(res == INVALID);

	/* cache is marked readonly. Do not allow READFILL on SSD */
	if (unlikely(dmc->cache_rdonly))
		goto out;
	/*
	 * Found an invalid block to be used.
	 * Can recycle only if iosize is block size
	 */
	if (eio_to_sector(ebio->eb_size) == dmc->block_size) {
		EIO_ASSERT(cstate & INVALID);
		EIO_CACHE_STATE_SET(dmc, index, VALID | DISKREADINPROG);
		atomic64_inc(&dmc->eio_stats.cached_blocks);
		EIO_DBN_SET(dmc, index, (sector_t)ebio->eb_sector);
		ebio->eb_index = index;
		ebio->eb_bc->bc_dir = UNCACHED_READ_AND_READFILL;
	}

out:

#if CONFIG_SET_SHARING
	
	if (master_set != rent_set) {
		tx_cache_attach_set(dmc, master_set);
		spin_unlock(&dmc->cache_sets[rent_set].cs_lock);
	}

	spin_unlock(&dmc->cache_sets[master_set].cs_lock);
	spin_unlock_irqrestore(&dmc->set_headers[master_set].header_lock, flags);
	/* expand set span of the current bio */	
	tx_cache_expand_set_span(dmc, ebio->eb_bc, ebio->eb_cacheset);
#else
	spin_unlock_irqrestore(&dmc->cache_sets[ebio->eb_cacheset].cs_lock,
			       flags);
#endif

	/*
	 * Enqueue clean set if there is no room in the set
	 * TBD
	 * Ensure, a force clean
	 */
#ifdef CONFIG_TX_CACHE
	spin_lock_irqsave(&dmc->pers_list_lock, flags);
	eio_comply_dirty_thresholds(dmc, ebio->eb_cacheset);
	spin_unlock_irqrestore(&dmc->pers_list_lock, flags);
	
	if (tx && tx->tx_status == TX_ABORTED) 
		return TX_CONFLICT;
#else
	if (res < 0)
		eio_comply_dirty_thresholds(dmc, ebio->eb_cacheset);
#endif
	return retval;
}


/*
 * Checks the cache block state, for deciding cached/uncached write.
 * Also reserves/allocates the cache block, wherever necessary.
 *
 * Return values
 * 1: cache block is available or newly allocated
 * 0: cache block could not be got for the ebio
 */
static int eio_write_peek(struct cache_c *dmc, struct eio_bio *ebio)
{
	index_t index;
	index_t old_index;
	int res;
	int retval = 0;
	u_int8_t cstate;
	unsigned long flags;
#ifdef CONFIG_TX_CACHE
	struct tx_block *new_block;
	struct tx_unit *tx = NULL;
	int rented = 0;
#endif
#if CONFIG_SET_SHARING
	struct set_chain *set_chain, *returned_chain;
	int rent_set, old_set, master_set;
	int returned = 0;
	int already_rented = 0;
	
	old_set = rent_set = master_set = ebio->eb_cacheset;
	
	spin_lock_irqsave(&dmc->set_headers[master_set].header_lock, flags);
	spin_lock(&dmc->cache_sets[master_set].cs_lock);
	
	tx_cache_detach_set(dmc, master_set);
	res = tx_cache_lookup(dmc, ebio, &index);
	TX_CACHE_DEBUG("index: %d sector: %d index->dbn: %d set: %u res: %d ebio->tx_id: %d", 
			index, EIO_ROUND_SECTOR(dmc, ebio->eb_sector), EIO_DBN_GET(dmc, index), 
			ebio->eb_cacheset, res, ebio->tx_id);	
#else
	spin_lock_irqsave(&dmc->cache_sets[ebio->eb_cacheset].cs_lock, flags);
	res = eio_lookup(dmc, ebio, &index);
#endif
	ebio->eb_index = -1;
	
#if CONFIG_SET_SHARING
	if (res != VALID) {
		/* at first, we try to find cached block that matched with dbn */
		TX_CACHE_DEBUG("start to search rental sets");
		
		if (dmc->set_headers[master_set].nr_rent > 0) {
			spin_unlock(&dmc->cache_sets[master_set].cs_lock);
			/* set_header has rental set. so we find matched block in rental set */
			if(tx_cache_lookup_rental_set_span(dmc, ebio, &index, -1)) {
				/* we find matched cache block */
				rent_set = ebio->eb_cacheset;
				tx_cache_detach_set(dmc, ebio->eb_cacheset);
			} else {
				/* we cannot find matched block in the set. So allocate new free block */
				spin_lock(&dmc->cache_sets[master_set].cs_lock);
				res = tx_cache_lookup(dmc, ebio, &index);

				if (res == INVALID)
					goto lookup_end;

				res = tx_cache_lookup_oldest_clean(dmc, ebio, &index);

				if (res == VALID)
					/* we find unused/free block */
					goto lookup_end;
				
				/* no room in the master set. find current rented set */
				atomic64_inc(&dmc->eio_stats.noroom);
				spin_unlock(&dmc->cache_sets[master_set].cs_lock);

				if (dmc->set_headers[master_set].current_set == rent_set) {
					rent_set = tx_cache_rent_set(dmc);
					TX_CACHE_DEBUG("set(%d) has no free block(%ld), let's rent another set(%d)",
							ebio->eb_cacheset, dmc->cache_sets[ebio->eb_cacheset].nr_dirty, rent_set);	
					spin_lock(&dmc->cache_sets[rent_set].cs_lock);
					set_chain = alloc_set_chain();
					
					if (!set_chain) {
						/* error handling */
						//spin_unlock(&dmc->cache_sets[rent_set].cs_lock);
						ebio->eb_cacheset = rent_set;
						TX_CACHE_ERROR("Unable to allocate set_chain");
						ebio->eb_iotype |= EB_INVAL;

						goto out;
					}

					dmc->set_headers[master_set].current_set = rent_set;
					set_chain->master_set = master_set;
					set_chain->rental_set = rent_set;
					
					list_add_tail(&set_chain->chain, &dmc->set_headers[master_set].chain);
					dmc->set_headers[master_set].nr_rent++;
					
					dmc->cache_sets[rent_set].set_chain = set_chain;
				} else {
					rent_set = dmc->set_headers[master_set].current_set;
					spin_lock(&dmc->cache_sets[rent_set].cs_lock);
					tx_cache_detach_set(dmc, rent_set);
				}
retry_lookup:
				ebio->eb_cacheset = rent_set;
				res = tx_cache_lookup_shadow(dmc, ebio, &index);
				
				if (res < 0) {
					/* no room in the rented set. rent additional set */
					if (rent_set != master_set) {
						PREPARE_LAST_LOC(__LINE__);
						tx_cache_attach_set(dmc, rent_set);
					}
					spin_unlock(&dmc->cache_sets[rent_set].cs_lock);

					rent_set = tx_cache_rent_set(dmc);
					spin_lock(&dmc->cache_sets[rent_set].cs_lock);
					
					set_chain = alloc_set_chain();
					
					if (!set_chain) {
						/* error handling */
						//spin_unlock(&dmc->cache_sets[rent_set].cs_lock);
						ebio->eb_cacheset = rent_set;
						TX_CACHE_ERROR("Unable to allocate set_chain");
						ebio->eb_iotype |= EB_INVAL;

						goto out;
					}

					dmc->set_headers[master_set].current_set = rent_set;
					set_chain->master_set = master_set;
					set_chain->rental_set = rent_set;
					list_add_tail(&set_chain->chain, &dmc->set_headers[master_set].chain);
					dmc->set_headers[master_set].nr_rent++;
					dmc->cache_sets[rent_set].set_chain = set_chain;
					
					goto retry_lookup;
				}

				TX_CACHE_STATE_RENT(dmc, index);
				dmc->cache_sets[rent_set].nr_rented++;
			}
		} //dmc->set_headers[master_set].nr_rent > 0
		else {
			//spin_unlock(&dmc->set_headers[master_set].header_lock);
			if (res == INVALID)
				goto lookup_end;

			res = tx_cache_lookup_oldest_clean(dmc, ebio, &index);
		
			if (res == VALID)
				/* we find unused/free block in master set */
				goto lookup_end;

			/* no room in the set, let us rent additional set */
retry_rent_and_lookup:
			atomic64_inc(&dmc->eio_stats.noroom);
			spin_unlock(&dmc->cache_sets[master_set].cs_lock);
			
			rent_set = tx_cache_rent_set(dmc);
			
			spin_lock(&dmc->cache_sets[rent_set].cs_lock);

			TX_CACHE_DEBUG("set(%d) has no free block(%u), let's rent another set(%d)",
					ebio->eb_cacheset, dmc->cache_sets[ebio->eb_cacheset].nr_dirty, rent_set);	
			
			set_chain = alloc_set_chain();
			
			if (!set_chain) {
				/* error handling */
				//spin_unlock(&dmc->cache_sets[rent_set].cs_lock);
				ebio->eb_cacheset = rent_set;
				TX_CACHE_ERROR("Unable to allocate set_chain");
				ebio->eb_iotype |= EB_INVAL;

				goto out;
			}
			
			dmc->set_headers[master_set].current_set = rent_set;
			set_chain->rental_set = rent_set;
			set_chain->master_set = master_set;

			list_add_tail(&set_chain->chain, &dmc->set_headers[master_set].chain);

			dmc->set_headers[master_set].nr_rent++;
			dmc->cache_sets[rent_set].set_chain = set_chain;

			ebio->eb_cacheset = rent_set;
			res = tx_cache_lookup_shadow(dmc, ebio, &index);

			if (res < 0)
				goto retry_rent_and_lookup;

			TX_CACHE_STATE_RENT(dmc, index);
			dmc->cache_sets[rent_set].nr_rented++;
		}
	} //res != valid
lookup_end:
#else
	if (res < 0) {
		/* cache block not found and new block couldn't be allocated */
		atomic64_inc(&dmc->eio_stats.noroom);
		ebio->eb_iotype |= EB_INVAL;
		goto out;
	}
#endif	//CONFIG_SET_SHARING
	cstate = EIO_CACHE_STATE_GET(dmc, index);

	if (cstate & (BLOCK_IO_INPROG | QUEUED)) {
#ifdef CONFIG_TX_CACHE
		/* it can be in cleaning process */
		if (!TX_CACHE_STATE_TEST(dmc, index, PERSIST)) 
			TX_CACHE_DEBUG("block(%d) in INPROG or QUEUED: %x", index, cstate);
#else
		ebio->eb_iotype |= EB_INVAL;
		/* treat as if cache block is not available */
		goto out;
#endif
	}

#ifdef CONFIG_TX_CACHE
	if (!ebio->tx_id) {
		tx = ebio->eb_bc->tx;

		if (tx)
			goto single_req_tx;
		else
			goto skip_tx;
	}

	spin_lock(&dmc->shadow_lock);
	if (ebio->eb_bc->tx)
		tx = ebio->eb_bc->tx;
	else
		tx = search_tx_by_id(&dmc->shadow_list, ebio->tx_id);

	if (!tx) {
		spin_unlock(&dmc->shadow_lock);
		TX_CACHE_DEBUG ("tx is null");
		goto skip_tx;
	}

	if (tx->tx_status != TX_IN_PROGRESS && tx->tx_status != TX_STARTED) {
		spin_unlock(&dmc->shadow_lock);
		TX_CACHE_ERROR("TX(%ld) is not in progress", tx->tx_id);
		ebio->eb_iotype |= EB_INVAL;
		goto out;
	}
	spin_unlock(&dmc->shadow_lock);
	already_rented = 0;

single_req_tx:
	if ((res == VALID) && (EIO_DBN_GET(dmc, index) ==
			       EIO_ROUND_SECTOR(dmc, ebio->eb_sector))) {
		/* block in shadow version */
		if (TX_CACHE_STATE_TEST(dmc, index, SHADOW)) {
			struct tx_block *block;
			int	block_in_tx = 0;
			sector_t dbn = 0, comp = 0;
			
			comp = EIO_DBN_GET(dmc, index);
			BUG_ON(tx == NULL);
			TX_CACHE_DEBUG ("TX(%ld) may conflict", tx->tx_id);	

			spin_lock(&tx->tx_lock);
			/* check blocks in transaction*/
			list_for_each_entry (block, &tx->blist, blist) {
				if (index == block->index) {
					dbn = EIO_DBN_GET(dmc, block->index);

					/*
					 * this block req is in same transaction of previous req.
					 * so, the req can modify this cache block.
					 */
					TX_CACHE_DEBUG("index: %d COMP: %ld, DBN: %ld", index, comp, dbn);
					if (dbn == comp) {
						block_in_tx = 1;
						break;
					}
				}
			}
			spin_unlock(&tx->tx_lock);

			if (!block_in_tx) {
				TX_CACHE_ERROR("TX(%ld) conflict is occurred (%d)", ebio->tx_id, index);	
				/*	
				if (tx->tx_id)
					spin_unlock(&dmc->shadow_lock);				
				*/	
				tx->tx_status = TX_IN_ABORT;
				ebio->eb_iotype |= EB_INVAL;
				
				goto out;
			}
			
			EIO_CACHE_STATE_ON(dmc, index, CACHEWRITEINPROG);
			retval = 1;
		}
		/* block in persistent version */
		else if (TX_CACHE_STATE_TEST(dmc, index, PERSIST)) {
			index_t shadow;
			
			/* make a new copy */
			BUG_ON(tx == NULL);
			TX_CACHE_DEBUG("TX(%ld) modify persistent cache block(%d)", tx->tx_id, index);
			
			if (TX_CACHE_TEST_LINK(dmc, index)) {
				/* this block already have shadow copy */
				unsigned holder_set = 0;
				
				shadow = TX_CACHE_GET_LINK_INDEX(dmc, index);

				spin_lock(&tx->tx_lock);
				if (test_index_in_tx(tx, shadow)) {
					spin_unlock(&tx->tx_lock);
					old_index = index;
					index = shadow;
					holder_set = index / dmc->assoc;
				} else {
					/* conflict occurred! */	
					spin_unlock(&tx->tx_lock);
					TX_CACHE_ERROR("TX conflict occurred! Let's abort tx(%ld) - index(%d) shadow(%d)", tx->tx_id, index, shadow);
					tx->tx_status = TX_IN_ABORT;
					ebio->eb_iotype |= EB_INVAL;
					
					goto out;
				}

				if (rent_set != holder_set) {
					/* if shadow copy is in another set */
					if (master_set != rent_set) {
						PREPARE_LAST_LOC(__LINE__);
						tx_cache_attach_set(dmc, rent_set);
					}
					spin_unlock(&dmc->cache_sets[rent_set].cs_lock);
					
					spin_lock(&dmc->cache_sets[holder_set].cs_lock);
					tx_cache_detach_set(dmc, holder_set);
					rent_set = ebio->eb_cacheset = holder_set;
				}

				EIO_CACHE_STATE_ON(dmc, index, CACHEWRITEINPROG);
				cstate = EIO_CACHE_STATE_GET(dmc, index);
				ebio->eb_index = index;
				retval = 1;

				goto out;
			}
			
			/* tx_block doesn't have shadow copy */	
			old_index = index;
			old_set = rent_set;	// == ebio->eb_cacheset
			res = tx_cache_lookup_shadow(dmc, ebio, &index);
		
			if (res < 0) {
#if CONFIG_SET_SHARING
				/* 
				 * In this case, other rental sets are searched at above lookup pass.
				 * So, we do not need to re-search rental sets.
				 */
				TX_CACHE_DEBUG("no room in the set(%lu) - master_set(%d)", ebio->eb_cacheset, master_set);
				//spin_unlock(&dmc->cache_sets[rent_set].cs_lock);
				
				/* no room in the master set. find current rented set */
				atomic64_inc(&dmc->eio_stats.noroom);

				if (master_set != rent_set) {
					PREPARE_LAST_LOC(__LINE__);
					tx_cache_attach_set(dmc, rent_set);
					spin_unlock(&dmc->cache_sets[rent_set].cs_lock);
					
					if (dmc->set_headers[master_set].current_set == master_set) {
						/* since persistent tx_block is in rented set and
						 * (current set == master set) which means master set is cleared,
						 * there can be free blocks in master set */
						rent_set = master_set;
						//goto retry_lookup_persist;
					} else if (dmc->set_headers[master_set].current_set == rent_set) {
						rent_set = tx_cache_rent_set(dmc);
						dmc->set_headers[master_set].current_set = rent_set;
						TX_CACHE_DEBUG("set(%d) has no free block(%u), let's rent another set(%d)",
								ebio->eb_cacheset, dmc->cache_sets[ebio->eb_cacheset].nr_dirty, rent_set);	
						rented = 1;
					} else {
						rent_set = dmc->set_headers[master_set].current_set;
						already_rented = 1;
					}
				} else {
					spin_unlock(&dmc->cache_sets[rent_set].cs_lock);
					
					if (dmc->set_headers[master_set].current_set == rent_set) {
						/* current_set == master_set == rent_set */
						rent_set = tx_cache_rent_set(dmc);
						TX_CACHE_DEBUG("set(%d) has no free block(%u), let's rent another set(%d)",
								ebio->eb_cacheset, dmc->cache_sets[ebio->eb_cacheset].nr_dirty, rent_set);	
						dmc->set_headers[master_set].current_set = rent_set;
						rented = 1;
					} else {
						rent_set = dmc->set_headers[master_set].current_set;
						already_rented = 1;
					}
				}

				spin_lock(&dmc->cache_sets[rent_set].cs_lock);
				
				if (rented) {
					set_chain = alloc_set_chain();

					if (!set_chain) {
						/* error handling */
						//spin_unlock(&dmc->cache_sets[rent_set].cs_lock);
						ebio->eb_cacheset = rent_set;
						TX_CACHE_ERROR("Unable to allocate set_chain");
						ebio->eb_iotype |= EB_INVAL;

						goto out;
					}

					set_chain->master_set = master_set;
					set_chain->rental_set = rent_set;
					list_add_tail(&set_chain->chain, &dmc->set_headers[master_set].chain);

					dmc->set_headers[master_set].nr_rent++;
					dmc->cache_sets[rent_set].set_chain = set_chain;	
					rented = 0;
				}
				
				if (already_rented) {
					tx_cache_detach_set(dmc, rent_set);
					already_rented = 0;
				}

retry_lookup_persist:
				ebio->eb_cacheset = rent_set;
				res = tx_cache_lookup_shadow(dmc, ebio, &index);

				if (res < 0) {
					/* no room in the rented set. rent additional set */
					TX_CACHE_DEBUG("no room in the rented set. let's rent additional set");
				
					/* in this point, rent_set != old_set */
					if (master_set != rent_set) {
						PREPARE_LAST_LOC(__LINE__);
						tx_cache_attach_set(dmc, rent_set);
					}
					spin_unlock(&dmc->cache_sets[rent_set].cs_lock);

					//spin_lock_irqsave(&dmc->set_headers[master_set].header_lock, flags);
					rent_set = tx_cache_rent_set(dmc);
					TX_CACHE_DEBUG("set(%d) has no free block(%u), let's rent another set(%d)",
							ebio->eb_cacheset, dmc->cache_sets[ebio->eb_cacheset].nr_dirty, rent_set);	
					spin_lock(&dmc->cache_sets[rent_set].cs_lock);
					dmc->set_headers[master_set].current_set = rent_set;

					set_chain = alloc_set_chain();

					if (!set_chain) {
						/* error handling */
						//spin_unlock(&dmc->cache_sets[rent_set].cs_lock);
						ebio->eb_cacheset = rent_set;
						TX_CACHE_ERROR("Unable to allocate set_chain");
						ebio->eb_iotype |= EB_INVAL;
						
						goto out;
					}

					set_chain->master_set = master_set;
					set_chain->rental_set = rent_set;
					list_add_tail(&set_chain->chain, &dmc->set_headers[master_set].chain);

					dmc->set_headers[master_set].nr_rent++;
					dmc->cache_sets[rent_set].set_chain = set_chain;	
					
					goto retry_lookup_persist;
				}

				TX_CACHE_STATE_RENT(dmc, index);
				dmc->cache_sets[rent_set].nr_rented++;
				atomic64_inc(&dmc->eio_stats.noroom);
#else
				atomic64_inc(&dmc->eio_stats.noroom);
				TX_CACHE_ERROR("can not find cache block for shadow copy");
				
				ebio->eb_iotype |= EB_INVAL;
				spin_lock(&tx->tx_lock);
				tx->tx_status = TX_IN_ABORT;
				spin_unlock(&tx->tx_lock);
				
				goto out;
#endif		//CONFIG_SET_SHARING
			}	//res < 0
			
			new_block = alloc_tx_block();
			atomic_inc(&dmc->nr_tx_block);
			new_block->index = index;
			
			tx_cache_set_undo(dmc, index, &new_block->undo);
			
			EIO_CACHE_STATE_SET(dmc, index, VALID | CACHEWRITEINPROG);
			EIO_DBN_SET(dmc, index, EIO_ROUND_SECTOR(dmc, ebio->eb_sector));
			
			atomic64_inc(&dmc->eio_stats.cached_blocks);
	
			/* we already hold spinlock of old_set*/
			TX_CACHE_SET_LINK_INDEX(dmc, old_index, index);	
			TX_CACHE_SET_LINK_INDEX(dmc, index, old_index);
			
			TX_CACHE_DEBUG("index(%d:%d)-old_index(%d:%d)", index, TX_CACHE_TEST_LINK(dmc, index), 
				old_index, TX_CACHE_TEST_LINK(dmc, old_index));
		
			if (master_set != old_set) {
				PREPARE_LAST_LOC(__LINE__);
				tx_cache_attach_set(dmc, old_set);
			}	

			spin_lock(&tx->tx_lock);	
			add_block_to_tx(dmc, tx, new_block, index);	
			tx->nr_invalidate++;
			spin_unlock(&tx->tx_lock);
			
			retval = 1;
		} // PERSIST
		/* block matched with valid and written-back copy or in-cleaning */	
		else {
			TX_CACHE_DEBUG("Block matched written-back copy (%u)", ebio->eb_cacheset);
#if CONFIG_SET_SHARING
			if (TX_CACHE_STATE_RENTED(dmc, index)) {
				int __master_set, __rental_set;

				if (dmc->cache_sets[rent_set].set_chain) {
					__master_set = dmc->cache_sets[rent_set].set_chain->master_set;
					__rental_set = dmc->cache_sets[rent_set].set_chain->rental_set;
				} else {
					TX_CACHE_DEBUG3("master: %d master->current: %d rent: %d index: %d", 
						master_set, dmc->set_headers[master_set].current_set, rent_set, index);
					TX_CACHE_DEBUG3("cache block(%d) is already returned", index);
					TX_CACHE_STATE_RETURN(dmc, index);

					goto already_returned;
				}
				
				//EIO_ASSERT(master_set == __master_set);
				//EIO_ASSERT(rent_set == __rental_set);
				//if (dmc->set_headers[ebio->eb_cacheset].current_set != ebio->eb_cacheset) {
				if (master_set == __master_set) {
					/* 
					 * Matched block is the rented block.
					 * In this case, we prefer to return the rented entry 
					 * rather than reuse it. 
					 * Then, we allocate new copy into current set. 
					 */
					TX_CACHE_DEBUG("Block matched rented written-back copy from rental set(%lu)", ebio->eb_cacheset);
					eio_invalidate_md(dmc, index);	

					ebio->eb_cacheset = dmc->set_headers[master_set].current_set;
					dmc->cache_sets[rent_set].nr_rented--;

					if (ebio->eb_cacheset != rent_set) {
						if (dmc->cache_sets[rent_set].nr_rented == 0) {
							set_chain = dmc->cache_sets[rent_set].set_chain;	

							dmc->cache_sets[rent_set].set_chain = NULL;
							list_del(&set_chain->chain);

							dealloc_set_chain(set_chain);
							dmc->set_headers[master_set].nr_rent--;
						}

						PREPARE_LAST_LOC(__LINE__);
						tx_cache_attach_set(dmc, rent_set);
						spin_unlock(&dmc->cache_sets[rent_set].cs_lock);

						spin_lock(&dmc->cache_sets[ebio->eb_cacheset].cs_lock);
						tx_cache_detach_set(dmc, ebio->eb_cacheset);
						TX_CACHE_DEBUG("Try to allocate new shadow copy into current set(%lu) - old_set(%lu)", 
								ebio->eb_cacheset, rent_set);
						rent_set = ebio->eb_cacheset;
					}
					
					res = tx_cache_lookup_shadow(dmc, ebio, &index);

					if (res < 0) {
						spin_unlock(&dmc->cache_sets[ebio->eb_cacheset].cs_lock);
						atomic64_inc(&dmc->eio_stats.noroom);
						ebio->eb_iotype |= EB_INVAL;
						
						spin_lock(&tx->tx_lock);
						tx->tx_status = TX_IN_ABORT;
						spin_unlock(&tx->tx_lock);
						
						goto out;
					}
				} else {
					/* try to pick the entry by set owner */
					TX_CACHE_STATE_RETURN(dmc, index);
					dmc->cache_sets[rent_set].nr_rented--;

					TX_CACHE_DEBUG3("this path must not be reached");

					if (dmc->cache_sets[rent_set].nr_rented == 0) {
						returned_chain = dmc->cache_sets[rent_set].set_chain;	
						dmc->cache_sets[rent_set].set_chain = NULL;
						//list_del(&set_chain->chain);
						returned = 1;
						//dealloc_set_chain(set_chain);
					}
					PREPARE_LAST_LOC(__LINE__);
					tx_cache_attach_set(dmc, rent_set);
					spin_unlock(&dmc->cache_sets[rent_set].cs_lock);
					
					spin_lock(&dmc->cache_sets[master_set].cs_lock);
					rent_set = master_set;
					ebio->eb_cacheset = master_set;
					tx_cache_detach_set(dmc, rent_set);

					res = tx_cache_lookup_shadow(dmc, ebio, &index);
					
					if (res < 0) {
						spin_unlock(&dmc->cache_sets[rent_set].cs_lock);
						TX_CACHE_ERROR("cannot find free block from master set(%d)", master_set);
						atomic64_inc(&dmc->eio_stats.noroom);
						ebio->eb_iotype |= EB_INVAL;
						
						spin_lock(&tx->tx_lock);
						tx->tx_status = TX_IN_ABORT;
						spin_unlock(&tx->tx_lock);
						
						goto out;
					}
				}
			}//RENTED
already_returned:
#endif		//CONFIG_SET_SHARING
			if (EIO_CACHE_STATE_GET(dmc, index) == CLEAN_INPROG) {
				TX_CACHE_DEBUG("index(%d) is in cleaning process, search another free block", index);

retry_find_for_cleaning:
				res = tx_cache_lookup_shadow(dmc, ebio, &index);

				if (res < 0) {
					rented = 0;
					if (master_set != rent_set) {
						PREPARE_LAST_LOC(__LINE__);
						tx_cache_attach_set(dmc, rent_set);
					}
					spin_unlock(&dmc->cache_sets[rent_set].cs_lock);	
					
					if (dmc->set_headers[rent_set].current_set != rent_set) {
						rent_set = dmc->set_headers[rent_set].current_set;
						spin_lock(&dmc->cache_sets[rent_set].cs_lock);
						tx_cache_detach_set(dmc, rent_set);
						ebio->eb_cacheset = rent_set;
						rented = 1;
						
						goto retry_find_for_cleaning;
					} else {
						rent_set = tx_cache_rent_set(dmc);
						spin_lock(&dmc->cache_sets[rent_set].cs_lock);
					}
					TX_CACHE_DEBUG("set(%d) has no free block(%d), let's rent another set(%d)",
							ebio->eb_cacheset, dmc->cache_sets[ebio->eb_cacheset].nr_persist, rent_set);	
					atomic64_inc(&dmc->eio_stats.noroom);
					
					dmc->set_headers[master_set].current_set = rent_set;

					set_chain = alloc_set_chain();

					if (!set_chain) {
						/* error handling */
						//spin_unlock(&dmc->cache_sets[rent_set].cs_lock);
						ebio->eb_cacheset = rent_set;
						TX_CACHE_ERROR("Unable to allocate set_chain");
						ebio->eb_iotype |= EB_INVAL;

						goto out;
					}

					set_chain->master_set = master_set;
					set_chain->rental_set = rent_set;
					list_add_tail(&set_chain->chain, &dmc->set_headers[master_set].chain);

					dmc->set_headers[master_set].nr_rent++;
					dmc->cache_sets[rent_set].set_chain = set_chain;	
					ebio->eb_cacheset = rent_set;
					rented = 1;
					
					goto retry_find_for_cleaning;
				}
				
				if (rented) 
					TX_CACHE_STATE_RENT(dmc, index);
			}

			new_block = alloc_tx_block();
			new_block->index = index;
			tx_cache_set_undo(dmc, index, &new_block->undo);
			
			atomic_inc(&dmc->nr_tx_block);
			
			EIO_CACHE_STATE_SET(dmc, index, VALID | CACHEWRITEINPROG);
			EIO_DBN_SET(dmc, index, EIO_ROUND_SECTOR(dmc, ebio->eb_sector));
			
			spin_lock(&tx->tx_lock);
			add_block_to_tx(dmc, tx, new_block, index);	
			spin_unlock(&tx->tx_lock);	
#if CONFIG_SET_SHARING
			//if (dmc->set_headers[ebio->eb_cacheset].current_set != ebio->eb_cacheset) 
			if (master_set != rent_set)
				dmc->cache_sets[rent_set].nr_rented++;
#endif
			retval = 1;
		}
	} else {	/* case of INVALID */
		TX_CACHE_DEBUG("case of INVALID");
		EIO_ASSERT(tx != NULL);
		
		new_block = alloc_tx_block();
		atomic_inc(&dmc->nr_tx_block);
		new_block->index = index;
		
		tx_cache_set_undo(dmc, index, &new_block->undo);
		
		EIO_CACHE_STATE_SET(dmc, index, VALID | CACHEWRITEINPROG);
		EIO_DBN_SET(dmc, index, EIO_ROUND_SECTOR(dmc, ebio->eb_sector));
		
		spin_lock(&tx->tx_lock);
		add_block_to_tx(dmc, tx, new_block, index);	
		spin_unlock(&tx->tx_lock);
		
		atomic64_inc(&dmc->eio_stats.cached_blocks);
#if CONFIG_SET_SHARING
		if (master_set != rent_set) 
			dmc->cache_sets[rent_set].nr_rented++;
#endif
		retval = 1;
	}
	spin_lock(&tx->tx_lock);
	if (tx->tx_status == TX_STARTED)
		tx->tx_status = TX_IN_PROGRESS;
	spin_unlock(&tx->tx_lock);
	
	cstate = EIO_CACHE_STATE_GET(dmc, index);	
	ebio->eb_index = index;
	goto out;
skip_tx:
#endif	//CONFIG_TX_CACHE

	if ((res == VALID) && (EIO_DBN_GET(dmc, index) ==
			       EIO_ROUND_SECTOR(dmc, ebio->eb_sector))) {
		/*
		 * Cache hit.
		 * All except an already DIRTY block should have an INPROG flag.
		 * If it is a cached write, a DIRTY flag would be added later.
		 */
		SECTOR_STATS(dmc->eio_stats.write_hits, ebio->eb_size);
		if (cstate != ALREADY_DIRTY)
			EIO_CACHE_STATE_ON(dmc, index, CACHEWRITEINPROG);
		else
			atomic64_inc(&dmc->eio_stats.dirty_write_hits);
		ebio->eb_index = index;
		/*
		 * A VALID block should get upgraded to DIRTY, only when we
		 * are updating the entire cache block(not partially).
		 * Otherwise, 2 sequential partial writes can lead to missing
		 * data when one write upgrades the cache block to DIRTY, while
		 * the other just writes to HDD. Subsequent read would be
		 * served from the cache block, which won't have the data from
		 * 2nd write.
		 */
		if ((cstate == ALREADY_DIRTY) ||
		    (eio_to_sector(ebio->eb_size) == dmc->block_size))
			retval = 1;
		else
			retval = 0;
		goto out;
	}

	/*
	 * cache miss with a new block allocated for recycle.
	 * Set INPROG flag, if the ebio size is equal to cache block size
	 */
	EIO_ASSERT(!(EIO_CACHE_STATE_GET(dmc, index) & DIRTY));
	if (eio_to_sector(ebio->eb_size) == dmc->block_size) {
		if (res == VALID)
			atomic64_inc(&dmc->eio_stats.wr_replace);
		else
			atomic64_inc(&dmc->eio_stats.cached_blocks);
		EIO_CACHE_STATE_SET(dmc, index, VALID | CACHEWRITEINPROG);
		EIO_DBN_SET(dmc, index, (sector_t)ebio->eb_sector);
		ebio->eb_index = index;
		retval = 1;
	} else {
		/*
		 * eb iosize smaller than cache block size shouldn't
		 * do cache write on a cache miss
		 */
		retval = 0;
		ebio->eb_iotype |= EB_INVAL;
		TX_CACHE_DEBUG("cachemiss - uncached write");
	}

out:
	if ((retval == 1) && (dmc->mode == CACHE_MODE_WB) &&
	    (cstate != ALREADY_DIRTY)) {
#ifdef CONFIG_TX_CACHE
		atomic_inc(&ebio->eb_bc->bc_mdwait);
		TX_CACHE_DEBUG("bc: %p bc_mdwait: %d", ebio->eb_bc, atomic_read(&ebio->eb_bc->bc_mdwait));
#else
		ebio->eb_bc->bc_mdwait++;
#endif
	} 

#if CONFIG_SET_SHARING
	/* block is stored into rent set, so we clean up the master set */
	
	if (master_set != rent_set) {
		PREPARE_LAST_LOC(__LINE__);
		tx_cache_attach_set(dmc, master_set);
	}

	if (old_set != master_set && old_set != rent_set) 
		spin_unlock(&dmc->cache_sets[old_set].cs_lock);
	
	spin_unlock(&dmc->cache_sets[ebio->eb_cacheset].cs_lock);
	spin_unlock_irqrestore(&dmc->set_headers[master_set].header_lock, flags);

	if (returned) {
		int header;
		header = returned_chain->master_set;
		spin_lock_irqsave(&dmc->set_headers[header].header_lock, flags);
		list_del(&returned_chain->chain);
		dealloc_set_chain(returned_chain);
		spin_unlock_irqrestore(&dmc->set_headers[header].header_lock, flags);
	}

	/* expand set span of the current bio */	
	tx_cache_expand_set_span(dmc, ebio->eb_bc, ebio->eb_cacheset);
#else
	spin_unlock_irqrestore(&dmc->cache_sets[ebio->eb_cacheset].cs_lock,
			       flags);
#endif
	/*
	 * Enqueue clean set if there is no room in the set
	 * TBD
	 * Ensure, a force clean
	 */
#ifdef CONFIG_TX_CACHE
	if (ebio->eb_iotype & EB_INVAL) {
		spin_lock(&tx->tx_lock);
		tx->tx_status = TX_IN_ABORT;
		spin_unlock(&tx->tx_lock);
	}

	spin_lock_irqsave(&dmc->pers_list_lock, flags);	
	eio_comply_dirty_thresholds(dmc, ebio->eb_cacheset);
	spin_unlock_irqrestore(&dmc->pers_list_lock, flags);	
	
	if (retval == 1)
		atomic_inc(&tx->nr_remain);
	
	if (tx && tx->tx_status == TX_IN_ABORT)
		return TX_CONFLICT;
#else
	if (res < 0)
		eio_comply_dirty_thresholds(dmc, ebio->eb_cacheset);

#endif
	return retval;
}

/* Top level read function, called from eio_map */
static void
eio_read(struct cache_c *dmc, struct bio_container *bc, struct eio_bio *ebegin)
{
	int ucread = 0;
	struct eio_bio *ebio;
	struct eio_bio *enext;
#ifdef CONFIG_TX_CACHE
	int error;
	int abort = 0;
#endif

	bc->bc_dir = UNCACHED_READ;
	ebio = ebegin;
	while (ebio) {
		enext = ebio->eb_next;
#ifdef CONFIG_TX_CACHE
		error = eio_read_peek(dmc, ebio);

		if (error == TX_CONFLICT) {
			abort = 1;
			goto tx_abort;
			//eb_endio(ebio, error);
		}
		else if (error == 0)
			ucread = 1;
#else
		if (eio_read_peek(dmc, ebio) == 0)
			ucread = 1;
#endif
		ebio = enext;
	}

	if (ucread) {
		/*
		 * Uncached read.
		 * Start HDD I/O. Once that is finished
		 * readfill or dirty block re-read would start
		 */
		atomic64_inc(&dmc->eio_stats.uncached_reads);
		eio_disk_io(dmc, bc->bc_bio, ebegin, bc, 0);
	} else {
		/* Cached read. Serve the read from SSD */

		/*
		 * Pass all orig bio flags except UNPLUG.
		 * Unplug in the end if flagged.
		 */
		int rw_flags;

		rw_flags = 0;

		bc->bc_dir = CACHED_READ;
		ebio = ebegin;

		VERIFY_BIO_FLAGS(ebio);

		EIO_ASSERT((rw_flags & 1) == READ);
		while (ebio) {
			enext = ebio->eb_next;
			ebio->eb_iotype = EB_MAIN_IO;

			eio_cached_read(dmc, ebio, rw_flags);
			ebio = enext;
		}
	}
tx_abort:
	if (abort) {
		ebio = ebegin;
		
		while (ebio) {
			enext = ebio->eb_next;
			
			if (ebio->eb_index != -1)
				tx_cache_undo_ebio(dmc, ebio);
			
			eb_endio(ebio, error);
			ebio = enext;
		}

		__abort_tx(dmc, bc->tx);
		bc->tx->tx_status = TX_ABORTED;
	}
}

/* Top level write function called from eio_map */
static void
eio_write(struct cache_c *dmc, struct bio_container *bc, struct eio_bio *ebegin)
{
	int ucwrite = 0;
	int error = 0;
	struct eio_bio *ebio;
	struct eio_bio *enext;
#ifdef CONFIG_TX_CACHE
	int abort = 0;	
	
	/*
	 * TODO
	 * 	For untransactional write, I assume that it is "single-write transaction".
	 *  So, when an un-tx write request is arrived, 
	 *  invoke tx-begin() tx-end() with tx_id(0) for it.
 	 */
#endif	//CONFIG_TX_CACHE

	if ((dmc->mode != CACHE_MODE_WB) ||
	    (dmc->sysctl_active.do_clean & EIO_CLEAN_KEEP))
		ucwrite = 1;

	ebio = ebegin;
	while (ebio) {
		enext = ebio->eb_next;
#ifdef CONFIG_TX_CACHE
		error = eio_write_peek(dmc, ebio);

		if (error == TX_CONFLICT) {
			/*
			 * TODO
			 * Implementing abort protocol
			 */
			abort = 1;
			goto tx_abort;
		} else if (error == 0)
			ucwrite = 1;
#else
		if (eio_write_peek(dmc, ebio) == 0)
			ucwrite = 1;
#endif	//CONFIG_TX_CACHE
		ebio = enext;
	}

	if (ucwrite) {
		/*
		 * Uncached write.
		 * Start both SSD and HDD writes
		 */
		TX_CACHE_DEBUG("uncached write occurred");

		atomic64_inc(&dmc->eio_stats.uncached_writes);
#ifdef CONFIG_TX_CACHE
		atomic_set(&bc->bc_mdwait, 0);
#else
		bc->bc_mdwait = 0;
#endif
		bc->bc_dir = UNCACHED_WRITE;
		ebio = ebegin;
		wmb();
		while (ebio) {
			enext = ebio->eb_next;
			eio_uncached_write(dmc, ebio);
			ebio = enext;
		}

		eio_disk_io(dmc, bc->bc_bio, ebegin, bc, 0);
	} else {
		/* Cached write. Start writes to SSD blocks */

		int rw_flags;
		rw_flags = 0;
		
		bc->bc_dir = CACHED_WRITE;
#ifdef CONFIG_TX_CACHE
		if (atomic_read(&bc->bc_mdwait) > 0) 
#else
		if (bc->bc_mdwait > 0)
#endif
		{
			/*
			 * mdreqs are required only if the write would cause a metadata
			 * update.
			 */
#if CONFIG_DELAYED_MD_WRITE
			atomic_inc(&bc->tx->mdupdate_cnt);
			error = 0;
#else
			error = eio_alloc_mdreqs(dmc, bc);
#endif
		} else {
			TX_CACHE_DEBUG("no bc_mdwait bc: %p", bc);
		}

		/*
		 * Pass all orig bio flags except UNPLUG.
		 * UNPLUG in the end if flagged.
		 */
		ebio = ebegin;
		VERIFY_BIO_FLAGS(ebio);

		while (ebio) {
			enext = ebio->eb_next;
			ebio->eb_iotype = EB_MAIN_IO;

			if (!error) {
				
				eio_cached_write(dmc, ebio, WRITE | rw_flags);

			} else {
				unsigned long flags;
				u_int8_t cstate;

				pr_err
					("eio_write: IO submission failed, block %llu",
					EIO_DBN_GET(dmc, ebio->eb_index));
				spin_lock_irqsave(&dmc->
						  cache_sets[ebio->eb_cacheset].
						  cs_lock, flags);
				cstate =
					EIO_CACHE_STATE_GET(dmc, ebio->eb_index);
				if (cstate != ALREADY_DIRTY) {

					/*
					 * A DIRTY(inprog) block should be invalidated on error.
					 */
					EIO_CACHE_STATE_SET(dmc, ebio->eb_index,
							    INVALID);
					atomic64_dec_if_positive(&dmc->
								 eio_stats.
								 cached_blocks);
				}
				spin_unlock_irqrestore(&dmc->
						       cache_sets[ebio->
								  eb_cacheset].
						       cs_lock, flags);
				eb_endio(ebio, error);
			}
			ebio = enext;
		}
	}

tx_abort:
	if (abort) {
		ebio = ebegin;
		while (ebio) {
			enext = ebio->eb_next;
			
			if (ebio->eb_index != -1)
				tx_cache_undo_ebio(dmc, ebio);
			
			eb_endio(ebio, error);
			ebio = enext;
		}

		__abort_tx(dmc, bc->tx);
		bc->tx->tx_status = TX_ABORTED;
	}
}

/*
 * Synchronous clean of all the cache sets. Callers of this function needs
 * to handle the situation that clean operation was aborted midway.
 */

void eio_clean_all(struct cache_c *dmc)
{
	unsigned long flags = 0;

	EIO_ASSERT(dmc->mode == CACHE_MODE_WB);
#ifdef CONFIG_TX_CACHE
	for (atomic_set(&dmc->clean_index, 0);
	     (atomic_read(&dmc->clean_index) <
	      (s32)(dmc->size >> dmc->consecutive_shift))
	     && (dmc->sysctl_active.do_clean & EIO_CLEAN_START)
	     && (atomic64_read(&dmc->nr_persist) > 0)
	     && (!(dmc->cache_flags & CACHE_FLAGS_SHUTDOWN_INPROG)
		 && !dmc->sysctl_active.fast_remove);
	     atomic_inc(&dmc->clean_index)) {
#else
	for (atomic_set(&dmc->clean_index, 0);
	     (atomic_read(&dmc->clean_index) <
	      (s32)(dmc->size >> dmc->consecutive_shift))
	     && (dmc->sysctl_active.do_clean & EIO_CLEAN_START)
	     && (atomic64_read(&dmc->nr_dirty) > 0)
	     && (!(dmc->cache_flags & CACHE_FLAGS_SHUTDOWN_INPROG)
		 && !dmc->sysctl_active.fast_remove);
	     atomic_inc(&dmc->clean_index)) {
#endif
		if (unlikely(CACHE_FAILED_IS_SET(dmc))) {
			pr_err("clean_all: CACHE \"%s\" is in FAILED state.",
			       dmc->cache_name);
			break;
		}
		
		TX_CACHE_DEBUG3("nr_dirty: %llu nr_persist: %llu, set_dirty: %d set_persist: %d", 
			atomic64_read(&dmc->nr_dirty), 
			atomic64_read(&dmc->nr_persist), 
			dmc->cache_sets[atomic_read(&dmc->clean_index)].nr_dirty, 
			dmc->cache_sets[atomic_read(&dmc->clean_index)].nr_persist);
		eio_clean_set(dmc, (index_t)(atomic_read(&dmc->clean_index)),
				/* whole */ 1, /* force */ 1);
	}

	spin_lock_irqsave(&dmc->cache_spin_lock, flags);
	dmc->sysctl_active.do_clean &= ~EIO_CLEAN_START;
	spin_unlock_irqrestore(&dmc->cache_spin_lock, flags);
}

/*
 * Do unconditional clean of a cache.
 * Useful for a cold enabled writeback cache.
 */
void eio_clean_for_reboot(struct cache_c *dmc)
{
	index_t i;

	for (i = 0; i < (index_t)(dmc->size >> dmc->consecutive_shift); i++)
		eio_clean_set(dmc, i, /* whole */ 1, /* force */ 1);
}

#ifdef CONFIG_TX_CACHE
static inline void 
tx_cache_make_clean_log(struct cache_c *dmc, index_t set, struct log_entry *logs)
{
	int i, start_idx, end_idx;
	int clean = 0;

	i = start_idx = set * dmc->assoc;
	end_idx = start_idx + dmc->assoc;

	for (; i < end_idx; i++) {
		if (EIO_CACHE_STATE_GET(dmc, i) == CLEAN_INPROG) {
			logs->index = i;
			logs->dbn = 0; 	//CLEAN == INVALIDATE
			logs++;
			clean++;
		}
	}	
	TX_CACHE_DEBUG2("clean: %d", clean);
}
#endif

/*
 * Used during the partial cache set clean.
 * Uses reclaim policy(LRU/FIFO) information to
 * identify the cache blocks that needs cleaning.
 * The number of such cache blocks is determined
 * by the high and low thresholds set.
 */
static void
eio_get_setblks_to_clean(struct cache_c *dmc, index_t set, int *ncleans, struct log_entry **log)
{
	int i = 0;
	int max_clean;
	index_t start_index;
	int nr_writes = 0;
	struct tx_block *block;

	*ncleans = 0;

#ifdef CONFIG_TX_CACHE
	max_clean = dmc->cache_sets[set].nr_persist -
		((dmc->sysctl_active.dirty_set_low_threshold * dmc->assoc) / 100);
#else
	max_clean = dmc->cache_sets[set].nr_dirty -
		((dmc->sysctl_active.dirty_set_low_threshold * dmc->assoc) / 100);
#endif

	if (max_clean > CLEAN_MAX)
		max_clean = CLEAN_MAX;

	if (max_clean <= 0)
		/* Nothing to clean */
		return;

	start_index = set * dmc->assoc;

	/*
	 * Spinlock is not required here, as we assume that we have
	 * taken a write lock on the cache set, when we reach here
	 */
	if (dmc->policy_ops == NULL) {
		/* Scan sequentially in the set and pick blocks to clean */
		while ((i < (int)dmc->assoc) && (nr_writes < max_clean)) {
			if ((EIO_CACHE_STATE_GET(dmc, start_index + i) &
			     (DIRTY | BLOCK_IO_INPROG)) == DIRTY) {
#ifdef CONFIG_TX_CACHE
				if (TX_CACHE_STATE_TEST(dmc, start_index + i, SHADOW))
					continue;
				if (TX_CACHE_TEST_LINK(dmc, start_index + i))
					continue;
				
				if (TX_CACHE_TEST_LINK(dmc, start_index + i)) {
					block = alloc_tx_block();
					block->index = start_index + i;
					tx_cache_set_undo(dmc, start_index + i, &block->undo);
					list_add_tail(&block->blist, &dmc->clean_undo_list);
					TX_CACHE_CLEAR_LINK(dmc, start_index + i);
				}

				if (TX_CACHE_STATE_RENTED(dmc, start_index + i)) {
					EIO_CACHE_STATE_SET(dmc, start_index + i, CLEAN_INPROG);
					TX_CACHE_STATE_RENT(dmc, start_index + i);
				} else {
					EIO_CACHE_STATE_SET(dmc, start_index + i, CLEAN_INPROG);
				}
#endif
				EIO_CACHE_STATE_ON(dmc, start_index + i,
						   DISKWRITEINPROG);
				nr_writes++;
			}
			i++;
		}
	} else
		nr_writes =
			eio_policy_clean_set(dmc->policy_ops, set, max_clean);
#ifdef CONFIG_TX_CACHE
	*log = (struct log_entry *)kzalloc(sizeof(struct log_entry) * nr_writes, GFP_NOWAIT);
	tx_cache_make_clean_log(dmc, set, *log);
	TX_CACHE_DEBUG2("nr_clean: %d", nr_writes);
#endif
	*ncleans = nr_writes;
}

/* Callback function, when synchronous I/O completes */
static void eio_sync_io_callback(int error, void *context)
{
	struct sync_io_context *sioc = (struct sync_io_context *)context;

	if (error)
		sioc->sio_error = error;
	up_read(&sioc->sio_lock);
}

/*
 * Setup biovecs for preallocated biovecs per cache set.
 */

struct bio_vec *setup_bio_vecs(struct bio_vec *bvec, index_t block_index,
			       unsigned block_size, unsigned total,
			       unsigned *num_bvecs)
{
	struct bio_vec *data = NULL;
	index_t iovec_index;

	switch (block_size) {
	case BLKSIZE_2K:
		*num_bvecs = total;
		iovec_index = block_index;
		data = &bvec[iovec_index];
		break;

	case BLKSIZE_4K:
		*num_bvecs = total;
		iovec_index = block_index;
		data = &bvec[iovec_index];
		break;

	case BLKSIZE_8K:
		/*
		 * For 8k data block size, we need 2 bio_vecs
		 * per data block.
		 */
		*num_bvecs = total * 2;
		iovec_index = block_index * 2;
		data = &bvec[iovec_index];
		break;
	}

	return data;
}

/* Cleans a given cache set */
static void
eio_clean_set(struct cache_c *dmc, index_t set, int whole, int force)
{
	struct eio_io_region where;
	int error;
	index_t i;
	index_t j;
	index_t start_index;
	index_t end_index;
	struct sync_io_context sioc;
	int ncleans = 0;
	unsigned long flags;
	unsigned long elapsed, begin_time;
	int k;
	index_t blkindex;
	struct bio_vec *bvecs;
	unsigned nr_bvecs = 0, total;
	void *pg_virt_addr[NR_MD_PAGE] = { NULL };

#ifdef CONFIG_TX_CACHE
	struct list_head wb_list;
	struct tx_block *block, *block_next;	
	struct log_page log_page;
	struct log_pack *log_pack;
	struct log_entry *logs = NULL;
	unsigned long log_size;
	int nr_log_pages = 1;
	int log_idx;
	int clean_window = 0;
	unsigned long sleep_time = 0, sleep_begin;
	int returning = 0, master;
	struct set_chain *set_chain;
	INIT_LIST_HEAD(&wb_list);
#endif

	/* Cache is failed mode, do nothing. */
	if (unlikely(CACHE_FAILED_IS_SET(dmc))) {
		pr_debug("clean_set: CACHE \"%s\" is in FAILED state.",
			 dmc->cache_name);
		goto err_out1;
	}

	/* Nothing to clean, if there are no dirty blocks */
#ifdef CONFIG_TX_CACHE
	if (dmc->cache_sets[set].nr_persist == 0)
		goto err_out1;
#else
	if (dmc->cache_sets[set].nr_dirty == 0)
		goto err_out1;
#endif

	/* If this is not the suitable time to clean, postpone it */
	if ((!force) && AUTOCLEAN_THRESHOLD_CROSSED(dmc)) {
		eio_touch_set_lru(dmc, set);
		goto err_out1;
	}

	/*
	 * 1. Take exclusive lock on the cache set
	 * 2. Verify that there are dirty blocks to clean
	 * 3. Identify the cache blocks to clean
	 * 4. Read the cache blocks data from ssd
	 * 5. Write the cache blocks data to hdd
	 * 6. Update on-disk cache metadata
	 * 7. Update in-core cache metadata
	 */

	start_index = set * dmc->assoc;
	end_index = start_index + dmc->assoc;

	/* 1. exclusive lock. Let the ongoing writes to finish. Pause new writes */

	//down_write(&dmc->cache_sets[set].rw_lock);
	down_read(&dmc->reclaimer_lock);
	begin_time = jiffies;
	atomic64_inc(&dmc->eio_stats.cleanings);

	/* 2. Return if there are no dirty blocks to clean */
	if (dmc->cache_sets[set].nr_dirty == 0)
		goto err_out2;

	//spin_lock_irqsave(&dmc->set_headers[set].header_lock, flags);	/* CONFIG_TX_CACHE */
	spin_lock(&dmc->cache_sets[set].cs_lock);	/* CONFIG_TX_CACHE */
	TX_CACHE_DEBUG3("set(%d:%d) is being cleaned", set, dmc->cache_sets[set].nr_dirty);
	
	/* 3. identify and mark cache blocks to clean */
	if (!whole) {
#if CONFIG_SET_SHARING
		tx_cache_detach_set(dmc, set);	
#endif
		eio_get_setblks_to_clean(dmc, set, &ncleans, &logs); /* CONFIG_TX_CACHE */
	} else {
#if	CONFIG_SET_SHARING
		tx_cache_detach_set(dmc, set);	
#endif
		for (i = start_index; i < end_index; i++) {
			if (EIO_CACHE_STATE_GET(dmc, i) == ALREADY_DIRTY) {
#ifdef CONFIG_TX_CACHE
				/* 
				 * only persistent blocks can be written-back
				 */
				if (TX_CACHE_STATE_TEST(dmc, i, PERSIST)) {
#if CONFIG_SET_SHARING
					if (TX_CACHE_TEST_LINK(dmc, i)) {
						block = alloc_tx_block();
						block->index = i;
						tx_cache_set_undo(dmc, i, &block->undo);
						list_add_tail(&block->blist, &dmc->clean_undo_list);
					}
					/* link is cleared */
					if (TX_CACHE_STATE_RENTED(dmc, i)) {
						EIO_CACHE_STATE_SET(dmc, i, CLEAN_INPROG);
						TX_CACHE_STATE_ON(dmc, i, RENT);
					} else {
						EIO_CACHE_STATE_SET(dmc, i, CLEAN_INPROG);
					}
#else
					EIO_CACHE_STATE_SET(dmc, i, CLEAN_INPROG);
					/* 
					 * EIO_CACHE_STATE_SET assigns CLEAN_INPROG regardless of tx cache state.
					 * So, we set PERSIST again here.
					 */
					TX_CACHE_STATE_ON(dmc, i, PERSIST);
#endif	//CONFIG_SET_SHARING
				}
				else
					continue;
				/* blocks in uncommitted tx MUST NOT be written back to HDD 
				 * and, we don't need to preserve persistent flag 
				 * because, a block being CLEAN_INPROG must have PERSIST flag before.
				 */
#else
				EIO_CACHE_STATE_SET(dmc, i, CLEAN_INPROG);
#endif	//CONFIG_TX_CACHE
				ncleans++;
			}
		}
#ifdef CONFIG_TX_CACHE
		TX_CACHE_INFO("for clean all, ncleans: %d", ncleans);
		logs = (struct log_entry *)kzalloc(sizeof(struct log_entry) * ncleans, GFP_NOWAIT);
		tx_cache_make_clean_log(dmc, set, logs);
#endif
	}	//else
	spin_unlock(&dmc->cache_sets[set].cs_lock);	/* CONFIG_TX_CACHE */
	//spin_unlock_irqrestore(&dmc->set_headers[set].header_lock, flags);	/* CONFIG_TX_CACHE */
	
	/* If nothing to clean, return */
#if CONFIG_SET_SHARING
	if (!ncleans) {	
		TX_CACHE_DEBUG("Nothing for cleaning");
		PREPARE_LAST_LOC(__LINE__);
		tx_cache_attach_set(dmc, set);
		goto err_out2;
	}
#else
	if (!ncleans)
		goto err_out2;
#endif

	/*
	 * From this point onwards, make sure to reset
	 * the clean inflag on cache blocks before returning
	 */

	/* 4. read cache set data */

	init_rwsem(&sioc.sio_lock);
	sioc.sio_error = 0;

	for (i = start_index; i < end_index; i++) {
		if (EIO_CACHE_STATE_GET(dmc, i) == CLEAN_INPROG) {

			for (j = i; ((j < end_index) &&
				(EIO_CACHE_STATE_GET(dmc, j) == CLEAN_INPROG));
				j++);

			blkindex = (i - start_index);
			total = (j - i);
			clean_window += total;

			/*
			 * Get the correct index and number of bvecs
			 * setup from dmc->clean_dbvecs before issuing i/o.
			 */
			bvecs =
				setup_bio_vecs(dmc->clean_dbvecs, blkindex,
					       dmc->block_size, total, &nr_bvecs);
			EIO_ASSERT(bvecs != NULL);
			EIO_ASSERT(nr_bvecs > 0);

			where.bdev = dmc->cache_dev->bdev;
			where.sector =
				(i << dmc->block_shift) + dmc->md_sectors;
			where.count = total * dmc->block_size;

			SECTOR_STATS(dmc->eio_stats.ssd_reads,
				     to_bytes(where.count));
			down_read(&sioc.sio_lock);
			error =
				eio_io_async_bvec(dmc, &where, READ, bvecs,
						  nr_bvecs, eio_sync_io_callback,
						  &sioc, 0);
			if (error) {
				sioc.sio_error = error;
				up_read(&sioc.sio_lock);
			}
			bvecs = NULL;
			i = j;
			/*
			if (clean_window >= CLEAN_WIN) {
				sleep_begin = jiffies;
				msleep(CLEAN_RELAX);
				sleep_time += jiffies - sleep_begin;
				clean_window = 0;
			}
			*/
		}
	}

	clean_window = 0;
	/*
	 * In above for loop, submit all READ I/Os to SSD
	 * and unplug the device for immediate submission to
	 * underlying device driver.
	 */
	eio_unplug_cache_device(dmc);

	/* wait for all I/Os to complete and release sync lock */
	down_write(&sioc.sio_lock);
	up_write(&sioc.sio_lock);

	error = sioc.sio_error;
	if (error)
		goto err_out3;

	/* 5. write to hdd */
	/*
	 * While writing the data to HDD, explicitly enable
	 * BIO_RW_SYNC flag to hint higher priority for these
	 * I/Os.
	 */
	for (i = start_index; i < end_index; i++) {
		if (EIO_CACHE_STATE_GET(dmc, i) == CLEAN_INPROG) {

			blkindex = (i - start_index);
			total = 1;
			clean_window++;	

			bvecs =
				setup_bio_vecs(dmc->clean_dbvecs, blkindex,
					       dmc->block_size, total, &nr_bvecs);
			EIO_ASSERT(bvecs != NULL);
			EIO_ASSERT(nr_bvecs > 0);

			where.bdev = dmc->disk_dev->bdev;
			where.sector = EIO_DBN_GET(dmc, i);
			where.count = dmc->block_size;

			SECTOR_STATS(dmc->eio_stats.disk_writes,
				     to_bytes(where.count));
			down_read(&sioc.sio_lock);
			error = eio_io_async_bvec(dmc, &where, WRITE | REQ_SYNC,
						  bvecs, nr_bvecs,
						  eio_sync_io_callback, &sioc,
						  1);

			if (error) {
				sioc.sio_error = error;
				up_read(&sioc.sio_lock);
			}
			bvecs = NULL;
			/*
			if (clean_window > CLEAN_WIN) {
				sleep_begin = jiffies;	
				msleep(CLEAN_RELAX);
				sleep_time += jiffies - sleep_begin;
				clean_window = 0;
			}
			*/
		}
	}

	clean_window = 0;
	/* wait for all I/Os to complete and release sync lock */
	down_write(&sioc.sio_lock);
	up_write(&sioc.sio_lock);
	
	elapsed = (long)jiffies_to_msecs(jiffies - begin_time);
	atomic64_add(elapsed, &dmc->eio_stats.cleantime_ms);
	begin_time = jiffies;
	TX_CACHE_DEBUG("Write to HDD successfully completed");

	error = sioc.sio_error;
	if (error)
		goto err_out3;

	/*
	 * At this point, we decide that all WB processes are done.
	 * So we start to clean WBed persist blocks.
	 */

	EIO_ASSERT(dmc->mdpage_count <= NR_MD_PAGE);

	/*
	 * XXX
	 * Shit, md_size can be extended upto NR_MD_PAGE.
	 */
	/*
	if (ncleans > LOG_PACK_MAX_HEAD) {
		log_size -= LOG_PACK_MAX_HEAD;
		nr_log_pages += log_size /LOG_PACK_MAX;

		if (log_size % LOG_PACK_MAX <= LOG_PACK_MAX_HEAD)
			nr_log_pages++;
		else if (log_size % LOG_PACK_MAX == 0)
			nr_log_pages++;
	}
	*/

	log_size = ncleans + 2;		//including header and footer
	nr_log_pages = log_size / LOG_PACK_MAX;

	if (log_size % LOG_PACK_MAX)
		nr_log_pages++;

	log_pack = (struct log_pack *) kzalloc(sizeof(struct log_pack), GFP_NOWAIT);
	
	log_pack->header.log_id = atomic_inc_return(&aoml_log_id);
	TX_CACHE_SET_CLEAN_LOG(log_pack->header.log_type);
	log_pack->header.nr_entries = log_size;	//including header and footer
	log_pack->logs = logs;

	log_page.log_pack = log_pack;
	log_page.log_page = dmc->clean_mdpages;
	log_page.nr_pages = nr_log_pages;

	TX_CACHE_DEBUG2("log_size: %d, nr_log_pages: %d", log_size, nr_log_pages);

	for (k = 0; k < nr_log_pages; k++)
		pg_virt_addr[k] = kmap(dmc->clean_mdpages[k]);
	
	log_idx = 0;
	error = 0;
	
	for (k = 0; k < nr_log_pages; k++) 
		log_idx = tx_cache_packing_md_log(dmc, pg_virt_addr[k], log_pack, log_idx);

	/* log_idx does not count header/footer */
	if (log_idx != log_pack->header.nr_entries - 2) {
		TX_CACHE_ERROR("error occrrued while in making clean log");	
		error = -EINVAL;
	}

	for (k = 0; k < nr_log_pages; k++)
		kunmap(dmc->clean_mdpages[k]);
	
	tx_cache_show_log_page(&log_page);

	if (!error) {
		tx_cache_append_md_log(dmc, &log_page, "clean");
		atomic64_add(log_page.nr_pages, &dmc->nr_mdlog_pages);
	}
	
	kfree(log_pack);
err_out3:

	/*
	 * 7. update in-core cache metadata for clean_inprog blocks.
	 * If there was an error, set them back to ALREADY_DIRTY
	 * If no error, set them to VALID
	 */
#ifdef CONFIG_TX_CACHE
	spin_lock_irqsave(&dmc->cache_sets[set].cs_lock, flags);
#endif
	for (i = start_index; i < end_index; i++) {
		if (EIO_CACHE_STATE_GET(dmc, i) == CLEAN_INPROG) {
#if CONFIG_SET_SHARING
			/* after wb, rented block is invalidated 
			 * thus, if read request to this block will cause
			 * block read from a source disk. 
			 * of course, we can optimize it for read performance,
			 * but, we implement for proof-of-conecpt,
			 * we just igonore it now. 
			 * TODO: remain rent information for read request
			 */
			if (error) {
				if (TX_CACHE_STATE_RENTED(dmc, i)) {
					EIO_CACHE_STATE_SET(dmc, i, RENT | ALREADY_DIRTY);
				} else
					EIO_CACHE_STATE_SET(dmc, i, ALREADY_DIRTY);
			} else {
				if (TX_CACHE_STATE_RENTED(dmc, i)) {
					TX_CACHE_STATE_RETURN(dmc, i);
					dmc->cache_sets[set].nr_rented--;
					
					if (dmc->cache_sets[set].nr_rented == 0) {
						TX_CACHE_DEBUG3("set(%d) will be returned", set);
						master = dmc->cache_sets[set].set_chain->master_set;
						set_chain = dmc->cache_sets[set].set_chain;	
						dmc->cache_sets[set].set_chain = NULL;
						returning = 1;
					}
				} 
				EIO_ASSERT(dmc->cache_sets[set].nr_persist > 0);
			
				EIO_CACHE_STATE_SET(dmc, i, VALID);
				dmc->cache_sets[set].nr_dirty--;
				dmc->cache_sets[set].nr_persist--;
				atomic64_dec(&dmc->nr_dirty);
				atomic64_dec(&dmc->nr_persist);
			}
#else	//CONFIG_SET_SHARING
			if (error)
				EIO_CACHE_STATE_SET(dmc, i, ALREADY_DIRTY);
			else {
				EIO_CACHE_STATE_SET(dmc, i, VALID);
				EIO_ASSERT(dmc->cache_sets[set].nr_dirty > 0);
				dmc->cache_sets[set].nr_dirty--;
#ifdef CONFIG_TX_CACHE
				dmc->cache_sets[set].nr_persist--;
				atomic64_dec(&dmc->nr_persist);
#endif	//CONFIG_TX_CACHE
				atomic64_dec(&dmc->nr_dirty);
			}
#endif	//CONFIG_SET_SHARING
		}
	}
#if CONFIG_SET_SHARING
	PREPARE_LAST_LOC(__LINE__);
	tx_cache_attach_set(dmc, set);
#endif
#ifdef CONFIG_TX_CACHE
	if (error) {
		TX_CACHE_ERROR("error occurred while cleaning set(%ld)", set);
		list_for_each_entry_safe(block, block_next, &dmc->clean_undo_list, blist) {
			list_del(&block->blist);
			tx_cache_do_undo(dmc, block->index, &block->undo);
			dealloc_tx_block(block);
		}
		spin_unlock_irqrestore(&dmc->cache_sets[set].cs_lock, flags);
	} else {
		spin_unlock_irqrestore(&dmc->cache_sets[set].cs_lock, flags);
		
		list_for_each_entry_safe(block, block_next, &dmc->clean_undo_list, blist) {
			list_del(&block->blist);
			dealloc_tx_block(block);
		}
		
		TX_CACHE_DEBUG3("set(%ld) is successfully cleaned (sleep_time: %lu)", set, sleep_time);
	}

	INIT_LIST_HEAD(&dmc->clean_undo_list);
#endif	//CONFIG_TX_CACHE

#if CONFIG_SET_SHARING
	spin_lock(&dmc->set_headers[set].header_lock);
	if (dmc->set_headers[set].current_set != set) {
		TX_CACHE_DEBUG("set->current_set is moved from %d to %d", 
			dmc->set_headers[set].current_set, set);
		dmc->set_headers[set].current_set = set;	
	}
	spin_unlock(&dmc->set_headers[set].header_lock);

	/* delayed returning job */
	if (returning) {
		//struct set_chain *set_chain;
		EIO_ASSERT(set_chain != NULL);
		master = set_chain->master_set;
		
		spin_lock(&dmc->set_headers[master].header_lock);
		spin_lock(&dmc->cache_sets[set].cs_lock);
	
		/* if set is released by eio_write_peek, 
		 * cache_set->set_chain is NULL
		 * thus, we just skip it
		 */
		//if (set_chain) {
			//dmc->cache_sets[set].set_chain = NULL;
		list_del(&set_chain->chain);
		dealloc_set_chain(set_chain);
		//}
			
		spin_unlock(&dmc->cache_sets[set].cs_lock);
		spin_unlock(&dmc->set_headers[master].header_lock);
	}
#endif

	elapsed = (long)jiffies_to_msecs(jiffies - begin_time);
	atomic64_add(elapsed, &dmc->eio_stats.cleantime_md_ms);
err_out2:
	up_read(&dmc->reclaimer_lock);
	//up_write(&dmc->cache_sets[set].rw_lock);

err_out1:

#ifdef CONFIG_TX_CACHE
	kfree(logs);
#endif

	/* Reset clean flags on the set */

	if (!force) {
		spin_lock_irqsave(&dmc->cache_sets[set].cs_lock, flags);
		dmc->cache_sets[set].flags &=
			~(SETFLAG_CLEAN_INPROG | SETFLAG_CLEAN_WHOLE);
		spin_unlock_irqrestore(&dmc->cache_sets[set].cs_lock, flags);
	}
#ifdef CONFIG_TX_CACHE
	spin_lock_irqsave(&dmc->cache_sets[set].cs_lock, flags);
	spin_lock(&dmc->pers_list_lock);
	
	/* before adding cleanq, it should be detached from rb_tree */	
	dmc->pers_sets[set].key = dmc->cache_sets[set].nr_persist;
	tx_cache_pers_set_timestamp(&dmc->pers_sets[set], jiffies);
	tx_cache_pers_add(dmc, &dmc->pers_sets[set]);
	
	spin_unlock(&dmc->pers_list_lock);
	spin_unlock_irqrestore(&dmc->cache_sets[set].cs_lock, flags);	

	if (dmc->cache_sets[set].nr_persist) 
		tx_cache_schedule_clean_aged_sets(dmc);

	tx_cache_comply_compaction_threshold(dmc);
#else
	if (dmc->cache_sets[set].nr_dirty)
		/*
		 * Lru touch the set, so that it can be picked
		 * up for whole set clean by clean thread later
		 */
		eio_touch_set_lru(dmc, set);
#endif	//CONFIG_TX_CACHE
	return;
}

/*
 * Enqueues the dirty sets for clean, which had got dirtied long
 * time back(aged). User tunable values to determine if a set has aged
 */
void eio_clean_aged_sets(struct work_struct *work)
{
	struct cache_c *dmc;
	unsigned long flags = 0;
	index_t set_index;
	u_int64_t set_time;
	u_int64_t cur_time;

	dmc = container_of(work, struct cache_c, clean_aged_sets_work.work);

	/*
	 * In FAILED state, dont schedule cleaning of sets.
	 */
	if (unlikely(CACHE_FAILED_IS_SET(dmc))) {
		pr_debug("clean_aged_sets: Cache \"%s\" is in failed mode.\n",
			 dmc->cache_name);
		/*
		 * This is to make sure that this thread is rescheduled
		 * once CACHE is ACTIVE again.
		 */
#ifdef CONFIG_TX_CACHE
		spin_lock_irqsave(&dmc->pers_list_lock, flags);
		dmc->is_clean_aged_sets_sched = 0;
		spin_unlock_irqrestore(&dmc->pers_list_lock, flags);
#else
		spin_lock_irqsave(&dmc->dirty_set_lru_lock, flags);
		dmc->is_clean_aged_sets_sched = 0;
		spin_unlock_irqrestore(&dmc->dirty_set_lru_lock, flags);
#endif
		return;
	}

	cur_time = jiffies;

	/* Use the set LRU list to pick up the most aged sets. */
#ifdef CONFIG_TX_CACHE
	spin_lock_irqsave(&dmc->pers_list_lock, flags);
#else
	spin_lock_irqsave(&dmc->dirty_set_lru_lock, flags);
#endif
	do {
#ifdef CONFIG_TX_CACHE
		struct set_list *set;
	
		set = tx_cache_pers_pick(dmc);
		if (set == NULL)
			break;
		set_time = tx_cache_pers_get_timestamp(set);

		if ((EIO_DIV((cur_time - set_time), HZ)) <
		    (dmc->sysctl_active.time_based_clean_interval * 60)) {
		    tx_cache_pers_add(dmc, set);
			break;
		}

		set_index = set->set_num;
		set->q_flag &= ~Q_MASK;
		set->q_flag |= IN_CLEANING;

		TX_CACHE_DEBUG("cleaning target: %d", set_index);
		if (dmc->cache_sets[set_index].nr_persist > 0) {
			spin_unlock_irqrestore(&dmc->pers_list_lock, flags);
			eio_addto_cleanq(dmc, set_index, 1);
			spin_lock_irqsave(&dmc->pers_list_lock, flags);
		}
#else
		lru_read_head(dmc->dirty_set_lru, &set_index, &set_time);
		if (set_index == LRU_NULL)
			break;
		
		if ((EIO_DIV((cur_time - set_time), HZ)) <
		    (dmc->sysctl_active.time_based_clean_interval * 60))
			break;
		
		lru_rem(dmc->dirty_set_lru, set_index);	//need to modify for TX_CACHE!
		if (dmc->cache_sets[set_index].nr_dirty > 0) {
			spin_unlock_irqrestore(&dmc->dirty_set_lru_lock, flags);
			eio_addto_cleanq(dmc, set_index, 1);
			spin_lock_irqsave(&dmc->dirty_set_lru_lock, flags);
		}
#endif	//CONFIG_TX_CACHE
	} while (1);
#ifdef CONFIG_TX_CACHE
	spin_unlock_irqrestore(&dmc->pers_list_lock, flags);
#else
	spin_unlock_irqrestore(&dmc->dirty_set_lru_lock, flags);
#endif

	/* Re-schedule the aged set clean, unless the clean has to stop now */

	if (dmc->sysctl_active.time_based_clean_interval == 0)
		goto out;

	schedule_delayed_work(&dmc->clean_aged_sets_work,
			      dmc->sysctl_active.time_based_clean_interval *
			      60 * HZ);
out:
	return;
}

/* Move the given set at the head of the set LRU list */
void eio_touch_set_lru(struct cache_c *dmc, index_t set)
{
	u_int64_t systime;
	unsigned long flags;

	systime = jiffies;
	spin_lock_irqsave(&dmc->dirty_set_lru_lock, flags);
	lru_touch(dmc->dirty_set_lru, set, systime);

	if ((dmc->sysctl_active.time_based_clean_interval > 0) &&
	    (dmc->is_clean_aged_sets_sched == 0)) {
		schedule_delayed_work(&dmc->clean_aged_sets_work,
				      dmc->sysctl_active.
				      time_based_clean_interval * 60 * HZ);
		dmc->is_clean_aged_sets_sched = 1;
	}

	spin_unlock_irqrestore(&dmc->dirty_set_lru_lock, flags);
}

#ifdef CONFIG_TX_CACHE
void tx_cache_schedule_clean_aged_sets(struct cache_c *dmc)
{
	if ((dmc->sysctl_active.time_based_clean_interval > 0) &&
	    (dmc->is_clean_aged_sets_sched == 0)) {
		schedule_delayed_work(&dmc->clean_aged_sets_work,
				      dmc->sysctl_active.
				      time_based_clean_interval * 60 * HZ);
		dmc->is_clean_aged_sets_sched = 1;
	}
}
#endif

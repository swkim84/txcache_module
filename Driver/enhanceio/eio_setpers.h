//#ifndef EIO_SETPERS_H
//#define EIO_SETPERS_H

/* rb-tree based sorted persistent set list 
 * if want to select cleaning target, 
 * thread can get the set number from this list */
#ifdef CONFIG_TX_CACHE

#define IN_CLEANING	1
#define IN_QUEUE	2
#define	Q_MASK	3
#define TIMESTAMP_SHIFT 2

struct set_list {
	struct rb_node node;
	unsigned key;		// number of persistent copies
	unsigned set_num;	// set index
	u_int64_t	q_flag;		// is this node in queue? + time stamp
};

extern int tx_cache_pers_rem(struct cache_c *dmc, index_t set);
extern struct set_list *tx_cache_pers_pick(struct cache_c *dmc);
extern void tx_cache_pers_add(struct cache_c *dmc, struct set_list *list);
extern void tx_cache_touch(struct cache_c *dmc, struct set_list *list);
extern int tx_cache_pers_empty(struct rb_root *root);
extern void tx_cache_recover_pers_list(struct cache_c *dmc);
extern void tx_cache_pers_touch(struct cache_c *dmc, unsigned set_unm, unsigned nr_pers);
extern void tx_cache_pers_set_timestamp(struct set_list *set, u_int64_t time);
extern u_int64_t tx_cache_pers_get_timestamp(struct set_list *set);
#endif 	//CONFIG_TX_CACHE
//#endif	//EIO_SETPERS_H

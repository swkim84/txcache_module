#include "eio.h"
#ifdef CONFIG_TX_CACHE

#define PERS_EMPTY(dmc) tx_cache_pers_empty(&dmc->pers_list)
/* tx_cache_pers_pick picks the set which has the largest number of persistent copies */

struct set_list *tx_cache_pers_pick(struct cache_c *dmc)
{
	struct set_list *pick;

	if (PERS_EMPTY(dmc))
		return NULL;

	if (dmc->pers_leftmost) {
		pick = dmc->pers_leftmost;
		dmc->pers_leftmost = NULL;
	} else {
		struct rb_node **link, *parent;
		
		link = &dmc->pers_list.rb_node;
		parent = NULL;
		
		while (*link) {
			parent = *link;
			link = &parent->rb_left;
		}

		pick = (struct set_list*) rb_entry(parent, struct set_list, node);
	}
	rb_erase(&pick->node, &dmc->pers_list);

	return pick;
}

/* 
 * Rule of ordering: larger -> left, otherwise -> right 
 */

void tx_cache_pers_add(struct cache_c *dmc, struct set_list *set)
{
	int leftmost = 1;
	struct rb_node **link, *parent;
	struct set_list *link_set;
	
	link = &dmc->pers_list.rb_node;
	set->q_flag = IN_QUEUE;
	parent = NULL;

	TX_CACHE_DEBUG("set: %d nr_persist: %d", set->set_num, set->key);
	
	/* do not allow to insert empty set */
	if (set->key == 0)
		return;

	while (*link) {
		int ret;
		parent = *link;
		link_set = (struct set_list *)rb_entry(*link, struct set_list, node);
		ret = set->key - link_set->key;

		if (ret > 0)
			link = &parent->rb_left;
		else {
			link = &parent->rb_right;
			leftmost = 0;
		}
	}

	if (leftmost)
		dmc->pers_leftmost = set;

	rb_link_node(&set->node, parent, link);
	rb_insert_color(&set->node, &dmc->pers_list);
}

/* Reorder pers_list with modified value */

void tx_cache_pers_touch(struct cache_c *dmc, unsigned set_num, unsigned nr_pers)
{
	if (dmc->pers_sets[set_num].key != 0)
		rb_erase(&dmc->pers_sets[set_num].node, &dmc->pers_list);
	dmc->pers_sets[set_num].key = nr_pers;
	tx_cache_pers_set_timestamp(&dmc->pers_sets[set_num], jiffies);
	tx_cache_pers_add(dmc, &dmc->pers_sets[set_num]);
}

int tx_cache_pers_empty(struct rb_root *root)
{
	return RB_EMPTY_ROOT(root);
}

int tx_cache_pers_rem(struct cache_c *dmc, index_t set)
{
	struct rb_node *node;
	/* if pers_list is empty, it is safe to use pers_set*/
	if (PERS_EMPTY(dmc))
		return 1;
	
	rb_erase(&dmc->pers_sets[set].node, &dmc->pers_list);
	return 1;
}

void tx_cache_recover_pers_list(struct cache_c *dmc)
{
	unsigned long set_idx;

	for (set_idx = 0; set_idx < dmc->num_sets; set_idx++) {
		if (dmc->cache_sets[set_idx].nr_persist == 0)
			continue;
		dmc->pers_sets[set_idx].key = dmc->cache_sets[set_idx].nr_persist;
		tx_cache_pers_add(dmc, &dmc->pers_sets[set_idx]);
	}

	TX_CACHE_INFO("persistent set list recovery complete");
}

void tx_cache_pers_set_timestamp(struct set_list *set, u_int64_t time)
{
	set->q_flag = (set->q_flag & Q_MASK) | (time << TIMESTAMP_SHIFT);
}

u_int64_t tx_cache_pers_get_timestamp(struct set_list *set)
{
	return set->q_flag >> TIMESTAMP_SHIFT;
}
#endif // CONFIG_TX_CACHE
